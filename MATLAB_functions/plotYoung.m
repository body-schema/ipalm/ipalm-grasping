% A script for computing a Young modulus from a data
% You must load the data first into you workspace (double click on the .mat file)
% or use the load function e.g. load('FT300-2020-03-26-NF2140-50.mat')

db = matfile("DB.mat");
db=db.database;

dbindex=find(strcmpi([db.id], object_id)==1);
objwidth=db(dbindex).dimensions(3);

%here you can change the point of interest, and its neighbourhood
point40=0.4;
limits=0.05;

switch lower(gripper)
    case {"2f85"}
        otherLimit=3e5;
        pos=position;
        cur=current;
        
        pos=objwidth-(1-pos)*85;
        pos1=pos(pos>=0);
        cur1=cur(pos>=0)*235;
        pos1=pos1/objwidth;
        area=(825)/1000000;
        cur1=cur1/area;
        
        windowSize = 10;
        b = (1/windowSize)*ones(1,windowSize);
        a = 1;
        
        cur2 = filter(b,a,cur1);
        hold on
        p=scatter(pos1,cur1,30);
        scatter(pos1,cur2,30,"MarkerEdgeColor","#808080");
        p.MarkerEdgeColor=db(dbindex).color;
        
        [pfit,S]=polyfit(pos1((pos1>=point40-(limits))&(pos1<=point40+(limits))),cur2((pos1>=point40-(limits))&(pos1<=point40+(limits))),1);
        disp("The Young modulus is: ")
        disp(pfit(1))
        [pval,delta] = polyval(pfit,pos1((pos1>=point40-(limits))&(pos1<=point40+(limits))),S);
        plot(pos1((pos1>=point40-(limits))&(pos1<=point40+(limits))),pval,'HandleVisibility','off','LineWidth',2,'Color',db(dbindex).color);
        
    case {"rg6"}
        otherLimit=14e4;
        wid=width;
        forc=force;
        forc=forc((wid<=objwidth));
        
        wid=objwidth-wid(wid<=objwidth);
        wid=wid/objwidth;
        area=(866)/1000000;
        forc=forc/area;
        
        
        windowSize = 15;
        b = (1/windowSize)*ones(1,windowSize);
        a = 1;
        
        wid2 = filter(b,a,wid);
        
        
        hold on
        p=scatter(wid,forc,30);
        p.MarkerEdgeColor=db(dbindex).color;
        
        pl=plot(wid2,forc,"r");
        pl.LineWidth=2;
        [pfit,S]=polyfit(wid2((wid2>=point40-(limits))&(wid2<=point40+(limits))),forc((wid2>=point40-(limits))&(wid2<=point40+(limits))),1);
        disp("The Young modulus is: ")
        disp(pfit(1))
        [pval,delta] = polyval(pfit,wid2((wid2>=point40-(limits))&(wid2<=point40+(limits))),S);
        pf=plot(wid2((wid2>=point40-(limits))&(wid2<=point40+(limits))),pval,'HandleVisibility','off','LineWidth',2,'Color',db(dbindex).color);
        pf.LineWidth=4;
        
        
    case {"ft300"}
        otherLimit=14e3;
        pos=-position*1000;
        forc=-force;
        
        fo=forc(forc>1);
        pos=pos(forc>1);
        pos=pos-pos(1);
        pos=pos(pos>=0);
        fo=fo(pos>=0);
        [~,i]=max(fo);
        
        pos=pos(1:i);
        fo=fo(1:i);
        
        pos=pos/objwidth;
        fo=fo*1000000/(9025);
        hold on
        p=scatter(pos,fo);
        
        [pfit,S]=polyfit(pos((pos>=point40-(limits))&(pos<=point40+(limits))),fo((pos>=point40-(limits))&(pos<=point40+(limits))),1);
        disp("The Young modulus is: ")
        disp(pfit(1))
        [pval,delta] = polyval(pfit,pos((pos>=point40-(limits))&(pos<=point40+(limits))),S);
        plot(pos((pos>=point40-(limits))&(pos<=point40+(limits))),pval,'HandleVisibility','off','LineWidth',2,'Color',db(dbindex).color);
end

pl=line(ones(1,2)*point40,[0 otherLimit]);
pl.Color="red";

p2=line(ones(1,2)*(point40-(0.08)),[0 otherLimit]);
p3=line(ones(1,2)*(point40+(0.08)),[0 otherLimit]);
p2.Color="blue";
p3.Color="blue";

hold off

xlabel("Strain - [-]")
ylabel("Stress - [N \cdot m^{-2}]")
set(gca, 'FontSize', 30);


