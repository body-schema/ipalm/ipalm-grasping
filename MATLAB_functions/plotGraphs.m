function plotGraphs(mode,gripper,varargin)
% PLOTGRAPHSTOGETHER2 function for plotting measurement data.
%   PLOTGRAPHSTOGETHER2(MODE,GRIPPER) plots all the measurments.
%   PLOTGRAPHSTOGETHER2(MODE,GRIPPER,OBJECT) plots the specified object.
%   PLOTGRAPHSTOGETHER2(MODE,GRIPPER,OBJECT,SPEED) plots the specified speed.
%


% version check
if(version('-release')<"2019a")
    disp(version('-release'))
    warning("You have a old version of matlab. Please consider updating. This function will not work correctly.");
end

% input parser for the input parameters/arguments
ip= inputParser;

% two kinds of data, we can plot a function of robot output related to compression
% and a function of robot output related to time
validModes = {'compression','time'};
checkMode = @(x) any(validatestring(x,validModes));


% two kinds of robots/grippers
% Kinova gen3 equipped with robotiq 2f-85 gripper, the output is current
% UR10e equipped with OnRobot RG6 gripper, the output is gripping force
validGrippers = {'2F85','RG6','FT300','SoftHand'};
checkGripper = @(x) any(validatestring(x,validGrippers));

% object filter
defaultObject='';
checkObject = @(x)(isstring(x)||ischar(x));

% speed filter
defaultSpeed=double.empty;
checkSpeed = @(x)(isnumeric(x) && isscalar(x) && (x >= 0));

addRequired(ip,'mode',checkMode);
addRequired(ip,'gripper',checkGripper)
addOptional(ip,'object',defaultObject,checkObject)
addOptional(ip,'speed',defaultSpeed,checkSpeed)

ip.KeepUnmatched = true;

% parsing the inputs and checking for errors
try
    parse(ip,mode,gripper,varargin{:});
catch E
    %     error('Not enough input arguments, please make sure you got it right or consult the manual.');
    rethrow(E)
end

% results from the parsing
% might chceck for empty here, but it's being done further down the
% function
funMod=ip.Results.mode;
funGrip=ip.Results.gripper;
funObj=ip.Results.object;
funSpe=ip.Results.speed;

% loading the object database
db = matfile("DB.mat");
db=db.database;
% Geting all the .mat files in the current folder
data=dir('*.mat');
% Separating the filenames (with path and everything) from the data structure
fnames={data.name};
% preparing the index for the filters
index=false(1,length(data));

% some info for the user
if(isempty(funObj))
    warning("Object not specified. Showing every object.")
end

if(isempty(funSpe))
    warning("Speed not specified. Showing every speed setting.")
end

% starting the filtering process
for n=1:length(fnames)
    %     getting just the file name
    [~,name,~] = fileparts(cell2mat(fnames(n)));
    %     splitting the filename, now we can check the filters
    des=strsplit(name,'-');
    
    %     first we check for the robot/gripper
    switch lower(funGrip)
        case {"2f85"}
            %     get the info from the file name
            %     filename format for the Robotiq 2F-85:
            %     1. robot name
            %     date of the measurement
            %     2. year
            %     3. month
            %     4. day
            %     5. hour
            %     6. minute
            %     7. material/object id
            %     8. object width
            %     9. gripping speed
            
            if(~contains(des(1),"2f85",'IgnoreCase',true))
                %   file is not a kinova measurement, ignore it
                continue;
            end
            %   filtering the specified objects/material
            if(isempty(funObj))
                %                 disp("Object not specified. Showing every object.")
                index(n)=true;
            else
                if(contains(des(7),funObj,'IgnoreCase',true))
                    index(n)=true;
                else
                    index(n)=false;
                end
            end
            %   filtering the specified speed
            if(isempty(funSpe)&&index(n))
                %   disp("Speed not specified. Showing every speed setting.")
                index(n)=true;
            else
                if(any(str2double(des(9))/10==funSpe)&&index(n))
                    index(n)=true;
                else
                    index(n)=false;
                end
            end
        case {"rg6"}
            %     get the info from the filename
            %     filename format for the OnRobot RG6
            %     1. robot name
            %     date of the measurement
            %     2. year
            %     3. month
            %     4. day
            %     5. material/object id
            %     6. object width
            if(~contains(des(1),"RG6",'IgnoreCase',true))
                % file is not a UR10e measurement, ignore it
                continue;
            end
            %   filtering the specified objects/material
            if(isempty(funObj))
                index(n)=true;
            else
                if(contains(des(5),funObj,'IgnoreCase',true))
                    index(n)=true;
                else
                    index(n)=false;
                end
            end
            
        case {"ft300"}
            %     get the info from the filename
            %     filename format for the Robotiq FT 300
            %     1. robot name
            %     date of the measurement
            %     2. year
            %     3. month
            %     4. day
            %     5. material/object id
            %     6. object width
            if(~contains(des(1),"ft300",'IgnoreCase',true))
                %   file is not a ft300 measurement, ignore it
                continue;
            end
            %   filtering the specified objects/material
            if(isempty(funObj))
                %                 disp("Object not specified. Showing every object.")
                index(n)=true;
            else
                if(contains(des(5),funObj,'IgnoreCase',true))
                    index(n)=true;
                else
                    index(n)=false;
                end
            end
            
        case {"softhand"}
            %     get the info from the filename
            %     filename format for the qbrobotics SoftHand
            %     1. robot name
            %     date of the measurement
            %     2. year
            %     3. month
            %     4. day
            %     5. material/object id
            %     6. object width
            %     7. gripping speed
            if(~contains(des(1),"SoftHand",'IgnoreCase',true))
                %   file is not a softhand measurement, ignore it
                continue;
            end
            %   filtering the specified objects/material
            if(isempty(funObj))
                %                 disp("Object not specified. Showing every object.")
                index(n)=true;
            else
                if(contains(des(7),funObj,'IgnoreCase',true))
                    index(n)=true;
                else
                    index(n)=false;
                end
            end
            %               filtering the specified speed
            if(isempty(funSpe)&&index(n))
                %   disp("Speed not specified. Showing every speed setting.")
                index(n)=true;
            else
                if(any(str2double(des(9))/10==funSpe)&&index(n))
                    index(n)=true;
                else
                    index(n)=false;
                end
            end
            
        otherwise
            %   Wrong robot or gripper inputs
            warning("Robot/gripper argument not recognized.")
            break;
    end
    
end

if(all(~index))
    %     all data were filtered out, no data to plot, quit the function
    warning("No data to plot for current parameters. Please change your input or current folder.");
    return;
end

% actually filtering the data
data=data(index);
% and the filenames
fnames=fnames(index);
% preallocation for the stiffnesses and speeds
stiffnesses=zeros(length(data),1);
speeds=zeros(length(data),1);

for d=1:length(data)
    [~,name,~] = fileparts(cell2mat(fnames(d)));
    des=strsplit(name,'-');
    %     depending on the robot, we need to look it up in the filename and
    %     database by the id of the object/material
    try
        switch lower(funGrip)
            case {"2f85"}
                nums=regexp(cell2mat(des(7)),'[0-9]','match');
                stiff=append(nums(end-1),nums(end));
                stiffnesses(d)=str2double(stiff);
            case {"rg6"}
                nums=regexp(cell2mat(des(5)),'[0-9]','match');
                stiff=append(nums(end-1),nums(end));
                stiffnesses(d)=str2double(stiff);
            case {"ft300"}
                nums=regexp(cell2mat(des(5)),'[0-9]','match');
                stiff=append(nums(end-1),nums(end));
                stiffnesses(d)=str2double(stiff);
            case {"softhand"}
                nums=regexp(cell2mat(des(7)),'[0-9]','match');
                stiff=append(nums(end-1),nums(end));
                stiffnesses(d)=str2double(stiff);
                
            otherwise
                warning("Robot/gripper argument not recognized.")
                break;
        end
    catch
        warning("No stiffnes info in the name")
    end
    try
        switch lower(funGrip)
            case {"2f85"}
                speeds(d)=str2double(des(9));
            case {"softhand"}
                speeds(d)=str2double(des(9));
            case {"ft300"}
                % no additional speed options implemented yet
            case {"rg6",}
                % no speed option, ignore
            otherwise
                warning("Robot/gripper argument not recognized.")
        end
    catch
        warning("No speeds info in the name")
    end
    
end

% sort the data based on the stiffness
[stiffnesses,I]=sort(stiffnesses);
data=data(I);
fnames=fnames(I);
leg=cell(length(data),1);

% prepare the figure
fig=figure();
hold on
% plotting the data one by one
for d=1:length(data)
    [~,name,ext] = fileparts(cell2mat(fnames(d)));
    des=strsplit(name,'-');
    
    %     loading the mat-file
    m = matfile(append(name,ext));
    switch lower(funMod)
        case{"compression","c"}
            % compression mode
            switch lower(funGrip)
                case {"2f85"}
                    % preparing the legend
                    id=find(strcmpi([db.id], des(7))==1);
                    if(stiffnesses(d)~=0)
                        leg(d)=append(cellstr(db(id).name),cellstr(", p="),cellstr(string(stiffnesses(d)/10)),cellstr(" kPa"));
                    else
                        leg(d)=cellstr(db(id).name);
                    end
                    %
                    if(speeds(d)~=0)
                        leg(d)=append(leg(d),cellstr(", v="),cellstr(string(speeds(d)*10)),cellstr("%"));
                    end
                    
                    % parsing the width of the object
                    objectwidth=str2double(des(8));
                    
                    pos=m.position;
                    % calculating the compression
                    pos=objectwidth-(1-pos)*85;
                    cur=m.current;
                    % getting the max current
                    [~,I]=max(cur);
                    %  cutting out the unwanted extra data
                    pos(I+1:end)=[];
                    cur(I+1:end)=[];
                    % plotting the graphs, usually we want everything from
                    % zero and up, but the empty gripper max position is zero
                    % so therfore this condition
                    
                    
                    cur=cur*235;
                    pos=pos/objectwidth;
                    area=(825)/1000000;
                    cur=cur/area;
                    if(all(pos<=0))
                        p=plot(pos,cur);
                        % p=scatter(pos,cur,30);
                        % [pfit,S]=polyfit(pos(1:50),cur(1:50),1);
                        % disp(pfit)
                        % [pval,delta] = polyval(pfit,pos,S);
                        % plot(pos,pval,'HandleVisibility','off','LineWidth',2,'Color',db(id).color);
                    else
                        pos1=pos(pos>=0);
                        cur1=cur(pos>=0);
                        p=plot(pos1,cur1);
                        % p=scatter(pos(pos>=0),cur(pos>=0),30);
                        % [pfit,S]=polyfit(pos1(1:30),cur1(1:30),1);
                        % disp(pfit)
                        % [pval,delta] = polyval(pfit,pos1,S);
                        % plot(pos1,pval,'HandleVisibility','off','LineWidth',2,'Color',db(id).color);
                    end
                    p.Color=db(id).color;
                    
                    
                    %switch speeds(d)
                    %    case 3
                    %        p.LineStyle=':';
                    %    case 5
                    %        p.LineStyle='-.';
                    %    case 8
                    %        p.LineStyle='--';
                    %    otherwise
                    %        p.LineStyle='-';
                    %end
                    
                case {"rg6"}
                    % preparing the legend
                    id=find(strcmpi([db.id], des(5))==1);
                    if(stiffnesses(d)~=0)
                        leg(d)=append(cellstr(db(id).name),cellstr(", p="),cellstr(string(stiffnesses(d)/10)),cellstr(" kPa"));
                    else
                        leg(d)=cellstr(db(id).name);
                    end
                    % parsing the width of the object
                    objectwidth=str2double(des(6));
                    wid=m.width;
                    
                    forc=m.force;
                    forc=forc((wid<=objectwidth));
                    % calculating the compression
                    wid=objectwidth-wid(wid<=objectwidth);
                    wid=wid/objectwidth;
                    area=(866)/1000000;
                    forc=forc/area;
                    
                    p=scatter(wid,forc,30);
                    p.MarkerEdgeColor=db(id).color;
                    
                case {"ft300"}
                    % preparing the legend
                    id=find(strcmpi([db.id], des(5))==1);
                    if(stiffnesses(d)~=0)
                        leg(d)=append(cellstr(db(id).name),cellstr(", p="),cellstr(string(stiffnesses(d)/10)),cellstr(" kPa"));
                    else
                        leg(d)=cellstr(db(id).name);
                    end
                    % parsing the width of the object
                    objectwidth=str2double(des(6));
                    pos=-m.position*1000;
                    forc=-m.force;
                    
                    fo=forc(forc>1);
                    pos=pos(forc>1);
                    pos=pos-pos(1);
                    pos=pos(pos>=0);
                    fo=fo(pos>=0);
                    %comment this block for hysteresis to show 
                    %%%%%%%%%%%%%%%%%%%%
                    [~,i]=max(fo);                     
                    pos=pos(1:i);  
                    fo=fo(1:i);    
                    %%%%%%%%%%%%%%%%%%%%
                    
                    pos=pos/objectwidth;
                    if(db(id).type=="poly")
                        fo=fo*1000000/(9025);
                    elseif(db(id).type=="cube")
                        fo=fo*1000000/(objectwidth*objectwidth);
                    elseif(db(id).type=="die")
                        fo=fo*1000000/(objectwidth*objectwidth);
                    end
                    
                    p=plot(pos,fo);
                    p.Color=db(id).color;
                    
                case {"softhand"}
                    % preparing the legend
                    id=find(strcmpi([db.id], des(7))==1);
                    if(stiffnesses(d)~=0)
                        
                        try
                            leg(d)=append(cellstr(db(id).name),cellstr(", p="),cellstr(string(stiffnesses(d)/10)),cellstr(" kPa"));
                        catch
                            leg(d)=append(des(7),cellstr(", p="),cellstr(string(stiffnesses(d)/10)),cellstr(" kPa"));
                        end
                    else
                        try
                            leg(d)=cellstr(db(id).name);
                        catch
                            leg(d)=cellstr(des(7));
                        end
                    end
                    if(speeds(d)~=0)
                        leg(d)=append(leg(d),cellstr(", v="),cellstr(string(speeds(d)*10)),cellstr("%"));
                    end
                    
                    pos = (m.position-200)/18600;
                    cur = m.current;
                    
                    p=plot(pos*100,cur);
                    p.Color=db(id).color;
                    
                otherwise
                    % Wrong Robot/gripper parameter, quit the function
                    warning("Robot/gripper argument not recognized.")
                    delete(fig);
                    return;
            end
            
        case{"time","t"}
            % time mode
            % only Robotiq 2F-85 and qbrobotics SoftHand supported as of now
            switch lower(funGrip)
                case {"2f85"}
                    id=find(strcmpi([db.id], des(7))==1);
                    if(stiffnesses(d)~=0)
                        leg(d)=append(cellstr(db(id).name),cellstr(", p="),cellstr(string(stiffnesses(d)/10)),cellstr(" kPa"));
                    else
                        leg(d)=cellstr(db(id).name);
                    end
                    
                    pos=m.position;
                    tim=m.time;
                    p=plot(tim,pos);
                    p.Color=db(id).color;
                    
                case {"softhand"}
                    id=find(strcmpi([db.id], des(7))==1);
                    if(stiffnesses(d)~=0)
                        try
                            leg(d)=append(cellstr(db(id).name),cellstr(", p="),cellstr(string(stiffnesses(d)/10)),cellstr(" kPa"));
                        catch
                            leg(d)=append(cellstr(des(7)),cellstr(", p="),cellstr(string(stiffnesses(d)/10)),cellstr(" kPa"));
                        end
                    else
                        try
                            leg(d)=cellstr(db(id).name);
                        catch
                            leg(d)=cellstr(des(7));
                        end
                    end
                    if(speeds(d)~=0)
                        leg(d)=append(leg(d),cellstr(", v="),cellstr(string(speeds(d)*10)),cellstr("%"));
                    end
                    pos=(m.position-200)/18600;
                    tim=m.time;
                    i=find(pos>0.01,1);
                    tim=tim-tim(i);
                    
                    p=scatter(tim(tim>0 & tim<10),pos(tim>0 & tim<10)*100,30);
                    p.MarkerEdgeColor=db(id).color;
                    
                case {"rg6"}
                    warning("Selected mode not supported for this gripper.")
                    delete(fig);
                    return;
                case {"ft300"}
                    warning("Selected mode not supported for this gripper.")
                    delete(fig);
                    return;
                otherwise
                    % Wrong Robot/gripper parameter, quit the function
                    warning("Robot/gripper argument not recognized. Selected mode not supported.")
                    delete(fig);
                    return;
            end
    end
    p.LineWidth=5;
    
end
hold off
legend(leg,'FontSize', 30);
set(gca, 'FontSize', 30);

% settings for the plots
switch funMod
    case{"compression","c"}
        switch lower(funGrip)
            case {"2f85"}
                xlabel("Strain - [-]",'FontSize',35)
                ylabel("Stress - [N \cdot m^{-2}]",'FontSize',35)
                
            case {"rg6"}
                xlabel("Strain - [-]",'FontSize',35)
                ylabel("Stress - [N \cdot m^{-2}]",'FontSize',35)
                
            case {"ft300"}
                xlabel("Strain - [-]",'FontSize',35)
                ylabel("Stress - [N \cdot m^{-2}]",'FontSize',35)
                
            case {"softhand"}
                ylabel('Current - [mA]','FontSize',35);
                xlabel('Motor position - [-]','FontSize',35);                
        end
    case{"time","t"}
        switch lower(funGrip)            
            case {"2f85"}
                xlabel('Time - [s]','FontSize',35);
                ylabel('Gripper position - [mm]','FontSize',35);
                
            case {"softhand"}
                xlabel('Time - [s]','FontSize',35);
                ylabel('Motor position - [-]','FontSize',35);                
        end
end
end