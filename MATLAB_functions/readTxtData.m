function readTxtData()
% Function for reading the raw data and saving it as a .mat file
% only works for raw data format with object id in the filename

% Identifies the gripper and the object and creates a .mat structre of data
% containing object inofromation, gripper information, closing speed, etc.

% Unknown/unidentified info is replaced with NaNs

data=dir('*.txt'); %makes a list of all the .txt files


for d=1:length(data) %goes throught the list one by one
    [~,name,~] = fileparts(data(d).name);
    fileID=fopen(data(d).name); %opens the .txt file
    name_parts=strsplit(name,'-');
    % gripper id is always the first parameter in the filename
    gripper_name=cell2mat(name_parts(1));
    
    switch lower(gripper_name)
        case {"2f85"}
            %     get the info from the file name
            %     filename format for the Robotiq 2F-85:
            %     1. robot name
            %     date of the measurement
            %     2. year
            %     3. month
            %     4. day
            %     5. hour
            %     6. minute
            %     7. material/object id
            %     8. object width
            %     9. gripping speed
            Cell = textscan(fileID,'%f %f %f','CommentStyle','//'); %loads everything into a cell, it ignores comments //
            fclose(fileID); %closes the file
            
            %             DATA
            tmp.time=cell2mat(Cell(1));
            tmp.position=cell2mat(Cell(2));
            tmp.current=cell2mat(Cell(3));
            %             INFO
            tmp.gripper="2F85";
            tmp.robot="Kinova Gen3";
            tmp.date=[str2double(name_parts(2:6)), 0];
            tmp.object_id=string(name_parts(7));
            tmp.object_width=str2double(name_parts(8));
            tmp.closing_speed=str2double(name_parts(9))/10;
            
        case {"rg6"}
            %     get the info from the filename
            %     filename format for the OnRobot RG6
            %     1. robot name
            %     date of the measurement
            %     2. year
            %     3. month
            %     4. day
            %     5. material/object id
            %     6. object width
            Cell = textscan(fileID,'%f %f %f','CommentStyle','//'); %loads everything into a cell, it ignores comments //
            fclose(fileID); %closes the file
            %             DATA
            tmp.id=cell2mat(Cell(1));
            tmp.width=cell2mat(Cell(2));
            tmp.force=cell2mat(Cell(3));
            %             INFO
            tmp.gripper="RG6";
            tmp.robot="UR UR10e";
            tmp.date=[str2double(name_parts(2:4)), 0, 0, 0];
            tmp.object_id=string(name_parts(5));
            tmp.object_width=str2double(name_parts(6));
            tmp.closing_speed=NaN;
            
        case {"ft300"}
            %     get the info from the filename
            %     filename format for the Robotiq FT 300
            %     1. robot name
            %     date of the measurement
            %     2. year
            %     3. month
            %     4. day
            %     5. material/object id
            %     6. object width
            Cell = textscan(fileID,'%f %f %f %f','CommentStyle','//'); %loads everything into a cell, it ignores comments //
            fclose(fileID); %closes the file
            %             DATA
            tmp.time=cell2mat(Cell(1));
            tmp.position=cell2mat(Cell(2));
            tmp.force=cell2mat(Cell(3));
            tmp.torque=cell2mat(Cell(4));
            %             INFO
            tmp.gripper="FT300";
            tmp.robot="UR UR10e";
            tmp.date=[str2double(name_parts(2:4)), 0, 0, 0];
            tmp.object_id=string(name_parts(5));
            tmp.object_width=str2double(name_parts(6));
            tmp.closing_speed=NaN;
            
        case {"softhand"}
            %     get the info from the filename
            %     filename format for the qbrobotics SoftHand
            %     1. robot name
            %     date of the measurement
            %     2. year
            %     3. month
            %     4. day
            %     5. material/object id
            %     6. object width
            %     7. gripping speed
            Cell = textscan(fileID,'%f %f %f','CommentStyle','//'); %loads everything into a cell, it ignores comments //
            fclose(fileID); %closes the file
            %             DATA
            tmp.time=cell2mat(Cell(1));
            tmp.position=cell2mat(Cell(2));
            tmp.current=cell2mat(Cell(3));
            %             INFO
            tmp.gripper="SoftHand";
            tmp.robot="UR UR10e";
            tmp.date=[str2double(name_parts(2:4)), 0, 0, 0];
            tmp.object_id=string(name_parts(5));
            tmp.object_width=str2double(name_parts(6));
            tmp.closing_speed=NaN;
        otherwise
            fclose(fileID);
            warning("Wrong gripper parameter! No data processed.")
            return;
    end
    
    disp(name);
    save(append(name,'.mat'),'-struct','tmp'); %save the .mat file
end
end