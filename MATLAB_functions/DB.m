% A script for setting up a database of objects used in the haptic exploration
%

% The Polyurethane Foams set %
rp1725.dimensions=[118 120 41];
rp1725.density=17;
rp1725.pressure=25;
rp1725.name="RP1725";
rp1725.id="rp1725";
rp1725.type="poly";
rp1725.color='#0000FF';

rp2440.dimensions=[118 120 38];
rp2440.density=24;
rp2440.pressure=40;
rp2440.name="RP2440";
rp2440.id="rp2440";
rp2440.type="poly";
rp2440.color='#545454';

rp2865.dimensions=[118 118 38];
rp2865.density=28;
rp2865.pressure=65;
rp2865.name="RP2865";
rp2865.id="rp2865";
rp2865.type="poly";
rp2865.color='#EDB120';

rp3555.dimensions=[117 119 39];
rp3555.density=35;
rp3555.pressure=55;
rp3555.name="RP3555";
rp3555.id="rp3555";
rp3555.type="poly";
rp3555.color='#7E2F8E';

rp27045.dimensions=[117 119 40];
rp27045.density=270;
rp27045.pressure=45;
rp27045.name="RP27045";
rp27045.id="rp27045";
rp27045.type="poly";
rp27045.color='#77AC30';

rp30048.dimensions=[123 121 39];
rp30048.density=300;
rp30048.pressure=48;
rp30048.name="RP30048";
rp30048.id="rp30048";
rp30048.type="poly";
rp30048.color='#4DBEEE';

rp50080.dimensions=[121 118 39];
rp50080.density=500;
rp50080.pressure=80;
rp50080.name="RP50080";
rp50080.id="rp50080";
rp50080.type="poly";
rp50080.color='#FF00FF';

rl3529.dimensions=[119 118 40];
rl3529.density=35;
rl3529.pressure=29;
rl3529.name="RL3529";
rl3529.id="rl3529";
rl3529.type="poly";
rl3529.color='#0072BD';

rl4040.dimensions=[117 120 40];
rl4040.density=40;
rl4040.pressure=40;
rl4040.name="RL4040";
rl4040.id="rl4040";
rl4040.type="poly";
rl4040.color='#D95319';

rl5045.dimensions=[118 118 39];
rl5045.density=50;
rl5045.pressure=45;
rl5045.name="RL5045";
rl5045.id="rl5045";
rl5045.type="poly";
rl5045.color='#EDB120';

t1820.dimensions=[125 125 50];
t1820.density=18;
t1820.pressure=20;
t1820.name="T1820";
t1820.id="t1820";
t1820.type="poly";
t1820.color='#0072BD';

t2030.dimensions=[125 120 48];
t2030.density=20;
t2030.pressure=30;
t2030.name="T2030";
t2030.id="t2030";
t2030.type="poly";
t2030.color='#D95319';

t3240.dimensions=[123 123 50];
t3240.density=32;
t3240.pressure=40;
t3240.name="T3240";
t3240.id="t3240";
t3240.type="poly";
t3240.color='#EDB120';

t2545.dimensions=[125 125 50];
t2545.density=25;
t2545.pressure=45;
t2545.name="T2545";
t2545.id="t2545";
t2545.type="poly";
t2545.color='#7E2F8E';

v4515.dimensions=[118 120 40];
v4515.density=45;
v4515.pressure=15;
v4515.name="V4515";
v4515.id="v4515";
v4515.type="poly";
v4515.color='#EDB120';

v5015.dimensions=[119 120 42];
v5015.density=50;
v5015.pressure=15;
v5015.name="V5015";
v5015.id="v5015";
v5015.type="poly";
v5015.color='#7E2F8E';

gv5030.dimensions=[118 119 40];
gv5030.density=50;
gv5030.pressure=30;
gv5030.name="GV5030";
gv5030.id="gv5030";
gv5030.type="poly";
gv5030.color='#0072BD';

gv5040.dimensions=[118 118 39];
gv5040.density=50;
gv5040.pressure=40;
gv5040.name="GV5040";
gv5040.id="gv5040";
gv5040.type="poly";
gv5040.color='#D95319';

n4072.dimensions=[118 117 37];
n4072.density=40;
n4072.pressure=72;
n4072.name="N4072";
n4072.id="n4072";
n4072.type="poly";
n4072.color='#7E2F8E';

nf2140.dimensions=[105 100 50];
nf2140.density=21;
nf2140.pressure=40;
nf2140.name="NF2140";
nf2140.id="nf2140";
nf2140.type="poly";
nf2140.color='#77AC30';

% Cubes and Dice set %
bluecube.dimensions=[56 56 56];
bluecube.density=NaN;
bluecube.pressure=NaN;
bluecube.name="Blue cube";
bluecube.id="bluecube";
bluecube.type="cube";
bluecube.color=[0.254 0.763 1];

yellowcube.dimensions=[56 56 56];
yellowcube.density=NaN;
yellowcube.pressure=NaN;
yellowcube.name="Yellow cube";
yellowcube.id="yellowcube";
yellowcube.type="cube";
yellowcube.color='#EDB120';

kinovacube.dimensions=[56 56 56];
kinovacube.density=NaN;
kinovacube.pressure=NaN;
kinovacube.name="Kinova cube";
kinovacube.id="kinovacube";
kinovacube.type="cube";
kinovacube.color=[0 0.523 0.546];

pinkdie.dimensions=[75 75 75];
pinkdie.density=NaN;
pinkdie.pressure=NaN;
pinkdie.name="Pink die";
pinkdie.id="pinkdie";
pinkdie.type="die";
pinkdie.color=[1 0 0.527];

whitedie.dimensions=[59 59 59];
whitedie.density=NaN;
whitedie.pressure=NaN;
whitedie.name="White die";
whitedie.id="whitedie";
whitedie.type="die";
whitedie.color="#7b7a79";

bluedie.dimensions=[90 90 90];
bluedie.density=NaN;
bluedie.pressure=NaN;
bluedie.name="Blue die";
bluedie.id="bluedie";
bluedie.type="die";
bluedie.color="#0a68d2";

darkbluedie.dimensions=[43 43 43];
darkbluedie.density=NaN;
darkbluedie.pressure=NaN;
darkbluedie.name="Darkblue die";
darkbluedie.id="darkbluedie";
darkbluedie.type="die";
darkbluedie.color=[0.03 0 0.69];

% Selected objects from the pilot set %

carsponge.dimensions=[NaN NaN 41];
carsponge.density=NaN;
carsponge.pressure=NaN;
carsponge.name="Car Sponge 41mm";
carsponge.id="carsponge";
carsponge.type=NaN;
carsponge.color=NaN;

sponge35mm.dimensions=[NaN NaN 35];
sponge35mm.density=NaN;
sponge35mm.pressure=NaN;
sponge35mm.name="Sponge 35mm";
sponge35mm.id="sponge35mm";
sponge35mm.type=NaN;
sponge35mm.color=NaN;

sponge40mm.dimensions=[NaN NaN 40];
sponge40mm.density=NaN;
sponge40mm.pressure=NaN;
sponge40mm.name="Sponge 40mm";
sponge40mm.id="sponge40mm";
sponge40mm.type=NaN;
sponge40mm.color=NaN;

sponge70mm.dimensions=[NaN NaN 70];
sponge70mm.density=NaN;
sponge70mm.pressure=NaN;
sponge70mm.name="Sponge 70mm";
sponge70mm.id="sponge70mm";
sponge70mm.type=NaN;
sponge70mm.color=NaN;

empty.dimensions=[0 0 0];
empty.density=NaN;
empty.pressure=NaN;
empty.name="Empty gripper";
empty.id="empty";
empty.type=NaN;
empty.color="#000000";

% Inicialize the database %
database=[
    rp1725
    rp2440
    rp2865
    rp3555
    rp27045
    rp30048
    rp50080
    
    rl3529
    rl4040
    rl5045
    
    t1820
    t2030
    t3240
    t2545
    
    v4515
    v5015
    
    gv5030
    gv5040
    
    n4072
    nf2140
    
    bluecube
    yellowcube
    kinovacube
    
    pinkdie
    whitedie
    bluedie
    darkbluedie
    
    carsponge
    sponge35mm
    sponge40mm
    sponge70mm
    
    empty
    ];

% Save into a .mat file %
save(append("DB",'.mat'),'database');

