# MATLAB functions

A repository for supporting matlab functions

## Contents

* **readTxtData.m**
    * Used for preprocessing the raw data
* **plotGraphs.m**
    * Used for plotting the filtered data based on the user input
* **plotYoung.m**
    * A script for cmputing the Young modulus of the loaded data structure and plotting the relation
* **DB.mat**
    * A database structure containing information about the gripped objects
* **DB.m**
    * A script for setting up and saving the DB database

## How to use

### readTxtData()
You can run the function with the command

```
readTxtData();
```
It is meant to be used in a folder where the raw data .txt files are located.
The raw data are read and saved into a .mat files of the same name.
A file should be named for example like this:
```
2F85-2020-04-03-17-24-T2545-50-05.txt
```

The convention for the filename is as follows:
* For the OnRobot RG6 gripper
```
RG6-<YYYY>-<MM>-<DD>-<ObjectID>-<ObjectWidth>
```
* For the Robotiq 2F-85 gripper
```
2F85-<YYYY>-<MM>-<DD>-<HH>-<MM>-<ObjectID>-<ObjectWidth>-<GrippingSpeed>
```
* For the Robotiq FT 300 force/torque sensor
```
FT300-<YYYY>-<MM>-<DD>-<ObjectID>-<ObjectWidth>
```
* For the qbrobotics SoftHand gripper
```
SoftHand-<YYYY>-<MM>-<DD>-<HH>-<MM>-<ObjectID>-<ObjectWidth>-<GrippingSpeed>
```

Notice that **gripper_ID** is always the first argument and 
the **readTxtData** function can identify the experimental setup thanks to it.
The **ObjectWidth** is in mm. The **GrippingSpeed** is in %.

### plotGraphs()
You can run the function with the command

```
plotGraphs("MODE","GRIPPER_ID","OBJECT_FILTER","SPEED_FILTER");
```
The parameters are as follows:
* **MODE**, required parameter, can be one of two settings:
    * "c", "compression" - for a compression mode, will plot a relation of stress and object strain
    * "t", "time" - for a time mode, will plot a relation of force (or current) over time
* **GRIPPER_ID**, required parameter, self-explanatory, settings are:
    * "rg6"
    * "2f85"
    * "ft300"
    * "softhand"
* **OBJECT_FILTER**, a string or an array of strings, the contents are compared with the object IDs of the data files and other files are filtered out e.g.,
    * ["V","N"] will show the N, V and GV type of materials
    * "V" will show only the V and GV type of materials
    * "GV" will show only the GV type of materials
    * "GV5030" will show only one material
* **SPEED_FILTER**, double, filters the data according to the speed value in the filename when supported
 
### plotYoung 
First you have to manually load the preprocessed data file, e.g.,
```
load('2F85-2020-04-03-17-27-GV5030-40-05.mat');
```
Then you can run the script with a following command:
```
plotYoung;
```
You can modify the setting for point of interest and the neighbourhood limits.
The default settings is the interval of (0.4-0.05,0.4+0.05)

### DB database
A **DB.m** cript will initialize and save the **DB.mat** database of objects
Each object from the database has the following items:
* dimensions
    * an array of three doubles representing the object dimensions
    * [x,y,z]
* density
    * the first two (or three) numbers from the material name, if specified
    * NaN otherwise
* pressure
    * the last two numbers from the material name, if specified
    * NaN otherwise
* name
    * Object name shown in the legend when poltting
* id
    * Object/material designator
* type
    * **poly** - for the objects from the Polyurethane foams set
    * **cube** - for cubes from the Cubes and dice set
    * **dice** - for dice from the Cubes and dice set
    * NaN otherwise
* color
    * Color of the plotline (for plot function) or the marker (for scatterf function)



