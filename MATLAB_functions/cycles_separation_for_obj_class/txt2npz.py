import os
import numpy as np
rootdir = './Polyurethane-Foams-set/speed_0.68percent_1.6mm_s(15t4v)/'

for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        M=np.loadtxt(os.path.join(subdir, file))
        pos=M[:,1]
        curr=M[:,2]
        np.savez(os.path.join(subdir, file[:-4]+'.npz'),position=pos,current=curr)
        #print(M.shape)
        #print(os.path.join(subdir, file))
