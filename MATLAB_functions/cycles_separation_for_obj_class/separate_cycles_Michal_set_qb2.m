clear all
close all
% Experiment definition
%num_cycles=10;
%skip some samples
skip=true;
skip_int=[1:40,1640:1720];
avoid_planes=false;
%% 
% Read file
object='yellowcube';
index_col='18-15';
const='56';
%thresh_min=0.04;
%thresh_max=0.0001;
%interv_const=25;
%filename=[object,'/decompression_same_speed','/2F85-2020-09-24-',index_col,'-',object,'-',const,'-0.585000'];
filename=['SoftHand-2020-10-01-',index_col,'-',object,'-',const];
f=fopen(['./Michal-set(qb)2/',object,'/',filename,'.txt'],'r');
datafile=textscan(f,'%f %f %f','CommentStyle','//');
fclose(f);
time=cell2mat(datafile(1));
position=cell2mat(datafile(2))/20000;
%position=cell2mat(datafile(2));

%width=width;
current=cell2mat(datafile(3))/1000;
data_length=size(time,1);
min(position)
min(current)

if skip
position(skip_int)=nan;
current(skip_int)=nan;
end

%increase last sample of a plane
plane_len=8;
yval_thr=0.98;
if avoid_planes
    for i=plane_len+1:numel(position)-1
        if isnan(position(i-plane_len)) | isnan(position(i+1)) 
        continue
        end
        if position(i)>yval_thr & (abs(position(i)-position(i-plane_len))==0.0) & (position(i+1)<position(i)) 
            position(i)=position(i)+0.001;
        end
    end
end

%find starts
%%
%Finding peaks via spacing between data point indices.
% max_pos_val=max(position);
% temp_peaks=find(abs(position-max_pos_val)<thresh_max);
% peaks_counter=1;
% for loop_var=1:(size(temp_peaks,1)-1)
%     gap=temp_peaks(loop_var+1)-temp_peaks(loop_var);
%     if(abs(gap-(data_length/num_cycles))<(data_length/interv_const))
%         final_peaks_beg(peaks_counter,1)=temp_peaks(loop_var);
%         %disp(["Peak Number: ",num2str(peaks_counter)," Peak Index: ",num2str(temp_peaks(loop_var))])
%         peaks_counter=peaks_counter+1;
%     end
% end
[~,final_peaks_beg]=findpeaks(-position,'MinPeakDistance',30,'MinPeakHeight',-0.5);
%change first beginning 
final_peaks_beg(1)=73;
%final_peaks_beg=final_peaks_beg(1:17);
%final_peaks_beg=[1;final_peaks_beg];

%find ends
%take median from 4
% min_pos_val1=min(position(final_peaks_beg(1):end/4));
% min_pos_val2=min(position(end/4+1:end/2));
% min_pos_val3=min(position(end/2+1:(3/4)*end));
% min_pos_val4=min(position((3/4)*end+1:end));
% min_pos_val=median([min_pos_val1,min_pos_val2,min_pos_val3,min_pos_val4]);

[~,final_peaks_end]=findpeaks(position, 'MinPeakHeight',0.5,'MinPeakDistance',30);
%final_peaks_end=[final_peaks_end;numel(position)];
% temp_peaks=find(position-min_pos_val<thresh_min);
% plot(position-min_pos_val)
% peaks_counter_min=1;
% added_in_cycle=false;
% for loop_var=1:(size(temp_peaks,1)-1)
%     if added_in_cycle==false %& temp_peaks(loop_var)< temp_peaks(loop_var+2)
%     final_peaks_end(peaks_counter_min,1)=temp_peaks(loop_var);
%     added_in_cycle=true;
%     peaks_counter_min=peaks_counter_min+1;
%     end
%     gap=temp_peaks(loop_var+1)-temp_peaks(loop_var);
%     if(abs(gap-(data_length/num_cycles))<(data_length/100))
%         %final_peaks_min(peaks_counter_min,1)=temp_peaks(loop_var);
%         %disp(["Peak Number: ",num2str(peaks_counter)," Peak Index: ",num2str(temp_peaks(loop_var))])
%         added_in_cycle=false;
%     end
% end


% figure
% plot(index,position,'b')
% hold on
% plot(index(final_peaks_beg),position(final_peaks_beg),'ko')
% plot(index(final_peaks_end),position(final_peaks_end),'ko')
% hold on
% plot(index,current,'r')

figure
plot(position,'b')
hold on
plot(final_peaks_beg,position(final_peaks_beg),'ko')
plot(final_peaks_end,position(final_peaks_end),'ko')
hold on
plot(current,'r')
numel(final_peaks_beg)
numel(final_peaks_end)
for i=1:length(final_peaks_beg)
    start_idx=final_peaks_beg(i);
    end_idx=final_peaks_end(i);
    
    index_s=time(start_idx:end_idx);
    current_s=current(start_idx:end_idx);
    position_s=position(start_idx:end_idx);

    M=[index_s,position_s,current_s];
%     % Plot raw data
%      figure
%      plot(M(:,1),M(:,2),'b.')
%      figure()
      dlmwrite(['./Michal-set(qb)2/',object,'/',filename,'_',num2str(start_idx),'-',num2str(end_idx),'.txt'], M, 'delimiter',' ')
    
end
clear all


