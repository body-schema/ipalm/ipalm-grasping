function readTxtData()
% Function for reading the raw data and saving it as a .mat file
% only works for raw data format with time, position, current !!!

%Can add some arguments here
data=dir('*.txt'); %makes a list of all the .txt files

for d=1:length(data) %goes throught the list one by one
    [~,name,~] = fileparts(data(d).name);
    fileID=fopen(data(d).name); %opens the .txt file
    Cell = textscan(fileID,'%f %f %f','CommentStyle','//'); %loads everything into a cell, it ignores comments //
    fclose(fileID); %closes the file
    
    tmp.time=cell2mat(Cell(1));
    tmp.position=cell2mat(Cell(2));
    tmp.current=cell2mat(Cell(3));
    
    save(append(name,'.mat'),'-struct','tmp'); %save the .mat file
end
end