clear;
close all;

load('Kinova2020_01_15_BrownCube.mat')
load('Kinova2020_01_15_Empty.mat')
load('Kinova2020_01_15_KinovaCube.mat')

pos1=cell2mat(Kinova2020_01_15_BrownCube.position);
cur1=cell2mat(Kinova2020_01_15_BrownCube.current);

pos2=cell2mat(Kinova2020_01_15_Empty.position);
cur2=cell2mat(Kinova2020_01_15_Empty.current);

pos3=cell2mat(Kinova2020_01_15_KinovaCube.position);
cur3=cell2mat(Kinova2020_01_15_KinovaCube.current);




x{1} = (1-pos1)*85;
y{1} = cur1;

x{2} = (1-pos2)*85;
y{2} = cur2;

x{3} = (1-pos3)*85;
y{3} = cur3;



% figure();
% % slm=cell(1,2);
% % for i = 1:length(x)
% %     slm{i} = slmengine(x{i},y{i},'degree',1,'knots',3, 'interiorknots','free','plot','on');
% %     slmeval(1.3,slm{i});
% % end
% close all;
figure()
hold on;
% 
% for i = 1:length(x)
%     scatter(x{i},y{i},8);
% end
%empty hand
%    xrange = slm{i}.knots
%    xev = linspace(xrange(1),xrange(2),size(x{i},1))
%    yline1 = a1*xev+b1;
%    plot(xev,yline1);hold on;
%    xev = linspace(xrange(2),xrange(3),size(x{i},1))
%    yline2 = a2*xev+b2;
%s    plot(xev,yline2);hold on;

for i = 1:length(x)
%     [a1,a2,b1,b2] = find_lin_coef_from_slm(slm{i});
%     %[a1,a2,a3,b1,b2,b3] = find_lin_coef_from_slm3(slm{i})
%     %find first equation
%     
%     %find second equation
%     xrange = slm{i}.knots;
%     xev = linspace(xrange(1),xrange(2),size(x{i},1));
%     yline1 = a1*xev+b1;
%     plot(xev,yline1);
%     xev = linspace(xrange(2),xrange(3),size(x{i},1));
%     yline2 = a2*xev+b2;
%     plot(xev,yline2);
% plot(x(i),y(i))
    %xev = linspace(xrange(3),xrange(4),size(x{i},1))
    %yline2 = a3*xev+b3;
    %plot(xev,yline3);hold on;
    plot(x{i},y{i})
end

hold off;


legend('Brown Cube','empty','Kinova Cube');
% legend('Brown Cube');
xlabel('Gripper position [mm]');
% axis([0 1 0 900]);
ylabel('Relative current [%]');
%set(gca,'Xdir','reverse')
