clear;
close all;

load('Kinova2020_01_20_17_28.mat')
load('Kinova2020_01_20_17_55.mat')
load('Kinova2020_01_20_17_45.mat')

pos1=cell2mat(Kinova2020_01_20_17_28.position);
cur1=cell2mat(Kinova2020_01_20_17_28.current);

pos2=cell2mat(Kinova2020_01_20_17_55.position);
cur2=cell2mat(Kinova2020_01_20_17_55.current);

pos3=cell2mat(Kinova2020_01_20_17_45.position);
cur3=cell2mat(Kinova2020_01_20_17_45.current);




x{1} = 50-(1-pos1(cur1>0.15))*85;
y{1} = cur1(cur1>0.15);

x{2} = 30-(1-pos2(cur2>0.22))*85;
y{2} = cur2(cur2>0.22);

x{3} = 56.5-(1-pos3(cur3>0.16))*85;
y{3} = cur3(cur3>0.16);



% figure();
% slm=cell(1,2);
% for i = 1:length(x)
%     slm{i} = slmengine(x{i},y{i},'degree',1,'knots',3, 'interiorknots','free','plot','on');
%     slmeval(1.3,slm{i});
% end
close all;
figure()
hold on;
for i = 1:length(x)
 
    scatter(x{i},y{i},15);
   
end
for i = 1:length(x)
   
    X = [ones(length(x{i}),1) x{i}];
    b = X\y{i};
    yCalc = X*b;
    plot(x{i},yCalc)
end

%empty hand
%    xrange = slm{i}.knots
%    xev = linspace(xrange(1),xrange(2),size(x{i},1))
%    yline1 = a1*xev+b1;
%    plot(xev,yline1);hold on;
%    xev = linspace(xrange(2),xrange(3),size(x{i},1))
%    yline2 = a2*xev+b2;
%s    plot(xev,yline2);hold on;

% for i = 1:length(x)
%     [a1,a2,b1,b2] = find_lin_coef_from_slm(slm{i});
%     %[a1,a2,a3,b1,b2,b3] = find_lin_coef_from_slm3(slm{i})
%     %find first equation
%     
%     %find second equation
%     xrange = slm{i}.knots;
%     xev = linspace(xrange(1),xrange(2),size(x{i},1));
%     yline1 = a1*xev+b1;
%     plot(xev,yline1);
%     xev = linspace(xrange(2),xrange(3),size(x{i},1));
%     yline2 = a2*xev+b2;
%     plot(xev,yline2);
%     %xev = linspace(xrange(3),xrange(4),size(x{i},1))
%     %yline2 = a3*xev+b3;
%     %plot(xev,yline3);hold on;
% end
hold off;


legend('Brown Cube','Green Cube','Kinova Cube','FontSize',20);
% legend('Brown Cube');
xlabel('Object compression [mm]','FontSize',20);
axis([0 40 0 1]);
set(gca, 'FontSize', 20);
ylabel('Gripper current [A]','FontSize',20);
%set(gca,'Xdir','reverse')
