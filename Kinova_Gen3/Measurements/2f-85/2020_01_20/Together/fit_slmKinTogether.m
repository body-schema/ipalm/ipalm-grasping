clear;
close all;

load('Kinova2020_01_20_17_28.mat')
load('Kinova2020_01_20_17_55.mat')
load('Kinova2020_01_20_17_45.mat')

pos1=cell2mat(Kinova2020_01_20_17_28.position);
cur1=cell2mat(Kinova2020_01_20_17_28.current);

pos2=cell2mat(Kinova2020_01_20_17_55.position);
cur2=cell2mat(Kinova2020_01_20_17_55.current);

pos3=cell2mat(Kinova2020_01_20_17_45.position);
cur3=cell2mat(Kinova2020_01_20_17_45.current);




x{1} = (1-pos1(1:199))*85;
y{1} = cur1(1:199);

x{2} = (1-pos2(1:193))*85;
y{2} = cur2(1:193);

x{3} = (1-pos3(1:209))*85;
y{3} = cur3(1:209);



figure();
slm=cell(1,2);
for i = 1:length(x)
    slm{i} = slmengine(x{i},y{i},'degree',1,'knots',3, 'interiorknots','free','plot','on');
    slmeval(1.3,slm{i});
end
close all;
figure()
hold on;
for i = 1:length(x)
    scatter(x{i},y{i},15);
end
%empty hand
%    xrange = slm{i}.knots
%    xev = linspace(xrange(1),xrange(2),size(x{i},1))
%    yline1 = a1*xev+b1;
%    plot(xev,yline1);hold on;
%    xev = linspace(xrange(2),xrange(3),size(x{i},1))
%    yline2 = a2*xev+b2;
%s    plot(xev,yline2);hold on;

for i = 1:length(x)
    [a1,a2,b1,b2] = find_lin_coef_from_slm(slm{i});
    %[a1,a2,a3,b1,b2,b3] = find_lin_coef_from_slm3(slm{i})
    %find first equation
    
    %find second equation
    xrange = slm{i}.knots;
    xev = linspace(xrange(1),xrange(2),size(x{i},1));
    yline1 = a1*xev+b1;
    plot(xev,yline1);
    xev = linspace(xrange(2),xrange(3),size(x{i},1));
    yline2 = a2*xev+b2;
    plot(xev,yline2);
    %xev = linspace(xrange(3),xrange(4),size(x{i},1))
    %yline2 = a3*xev+b3;
    %plot(xev,yline3);hold on;
end
hold off;


legend('Brown Cube','Green Cube','Kinova Cube','FontSize',20);
% legend('Brown Cube');
xlabel('Gripper position - [mm]','FontSize',20);
axis([0 90 0 1]);
set(gca, 'FontSize', 20);
ylabel('Gripper current - [A]','FontSize',20);
%set(gca,'Xdir','reverse')
