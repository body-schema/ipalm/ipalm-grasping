function plotGraphsTogether(objectWidth,title)
speeds={'*v03','*v05','*v08','*v10'};
speedTitles={'30%','50%','80%','100%'};
speedLines={':','-.','--','-'};
speedColors={'#0072BD','#D95319','#A2142F','#7E2F8E'};


leg=cell(length(speeds),1);
figure();
for s=1:length(speeds)
    
    data=dir(append(cell2mat(speeds(s)),'.mat'));
    leg(s)={append('closing speed ',cell2mat(speedTitles(s)))};
    hold on
    
    for d=1:1 %length(data)
        [~,name,ext] = fileparts(data(d).name);
        m = matfile(append(name,ext));
        pos=m.position;
        pos=objectWidth-(1-pos)*85;
        cur=m.current;
        
        p=plot(pos(pos>0),cur(pos>0));
        
        p.LineStyle=cell2mat(speedLines(s));
        p.Color=cell2mat(speedColors(s));
        p.LineWidth=1.25;
    end
    hold off    
    
end
title(title);
legend((leg));
set(gca, 'FontSize', 15);
xlabel('Object compression [mm]','FontSize',15);
% axis([0 40 0 1]);
ylabel('Gripper current [A]','FontSize',15);
end