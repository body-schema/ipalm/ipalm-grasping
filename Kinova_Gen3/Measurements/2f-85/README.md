# Kinova Gen3 robot with a Robotiq 2F-85 gripper

## Matlab files
Since 20/02/2020 the raw data also contain the time variable. It was originaly in [ns], but just [s] proved to be more convenient.
Each folder contains **.mat** files with corresponding measurment data. These files can be used in MATLAB for further processing.


## Matlab functions
You can also run the **.m** files to get the data and the plots and fitments using the slm engine (make sure to have it included in your Matlab path).
The `20/01/2020` folder also contains the aligned-together plots to get the object compression relations.
You can utilize these prepared functions

* **readTxtData.m**
    * Reads the .txt file with raw data in format `time position current` and saves it into a .mat file.
    * Do not forget to set the current folder properly.
* **readTxtDataOld.m**
    * function used for the old data format `position current`.
    * considered redundant
* **plotGraphs.m**
    * Plots graphs from the .mat files.
    * Groups the plotlines together based on the speeds (this can/might be changed).
    * The speeds are 30%, 50%, 80%, 100% (might be loading the speeds from the filename).
    * argument is
        * **objectWidth**
            * defualt=0.0, used for aligning the object compression plot
* **plotGraphsTogether.m**
    * Plots graphs from the .mat files in a one single plot.
    * arguments are
        * **objectWidth**
            * defualt=0.0, used for aligning the object compression plot
        * **title**
            * Title of the graph.  

## Measurements
The filename is in this format
`[NameOfTheRobot]-[YYYY]-[MM]-[DD]-[ObjectName]-[ObjectWidthInMM]-[GripperClosingSpeedIn%]`

**The measurements are as follows**

* `15/01/2020`
	* initial measuring test
	* measured objects:
		* brown cube
		* empty gripper
		* kinova cube 
* `20/01/2020`
	* this measuring session was also recorded on camera
	* measured objects:
		* brown cube
		* green wooden block
		* kinova cube
* `18/02/2020`
	* multiple measurements of multiple objects
		* 3 times for each object and speed setting
	* Trying various gripping speeds
		* 30%, 50%, 80% and 100%
	* measured objects:
		* yellow kitchen sponge, width 35mm
		* sandy car sponge, width 70mm
* `20/02/2020`
	* multiple measurements of multiple objects
		* Once for each speed setting
	* Trying various gripping speeds
		* 30%, 50%, 80% and 100%
	* measured objects:
		* yellow kitchen sponge, width 35mm
		* sandy car sponge, width 70mm
		* Kinova cube, side 55-56mm
		* green kitchen sponge, width 40mm
		* yellow car sponge. width 41mm
* `23/02/2020`
	* Measuring closing speeds of an empty gripper
		* Once for each speed setting
	* Trying various gripping speeds
		* 30%, 50%, 80% and 100%
	* measured objects:
		* none, just an empty gripper
* `04/03/2020`
	* Measuring the molitan set
	* One gripping speed
		* 50%
	* measured objects:
		* 20 measured various molitan pieces
* `06/03/2020`
	* Measuring the molitan set, but several times in one measurement, closing and opening again
	* One gripping speed
		* 50%, then opening with 100% speed
	* measured objects:
		* 20 measured various molitan pieces
* `12/03/2020`
	* Measuring some other objects. varing speeds
	* Trying various gripping speeds
		*50% and 100%
	* measured objects:
		* kinova cube, pink dice and white dice
* `13/03/2020`
	* 100% closing speed
	* measured objects:
		* Cubes_Dice_set 
		* Polyurethane_Foams_set		
* `03/04/2020`
	* Simple grip - one cycle with 50% closing speed
		* Cubes_Dice_set
		* Polyurethane_Foams_set
	* Multiple_grips_and_release - multiple cycles with 50% closing speed
		* Cubes_Dice_set
		* Polyurethane_Foams_setmeasured objects:
	* measured objects: complete set of cubes and dice and polyurethane foams 


