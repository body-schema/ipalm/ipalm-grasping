function plotGraphs(objectWidth)
% here check for arguments


speeds={'*v03','*v05','*v08','*v10'};
speedTitles={'30%','50%','80%','100%'};
speedLines={':','-.','--','-'};


for s=1:length(speeds)
    
    data=dir(append(cell2mat(speeds(s)),'.mat'));
    figure();
    hold on
    leg=cell(length(data),1);
    for d=1:length(data)
        [~,name,ext] = fileparts(data(d).name);
        m = matfile(append(name,ext));
        pos=m.position;
        pos=objectWidth-(1-pos)*85;
        cur=m.current;
        
        p=plot(pos(pos>0),cur(pos>0));
        leg(d)={append('measure',num2str(d))};
        p.LineStyle=cell2mat(speedLines(s));
        p.LineWidth=1.25;
    end
    
    hold off
    title(append('Grasping speed ',speedTitles(s)));
    legend(leg);
    set(gca, 'FontSize', 15);
    xlabel('Object compression [mm]','FontSize',15);
    % axis([0 40 0 1]);
    ylabel('Gripper current [A]','FontSize',15);
    
end
end