import os
import shutil
import sys
from os import path
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F
from lstm_model import LSTM
from utilities import get_free_gpu, create_dataset, create_loader, print_confusion_matrix, float_to_str, cleanup_dir
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
position = True
current = False

labels_objects = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
              "yellowcube", "bluecube", "darkbluedie"]
labels_foams = ["GV5030", "GV5040", "N4072", "NF2140", "RL3529", "RL4040", "RL5045", "RP1725", "RP2440",
              "RP2865", "RP3555", "RP27045", "RP30048", "RP50080", "T1820", "T2030", "T2545", "T3240", "V4515", "V5015"]
labels_smaller_foams = ["NF2140", "RL5045", "RP1725", "RP30048", "RP50080", "V4515"]

models = {
    'objects': {'layer': 4, 'hidden': 256, 'input': 2, 'output': 9, 'labels': labels_objects, 'position': True, 'current': True},
    'foams': {'layer': 4, 'hidden': 128, 'input': 2, 'output': 20, 'labels': labels_foams, 'position': True, 'current': True},
    'foams-smaller-0068': {'layer': 4, 'hidden': 256, 'input': 2, 'output': 6, 'labels': labels_smaller_foams, 'position': True, 'current': True},
    'foams-smaller-1445': {'layer': 4, 'hidden': 128, 'input': 2, 'output': 6, 'labels': labels_smaller_foams, 'position': True, 'current': True},
    'foams-smaller-5085': {'layer': 4, 'hidden': 128, 'input': 2, 'output': 6, 'labels': labels_smaller_foams, 'position': True, 'current': True},
    'foams-smaller-10000': {'layer': 2, 'hidden': 128, 'input': 2, 'output': 6, 'labels': labels_smaller_foams, 'position': True, 'current': True},
    'objects-ab-pos': {'layer': 4, 'hidden': 256, 'input': 1, 'output': 9, 'labels': labels_objects, 'position': False, 'current': True},
    'foams-ab-pos*': {'layer': 4, 'hidden': 128, 'input': 1, 'output': 20, 'labels': labels_foams, 'position': False, 'current': True},
    'foams-smaller-0068-ab-pos': {'layer': 4, 'hidden': 128, 'input': 1, 'output': 6, 'labels': labels_smaller_foams, 'position': False, 'current': True},
    'objects-ab-curr': {'layer': 4, 'hidden': 256, 'input': 1, 'output': 9, 'labels': labels_objects, 'position': True, 'current': False},
    'foams-ab-curr*': {'layer': 4, 'hidden': 256, 'input': 1, 'output': 20, 'labels': labels_foams, 'position': True, 'current': False},
    'foams-smaller-0068-ab-curr': {'layer': 4, 'hidden': 256, 'input': 1, 'output': 6, 'labels': labels_smaller_foams, 'position': True, 'current': False},
}

datasets = ['objects', 'foams', 'foams-smaller-0068', 'foams-smaller-1445', 'foams-smaller-5085', 'foams-smaller-10000']
models_names = ['objects', 'foams', 'foams-smaller-0068', 'foams-smaller-1445', 'foams-smaller-5085', 'foams-smaller-10000',
                'objects-ab-pos', 'foams-ab-pos*', 'foams-smaller-0068-ab-pos', 'objects-ab-curr', 'foams-ab-curr*', 'foams-smaller-0068-ab-curr']

test_models = [8]
test_datasets = [2]

for model_num in test_models:
    for dataset_num in test_datasets:
        test_model = models_names[model_num]
        dataset = datasets[dataset_num]
        test_folder = 'DATASETs/' + dataset + "/test/"
        test_dataset, _ = create_dataset(test_folder, models[test_model]['labels'], position=models[test_model]['position'], current=models[test_model]['current'])
        trn_loader = create_loader(test_dataset, 32)
        lstm = LSTM(models[test_model]['input'], models[test_model]['layer'], models[test_model]['hidden'], models[test_model]['output'], 'cpu')
        lstm.load_state_dict(torch.load('Models/' + test_model + '.pth', map_location=torch.device('cpu')))
        lstm.eval()

        correct, total = 0, 0
        y_val_complete, y_pred_complete = [], []
        with torch.no_grad():
            for (x_trn, len_trn, y_trn) in trn_loader:
                out = lstm(x_trn, len_trn)
                preds = F.log_softmax(out, dim=1).argmax(dim=1)
                total += y_trn.size(0)
                correct += (preds == y_trn).sum().item()

                y_val_complete = y_val_complete + y_trn.tolist()
                y_pred_complete = y_pred_complete + preds.tolist()

        acc = correct / total

        slogan = "Model: " + test_model + ', Dataset: ' + dataset + f', Acc.: {acc:2.2%}'
        print(slogan)
        print_confusion_matrix('Test/' + slogan, y_val_complete, y_pred_complete, models[test_model]['labels'])






