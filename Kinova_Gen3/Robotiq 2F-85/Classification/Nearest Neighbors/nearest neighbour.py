import os
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')
from utilities import print_confusion_matrix

def make_dataset(folder, labels):
    data = {"points": [], "labels": []}

    for file in os.listdir(folder):
        if file.endswith(".txt"):
            timeseries = np.loadtxt(folder + file)
            position = timeseries[:, 0]
            current = timeseries[:, 1]
            point = np.concatenate((position, current))
            # point = current
            data["points"].append(point)

            for index, label in enumerate(labels):
                if file[22:].startswith(label):
                    data["labels"].append(index)
                    break
    return data

data = 'objects'

if data is 'objects':
    labels = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
              "yellowcube", "bluecube", "darkbluedie"]
    folders = ["DATASETs/objects/"]
elif data is 'foams':
    labels = ["GV5030", "GV5040", "N4072", "NF2140", "RL3529", "RL4040", "RL5045", "RP1725", "RP2440",
              "RP2865", "RP3555", "RP27045", "RP30048", "RP50080", "T1820", "T2030", "T2545", "T3240", "V4515", "V5015"]
    folders = ["DATASETs/foams/"]
elif data is 'foams-smaller':
    labels = ["NF2140", "RL5045", "RP1725", "RP30048", "RP50080", "V4515"]
    folders = ["DATASETs/foams-smaller-0068/", "DATASETs/foams-smaller-1445/", "DATASETs/foams-smaller-5085/", "DATASETs/foams-smaller-10000/"]



k_acc = []
for k in range(1, 70):
    acc = []
    for folder in folders:
        trn_folder = folder + "trn/"
        val_folder = folder + "val/"
        memory = make_dataset(trn_folder, labels)
        validation = make_dataset(val_folder, labels)

        knn = KNeighborsClassifier(n_neighbors=k)
        knn.fit(memory["points"], memory["labels"])
        predictions = knn.predict(validation["points"])

        correct = (predictions == validation["labels"]).sum().item()
        total = len(validation["labels"])
        acc.append(correct / total * 100)
    k_acc.append(sum(acc)/len(acc))
    print("-", end="")
    acc = []

best_k = np.argmax(np.array(k_acc)) + 1
print("Best K:", best_k, "with acc", np.max(np.array(k_acc)))
for folder in folders:
    trn_folder = folder + "trn/"
    test_folder = folder + "test/"
    memory = make_dataset(trn_folder, labels)
    test = make_dataset(test_folder, labels)

    knn = KNeighborsClassifier(n_neighbors=best_k)
    knn.fit(memory["points"], memory["labels"])
    predictions = knn.predict(test["points"])

    correct = (predictions == test["labels"]).sum().item()
    total = len(test["labels"])
    acc = correct / total * 100
    name = folder
    name = name.replace("DATASETs", "")
    name = name.replace("/", "")
    print("ACC on " + name + " test set: ", acc)
    print_confusion_matrix("Results/Dataset:" + name + f' ACC:{acc} K:{best_k}', test["labels"], predictions,
                           labels, save=True)

print(k_acc)
plt.plot(np.linspace(1, len(k_acc), len(k_acc)), k_acc)
plt.xlabel('K')
plt.ylabel('Accuracy')
plt.title('Accuracy over K')
plt.savefig(data+'-Accuracy over K.png')

