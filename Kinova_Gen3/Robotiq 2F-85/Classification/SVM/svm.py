import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.use('Agg')

from utilities import print_confusion_matrix
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from features import extract_features
from sklearn.model_selection import GridSearchCV


def make_dataset(folder, labels):
    data = {"points": [], "labels": []}

    for file in os.listdir(folder):
        if file.endswith(".txt"):
            timeseries = np.loadtxt(folder + file)
            position_features = extract_features(timeseries[:, 0])
            current_features = extract_features(timeseries[:, 1])
            point = np.concatenate((position_features, current_features))
            # point = current
            data["points"].append(point)

            for index, label in enumerate(labels):
                if file[22:].startswith(label):
                    data["labels"].append(index)
                    break
    return data

data = 'objects'

if data is 'objects':
    labels = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
              "yellowcube", "bluecube", "darkbluedie"]
    folders = ["DATASETs/objects/"]
elif data is 'foams':
    labels = ["GV5030", "GV5040", "N4072", "NF2140", "RL3529", "RL4040", "RL5045", "RP1725", "RP2440",
              "RP2865", "RP3555", "RP27045", "RP30048", "RP50080", "T1820", "T2030", "T2545", "T3240", "V4515", "V5015"]
    folders = ["DATASETs/foams/"]
elif data is 'foams-smaller':
    labels = ["NF2140", "RL5045", "RP1725", "RP30048", "RP50080", "V4515"]
    folders = ["DATASETs/foams-smaller-0068/", "DATASETs/foams-smaller-1445/", "DATASETs/foams-smaller-5085/", "DATASETs/foams-smaller-10000/"]


param_grid = {'C': np.logspace(1, 3, 4, endpoint=True),
              'gamma': np.logspace(-3, 1, 5, endpoint=True),
              'kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
              'cache_size': [10000]}

for folder in folders:
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"
    test_folder = folder + "test/"

    memory = make_dataset(trn_folder, labels)
    validation = make_dataset(val_folder, labels)
    test = make_dataset(test_folder, labels)
    memory["points"] = np.concatenate((memory["points"], validation["points"]))
    memory["labels"] = np.concatenate((memory["labels"], validation["labels"]))
    scaler = StandardScaler()
    scaler.fit(memory["points"])
    memory["points"] = scaler.transform(memory["points"])
    test["points"] = scaler.transform(test["points"])

    grid = GridSearchCV(SVC(), param_grid, refit=True, verbose=5)
    grid.fit(memory["points"], memory["labels"])
    predictions = grid.predict(test["points"])

    correct = (predictions == test["labels"]).sum().item()
    total = len(test["labels"])
    acc = correct / total * 100
    name = folder
    name = name.replace("DATASETs", "")
    name = name.replace("/", "")
    print("ACC on " + name + " test set: ", acc)
    print_confusion_matrix("Results/Dataset:" + name + f' ACC:{acc} ' + str(grid.best_params_), test["labels"], predictions,
                           labels, save=True)

