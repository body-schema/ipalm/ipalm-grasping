clear, clc;
dir1 = "Processed/time/";
dir2 = "Processed/no time/";
dir3 = "Processed/mat/";

data.directory = "Processed/mat/";
tmp  = dir(data.directory+"*.mat");
tmp = struct2cell(tmp);
todo = string(tmp(1, :));

my_max = 0;
for file = todo
    A = load(data.directory + file);
    A = cell2mat(struct2cell(A));
    tmp = size(A);
    if tmp(1) > my_max
        my_max = tmp(1)
    end
    
end

for file = todo
    A = load(data.directory + file);
    A = cell2mat(struct2cell(A));
    A(:,2) = 1.18 * A(:,2);
    tmp = size(A);
    corr = my_max - tmp(1);
    A = [linspace(A(1,1)-corr/100, A(1,1), corr).', ones(corr, 1) * A(1, 2), ones(corr, 1) * A(1, 3); A];
    new_file = strrep(file,'.mat','');
    dlmwrite(dir1+new_file+".txt", A, 'delimiter',' ');
    dlmwrite(dir2+new_file+".txt", A(:,2:3), 'delimiter',' '); 
    save(dir3+new_file, "A");  
end

for file = todo
    A = load(data.directory + file);
    A = cell2mat(struct2cell(A));
    tmp = size(A)  
end








    
    
    
    



