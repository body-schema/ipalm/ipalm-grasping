import random
import os
import shutil

path = "foams-smaller-10000/"
listed_dir = os.listdir(path)
listed_dir.sort()
trn = path + "trn/"
val = path + "val/"
test = path + "test/"
os.mkdir(trn)
os.mkdir(val)
os.mkdir(test)
labels = ["NF2140", "RL5045", "RP1725", "RP30048", "RP50080", "V4515"]

labels_dict = dict()
for label in labels:
    labels_dict[label] = list()
    
print(labels_dict)

while listed_dir:
    file = listed_dir.pop()
    if file.endswith(".txt"):
        for label in labels:
            if label in file:
                labels_dict[label].append(file)
                break
            
for label, files in labels_dict.items():
    print(label,": ", len(files))
    random.shuffle(files)
    random.shuffle(files)
    random.shuffle(files)
    #more is of course better

    samples = 15
    for file in files[0:samples]:
        shutil.move(path + file, test + file)
    for file in files[samples:2*samples]:
        shutil.move(path + file, val + file)
    for file in files[2*samples:]:
        shutil.move(path + file, trn + file)


    
    
    
    

        
    

