import os
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib
import matplotlib.pyplot as plt
import random
matplotlib.use('Agg')
from features import extract_features


def make_dataset(folder, labels):
    data = {"points": [], "labels": []}

    for file in os.listdir(folder):
        if file.endswith(".txt"):
            timeseries = np.loadtxt(folder + file)
            position_features = extract_features(timeseries[:, 0])
            current_features = extract_features(timeseries[:, 1])
            point = np.concatenate((position_features, current_features))
            data["points"].append(point)

            for index, label in enumerate(labels):
                if file[22:].startswith(label):
                    data["labels"].append(index)
                    break
    return data


labels = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
          "yellowcube", "bluecube", "darkbluedie"]
folders = ["DATASETs/objects-v068/"]

for folder in folders:
    name = folder.replace('DATASETs', '')
    name = name.replace('/', '')
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"
    test_folder = folder + "test/"
    memory = make_dataset(trn_folder, labels)
    validation = make_dataset(val_folder, labels)
    test = make_dataset(test_folder, labels)
    data = {"points": memory["points"] + validation["points"] + test["points"],
            "labels": memory["labels"] + validation["labels"] + test["labels"]}
    scaler = StandardScaler()
    data["points"] = scaler.fit_transform(data["points"])

    pca = PCA(n_components=2)
    principalComponents = pca.fit_transform(data["points"])

    #https://www.colorhexa.com/b8b8b8
    list_of_colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22',
                      '#17becf']
    list_of_colors = ['#ded141', '#1f6bb4', '#ca5f86', '#9f9f9f', '#d2d2d2', '#8c564b', '#e9e182', '#1f90b4', '#141d73']


    plt.figure()
    for i, point in enumerate(principalComponents):
        if random.random() < 0.8:
            plt.scatter(point[0], point[1], c=list_of_colors[data['labels'][i]], s=0.9)

    plt.xlabel('Principal Component 1', fontsize=15)
    plt.ylabel('Principal Component 2', fontsize=15)
    plt.title('Robotiq 2F-85', fontsize=20)
    colors_legend = dict(zip(labels, list_of_colors))
    labels = list(colors_legend.keys())
    handles = [plt.Rectangle((0, 0), 1, 1, color=colors_legend[label]) for label in labels]
    plt.legend(handles, labels, loc=1, prop={'size': 8})
    plt.savefig('Results/' + name)
    plt.clf()
