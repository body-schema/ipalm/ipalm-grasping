# Kinova Gen3 robot with a Robotiq 2F-85 gripper
Subdepository for the IPALM grasping project.

## List of contents
This depository contains two folders.

* kinova_gripping
* Measurements

## kinova_gripping
C++ source files for the ROS package used for controling the robot and the gripper.

## Measurements
Raw data, MATLAB sripts, data structures and final plots.
