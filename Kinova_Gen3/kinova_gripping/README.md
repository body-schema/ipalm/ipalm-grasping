# kinova_gripping package
ROS package for controling the Gen3 robot and the 2F-85 gripper for grasping the objects and logging the feedback measurements.

## Getting Started

These instructions will get you a copy of the package up and running on your local machine for measuring purposes.

### Prerequisites

For this all to work you need


* PC with Ubuntu (16. or 18.)
* Installed ROS (Kinetic or Melodic)
* ROS workspace set up and running with Kinova kortex_driver and kortex_vision installed
* Connection to the Kinova robot (ethernet or wireless)


### Installing
First follow the tutorials here (if you haven't already)
* [kortex_driver](https://github.com/kinovarobotics/ros_kortex)
* [kortex_vision](https://github.com/Kinovarobotics/ros_kortex_vision)

Then copy the package file into the src folder in your workspace.
The path should be something like this

```
your_catkin_workspace/src/ros_kortex/kinova_gripping
```
then you return to the root workspace folder and build all the files with
```
catkin_make
```
do not forget to
```
source devel/setup.bash
```

## Runing the node

* First you need to establish a connection with the robot.
* Then, launch a kortex driver (make sure you use the correct address)

```
roslaunch kortex_driver kortex_driver.launch ip_address:=192.168.210.126 start_rviz:=false
```
* When using the ethernet connection use
 
```
ip_address:=192.168.1.10
```

* Now, you can run the gripping node by

```
rosrun kinova_gripping kinova_gripping_node _gripper_speed:=0.005 _object_name:="empty" _logging_enabled:=true
```

* **_gripper_speed:** sets gripper closing speed
    * relative speed from 0.0 to 1.0
    * negative speed is closing the gripper, positive otherwise
* **_object_name:** sets the object name that is used for generating the filename
* **_logging_enabled:** enables or disables the logging and following saving the data into a file
    * true or false

* Or you can run the grip n compare node by

```
rosrun kinova_gripping kinova_gripncompare_node _gripper_speed:=-0.005 _threshold:=0.25
```
* This node is a work-in-progress and for now just detects the object and returns the object width

* **_gripper_speed:** sets gripper closing speed
    * relative speed from 0.0 to 1.0
    * negative speed is closing the gripper, positive otherwise
* **_threshold:** sets the current sum threshold for stopping the gripper
    * around 0.15-0.25 is the sweet spot
    * another threshold is when the current reaches 0.03 A


## Further reading
I followed these tutorials 
* [WritingPublisherSubscriber](http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c%2B%2B%29)
* [CreatingPackage](http://wiki.ros.org/ROS/Tutorials/CreatingPackage)
* [BuildingPackages](http://wiki.ros.org/ROS/Tutorials/BuildingPackages)

More details here [Gripper - Robotiq 2F-85](https://docs.google.com/document/d/17TN1J_pOTi6WGJQ23UofUn77uDG3092jMDhxPUaClKY/edit#heading=h.nqq06bl7cio9)



