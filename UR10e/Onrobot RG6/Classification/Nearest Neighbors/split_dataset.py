import random
import os
import shutil

path = "constant_length/"
listed_dir = os.listdir(path)
listed_dir.sort()
trn = path + "trn/"
val = path + "val/"
test = path + "test/"
os.mkdir(trn)
os.mkdir(val)
os.mkdir(test)
labels = ["darkbluedie", "yellowsponge", "bluedie", "pinkdie",
          "kinovacube", "whitedie", "ycbcube", "yellowcube", "bluecube"]

labels_dict = {"darkbluedie": [],"yellowsponge": [], "bluedie": [], "pinkdie": [], "kinovacube": [],
               "whitedie": [], "ycbcube": [], "yellowcube": [], "bluecube": []}

while listed_dir:
    file = listed_dir.pop()
    if file.endswith(".txt"):
        for label in labels:
            if label in file:
                labels_dict[label].append(file)
                break
            
for label, files in labels_dict.items():
    samples = 6
    print(label,": ", len(files))
    random.shuffle(files)
    random.shuffle(files)
    random.shuffle(files)
    #more is of course better

    for file in files[0:samples]:
        shutil.move(path + file, test + file)
    for file in files[samples:(2*samples)]:
        shutil.move(path + file, val + file)
    for file in files[(2*samples):]:
        shutil.move(path + file, trn + file)
    
    
    

        
    

