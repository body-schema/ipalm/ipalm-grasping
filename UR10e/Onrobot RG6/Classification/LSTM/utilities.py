#!/usr/bin/env python3
import os

import numpy as np
import torch
import torch.nn.utils.rnn as rnn_utils
from sklearn.metrics import confusion_matrix
from torch.utils.data import DataLoader

import matplotlib.pyplot as plt
# type 3 font fixed
import matplotlib

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42


def get_free_gpu():
    os.system('nvidia-smi -q -d Memory |grep -A4 GPU|grep Free >tmp')
    memory_available = [int(x.split()[2]) for x in open('tmp', 'r').readlines()]
    for index, memory in enumerate(memory_available):
        if memory > 2000:
            return index

    return np.argmax(memory_available[:-1])


def cleanup_dir(dir):
    list_of_files = os.listdir(dir)
    while len(list_of_files) > 1:
        full_path = [dir + "{0}".format(x) for x in list_of_files]
        oldest_file = min(full_path, key=os.path.getctime)
        os.remove(oldest_file)
        list_of_files = os.listdir(dir)


def float_to_str(input_float, precision=2, separator="\'"):
    """
    Formats float to string with desired precision and specified separator instead of decimal point.

    Parameters
    ----------
    input_float : float
        Input value to be formatted.
    precision : int
        Desired precision.
    separator : str
        Separator to be used instead of decimal point.

    Returns
    -------
    float_str : str
        Input formatted as string by specifications.
    """
    multiplier = 10 ** precision
    decimal = int(input_float * multiplier) - int(input_float) * multiplier

    integer_str = str(int(input_float))
    decimal_str = '{:0>2d}'.format(decimal)
    float_str = integer_str + '\'' + decimal_str
    return float_str


def print_confusion_matrix(fn, y_true, y_pred, labels, save=True):
    """
    Creates confusion matrix and optionally saves it as a file.

    Parameters
    ----------
    fn : string
        Name of the created image.
    y_true : array-like of shape (n_samples,)
        Ground truth (correct) target values.
    y_pred : array-like of shape (n_samples,)
        Estimated targets as returned by a classifier.
    labels : array
        String labeles for classes, must be ordered from 0 to C, where C is the number of classes.
    save : bool
        If True, confusion matrix is visualized and saved as an image.
    normalize : {'true', 'pred', 'all'}, default=None
        Normalizes confusion matrix over the true (rows), predicted (columns) conditions or all the population. If None, confusion matrix will not be normalized.

    Returns
    -------
    none
    """
    file_format = "pdf"
    cm = confusion_matrix(y_true, y_pred)
    cm = np.array(cm)
    row_sums = cm.sum(axis=1)
    cm = cm / row_sums[:, np.newaxis]
    if save:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax = ax.imshow(cm, cmap=plt.cm.Blues)

        for (i, j), z in np.ndenumerate(cm):
            if z == 0:
                continue
            z_str = '{:.2f}'.format(z)
            ax.text(j, i, z_str, ha='center', va='center',
                    size='xx-small', color='w')

        fig.colorbar(cax)
        ax.set_xticks(np.arange(len(labels)))
        ax.set_yticks(np.arange(len(labels)))
        ax.set_xticklabels(labels)
        ax.set_yticklabels(labels)
        plt.setp(ax.get_xticklabels(), rotation=45,
                 ha="right", rotation_mode="anchor")

        plt.title('Confusion matrix of the classifier')
        plt.xlabel('Predicted')
        plt.ylabel('True')
        fig.tight_layout()
        plt.savefig(fn + '.' + file_format, format=file_format)
        plt.close()
    else:
        print(cm)
    return 0


def create_dataset(entry_path, labels, width=True, force=True):
    dataset = []
    count = np.array([0] * len(labels))

    for file in os.listdir(entry_path):
        if file.endswith(".txt"):
            timeseries = np.loadtxt(os.path.join(entry_path, file))
            position_local = timeseries[:, 0].astype('float32')
            position_local.shape += (1,)
            current_local = timeseries[:, 1].astype('float32')
            current_local.shape += (1,)
            if width and force:
                data = torch.from_numpy(np.hstack([position_local, current_local]))
            elif width and not force:
                data = torch.FloatTensor(position_local)
            elif not width and force:
                data = torch.FloatTensor(current_local)
            else:
                print("Error in create_dataset, ablation of all data is not possible.")
                exit(0)

            for index, label in enumerate(labels):
                if file[21:].startswith(label):
                    dataset.append((data, index))
                    count[index] += 1
                    break

    return dataset, 1/count / sum(1/count)


def create_loader(dataset, batch_size):
    """
    Creates loaders from datasets with desired batch size.

    Parameters
    ----------
    dataset : list of touples
        Datasets created by the create_dataset(...) function from which the loader should be created.
    batch_size : int
        Batch size of loaders.

    Returns
    -------
    loader : DataLoader
    """

    class PadSequence:
        # This function was taken from a website:
        # https://www.codefull.org/2018/11/use-pytorchs-dataloader-with-variable-length-sequences-for-lstm-gru/
        def __call__(self, batch):
            # Let's assume that each element in "batch" is a tuple (data, label).
            # Sort the batch in the descending order
            sorted_batch = sorted(
                batch, key=lambda x: x[0].shape[0], reverse=True)
            # Get each sequence and pad it
            sequences = [x[0] for x in sorted_batch]
            sequences_padded = rnn_utils.pad_sequence(
                sequences, batch_first=True)
            # Also need to store the length of each sequence
            # This is later needed in order to unpad the sequences
            lengths = torch.LongTensor([len(x) for x in sequences])
            # Don't forget to grab the labels of the *sorted* batch
            labels = torch.LongTensor(list(map(lambda x: x[1], sorted_batch)))
            return sequences_padded, lengths, labels

    loader = DataLoader(dataset, batch_size=batch_size,
                        shuffle=True, collate_fn=PadSequence())
    return loader
