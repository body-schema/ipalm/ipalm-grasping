clear, clc;

rmdir("Processed", 's');
mkdir("Processed/mat");
mkdir("Processed/no time");
mkdir("Processed/time");

data = load("my_resources/edge.mat", '-mat');
data.off_pre = 32;
data.off_suff = 122;
    
data.active = "pre";
data.s1 = [];
data.s2 = [];
data.index = ones(1,9);
directory = "DATA/*.txt";
tmp  = dir(directory);
tmp = struct2cell(tmp);
data.todo = string(tmp(1, :));
data = process_step(data);

dispSize = get(0, 'ScreenSize'); 
figSize = [dispSize(3) dispSize(4)/6];
hfig = figure('name', data.file, 'pos', [(dispSize(3)-figSize(1))/2 (dispSize(4)-figSize(2))/2 figSize(1)  figSize(2)]);
guidata(hfig, data);
hfig.KeyPressFcn = @keyboardCallback;
hfig.WindowButtonDownFcn = @mytestcallback;
hold on
ploting(hfig)

function ret_data = process_step(data)
    bounds = 300;
    data.file = data.todo(1);
    data.todo = data.todo(2:end);
    file = fopen("DATA/"+data.file, "r");
    fgetl(file);
    fgetl(file);
    formatSpec = '%f %f %f';
    sizeA = [3 inf];
    A = fscanf(file,formatSpec,sizeA);
    A = A.';
    A(:, 2) = A(:, 2)/100;
    A(:, 3) = A(:, 3)/120;
    A = [((A(1,1)-(bounds-1)):1:A(1,1)).' ones(bounds, 1) * A(1,2) ones(bounds, 1) * A(1,3); A; (A(end,1):1:(A(end,1)+(bounds-1))).' ones(bounds, 1) * A(end,2) ones(bounds, 1) * A(1,3)];
    data.A = A;
    data.checkbox = ones(1, length(get_lag(data)));
    ret_data = data;
end

function ploting(hfig)
    bounds = 200;
    data = guidata(hfig);
    A = data.A;
    delete(data.s1);
    delete(data.s2);
    
    data.s1 = subplot(1,5,(1:4));
    set(gca,'tag',"cutting");
    hold on
    lag = get_lag(data);
    %lag(1)
    %lag(end) + length(data.edge)
    range = (lag(1)-bounds):lag(end) + length(data.edge)+bounds;
    plot(A(range,1), A(range,2), "b");
    plot(A(range,1), A(range,3), "k");
    rectangles(data);
    axis([-inf inf -1.5 1.5])
    
    data.s2 = subplot(1,5,5);
    set(gca,'tag',"selecting");
    hold on
    plot(data.edge)
    if data.active == "pre"
        plot([data.off_pre data.off_pre] ,[-1.5 1.5], "r+:");
        plot([data.off_pre + data.off_suff data.off_pre + data.off_suff] ,[-1.5 1.5], "g+:");
    else
        plot([data.off_pre data.off_pre] ,[-1.5 1.5], "g+:");
        plot([data.off_pre + data.off_suff data.off_pre + data.off_suff] ,[-1.5 1.5], "r+:");
    end
    guidata(hfig, data);
end


function keyboardCallback(src,event)
data = guidata(src);

switch event.Key  
    case 'leftarrow'
        if data.active ==  "pre"
            data.off_pre = data.off_pre - 1;
            data.off_suff = data.off_suff + 1;
        else
            data.off_suff = data.off_suff - 1;
        end
        fprintf('Pre = %d Suff = %d  \n', data.off_pre, data.off_suff);
        
    case 'rightarrow'
        if data.active ==  "pre"
            data.off_pre = data.off_pre + 1;
            data.off_suff = data.off_suff - 1;
        else
            data.off_suff = data.off_suff + 1;
        end
        fprintf('Pre = %d Suff = %d  \n', data.off_pre, data.off_suff);
    case 'space' 
        if data.active == "pre"
            data.active = "suff";
        else
            data.active = "pre";
        end
        fprintf('Pre = %d Suff = %d  \n', data.off_pre, data.off_suff);
    case 'return'
        my_save(data);
        data = process_step(data);
        set(src, 'name', data.file);   
end 

guidata(src,data);
ploting(src);
end

function mytestcallback(src,~)
data = guidata(src);
pt = get(gca,'CurrentPoint');
get(gca, 'tag');

if get(gca,'tag') == "selecting"
    if data.active == "pre"
       data.active = "suff";
    else
       data.active = "pre";
    end
else
    tmp = data.A(get_lag(data) + data.off_pre, 1);
    tmp = abs(pt(1) - tmp);
    [~, tmp] = min(tmp);
     if data.checkbox(tmp) == 1
        data.checkbox(tmp) = 0;
     else
        data.checkbox(tmp) = 1;
     end
end
guidata(src,data);
ploting(src);    
end

function my_save(data)
    pre = data.off_pre;
    suff = data.off_suff;
    marks = get_lag(data);
    file = erase(data.file, ".txt");
    dir1 = "Processed/time/";
    dir2 = "Processed/no time/";
    dir3 = "Processed/mat/";
    
    for i = 1:length(marks)
        if data.checkbox(i) == 1
            tmp = data.A(marks(i) + pre:marks(i)+pre+suff, :);
            dlmwrite(dir1+file+"-"+string(i)+".txt", tmp, 'delimiter',' ');
            dlmwrite(dir2+file+"-"+string(i)+".txt", tmp(:,2:3), 'delimiter',' '); 
            save(dir3+file+"-"+string(i), "tmp");
        end
    end
    
end

function lag = get_lag(data)
    [r, lg] = xcorr( data.edge, data.A(:,3));
    %plot(r)
    %input("")
    [~,final_peaks_beg]=findpeaks(r,'MinPeakDistance',60, 'MinPeakHeight',40);
    lag = flip(-lg(final_peaks_beg));
end

function rectangles(data)
    lag = get_lag(data);
    
    for i = 1:length(lag)
        if data.checkbox(i) == 1
            rectangle('Position',[data.A(lag(i) + data.off_pre,1) -1.499 data.A(lag(i) + data.off_pre + data.off_suff, 1)-data.A(lag(i) + data.off_pre,1) 3],'FaceColor',[0 0.9 0 0.2], 'EdgeColor', [0 0.9 0 0.2]);
        else
            rectangle('Position',[data.A(lag(i) + data.off_pre,1) -1.499 data.A(lag(i) + data.off_pre + data.off_suff, 1)-data.A(lag(i) + data.off_pre,1) 3],'FaceColor',[0.9 0 0 0.2], 'EdgeColor', [0.9 0 0 0.2]);
        end
    end
    
end



        
    
    
    
    
    







