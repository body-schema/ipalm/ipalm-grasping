directory = "/Users/michalpliska/Desktop/Processing/DATA/*.txt";
tmp = dir(directory);
tmp = struct2cell(tmp);
data.todo = string(tmp(1, :));
data.file = data.todo(1)
file = fopen("/Users/michalpliska/Desktop/Processing/DATA/"+data.file, "r");
fgetl(file);
fgetl(file);
formatSpec = '%f %f %f';
sizeA = [3 inf];
A = fscanf(file,formatSpec,sizeA);
A = A.';
A(:, 2) = A(:, 2)/100;
A(:, 3) = A(:, 3)/120;
%range = 2160:2230;
range = 420:558;
edge = A(range,3);
%plot(A(:,1), A(:,3), "b");
%plot(A(range,1), A(range,3), "b");
edge = [ones(20, 1)*edge(1); edge; ones(20, 1)*edge(end)];
plot(edge)
save("edge", "edge");
    