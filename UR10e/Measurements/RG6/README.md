# UR10e with OnRobot RG-6 gripper
## Measurements
Measuring sessions conducted so far in a chronological order

* 13/11/2019
	* initial measuring test
	* measured objects:
		* kitchen sponge
		* rubber duck
		* kinova cube 
* 15/11/2019
	* measuring various objects
	* testing the different relations
	* measured objects:
		* kitchen sponge
		* stressball
		* rubber ball
		* green wooden block
		* kinova cube
* 27/11/2019
	* multiple measurements of the same objects
	* measured objects:
		* stressball
		* green wooden block
* 09/03/2020
	* simple measurements of some PU foams
	* measured objects:
		* Polyurethane foam objects
		* 9 selected foams 
* 27/03/2020
	* measurements of the `PU foams` set and `Cubes and Dice` set
	* measured objects:
		* 20 PU foam objects
		* 3 cubes and 4 dice
## Matlab files
Most folders contain **.mat** files with corresponding measurment data. This files can be used in MATLAB for further inspection and processing.
You can also run the **.m** files to get the plots and fitments using the slm engine (make sure to have it included in your Matlab path).
The `15/11/2019` folder also contains the aligned-together plots to get the object compression relations.
