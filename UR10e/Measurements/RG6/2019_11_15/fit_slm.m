clear;
close all;
load('RG6_Sponge_thin.mat');
load('RG6_Sponge_wide.mat');
load('RG6_StressBall.mat');
load('RG6_KinovaCube.mat');
load('RG6_GreenCube.mat');

width1 = cell2mat(RG6_Sponge_wide.width);
force1 = cell2mat(RG6_Sponge_wide.F);

width2 = cell2mat(RG6_StressBall.width);
force2 = cell2mat(RG6_StressBall.F);

width3 = cell2mat(RG6_Sponge_thin.width);
force3 = cell2mat(RG6_Sponge_thin.F);

width4 = cell2mat(RG6_KinovaCube.width);
force4 = cell2mat(RG6_KinovaCube.F);

width5 = cell2mat(RG6_GreenCube.width);
force5 = cell2mat(RG6_GreenCube.F);


x{1} = width1(force1>20);
y{1} = force1(force1>20);

x{2} = width2(width2<80);
y{2} = force2(width2<80);

x{3} = width3(force3>20);
y{3} = force3(force3>20);

x{4} = width4;
y{4} = force4;

x{5} = width5(force5>20);
y{5} = force5(force5>20);

%x = (sort(rand(1,100)) - 0.5)*pi;
%y = sin(x).^5 + randn(size(x))/10;

%slm = slmengine(x,y,'plot','on','knots',10,'increasing','on', ...
%'leftslope',0,'rightslope',0)


figure();
slm=cell(1,5);
% x{1} = sponge_wide.width;
% x{2} = stressball.width;
% x{3} = sponge_thin.width;
for i = 1:4
    slm{i} = slmengine(x{i},y{i},'degree',1,'knots',3, 'interiorknots','free','plot','on');
    slmeval(1.3,slm{i});
end
close all;


figure()
hold on;
for i = 1:length(x)
    scatter(x{i},y{i},15);
end

for i = 1:4
    [a1,a2,b1,b2] = find_lin_coef_from_slm(slm{i});
    %find first equation
    
    %find second equation
    xrange = slm{i}.knots;
    xev = linspace(xrange(1),xrange(2),size(x{i},1));
    yline1 = a1*xev+b1;
    plot(xev,yline1);
    xev = linspace(xrange(2),xrange(3),size(x{i},1));
    yline2 = a2*xev+b2;
    plot(xev,yline2);
end
hold off;
legend('Sponge - Horizontal','Stressball','Sponge - Vertical','Kinova Cube','Green Cube','FontSize',20);

xlabel('Measured gripper width - [mm]','FontSize',20);
ylabel('Applied force - [N]','FontSize',20);
set(gca,'FontSize',20)
