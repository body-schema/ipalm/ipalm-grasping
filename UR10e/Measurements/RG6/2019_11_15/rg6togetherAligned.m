clear;
close all;
load('RG6_Sponge_thin.mat');
load('RG6_Sponge_wide.mat');
load('RG6_StressBall.mat');
load('RG6_KinovaCube.mat');
load('RG6_GreenCube.mat');




width1 = cell2mat(RG6_Sponge_wide.width);
force1 = cell2mat(RG6_Sponge_wide.F);

width2 = cell2mat(RG6_StressBall.width);
force2 = cell2mat(RG6_StressBall.F);

width3 = cell2mat(RG6_Sponge_thin.width);
force3 = cell2mat(RG6_Sponge_thin.F);

width4 = cell2mat(RG6_KinovaCube.width);
force4 = cell2mat(RG6_KinovaCube.F);

width5 = cell2mat(RG6_GreenCube.width);
force5 = cell2mat(RG6_GreenCube.F);

x{1} = 110-width1(width1<=110);
y{1} = force1(width1<=110);

x{2} = 69-width2(width2<=69);
y{2} = force2(width2<=69);

x{3} = 29-width3(width3<=29);
y{3} = force3(width3<=29);

x{4} = 56-width4(width4<=56);
y{4} = force4(width4<=56);

x{5} = 58-width5(width5<=58);
y{5} = force5(width5<=58);

%x = (sort(rand(1,100)) - 0.5)*pi;
%y = sin(x).^5 + randn(size(x))/10;

%slm = slmengine(x,y,'plot','on','knots',10,'increasing','on', ...
%'leftslope',0,'rightslope',0)


% figure();
% slm=cell(1,5);
% % x{1} = RG6_Sponge_wide.width;
% % x{2} = RG6_StressBall.width;
% % x{3} = RG6_Sponge_thin.width;
% for i = 1:length(x)
%     slm{i} = slmengine(x{i},y{i},'degree',1,'knots',3, 'interiorknots','free','plot','on');
%     slmeval(1.3,slm{i});
% end
% close all;


figure()
hold on;
for i = 1:length(x)
    scatter(x{i},y{i},15);
end
for i = 1:length(x)
   
    X = [ones(length(x{i}),1) x{i}];
    b = X\y{i};
    yCalc = X*b;
    plot(x{i},yCalc)
end

% for i = 1:length(x)
% %     [a1,a2,b1,b2] = find_lin_coef_from_slm(slm{i});
% %     %find first equation
% %     
% %     %find second equation
% %     xrange = slm{i}.knots;
% %     xev = linspace(xrange(1),xrange(2),size(x{i},1));
% %     yline1 = a1*xev+b1;
% %     plot(xev,yline1);
% %     xev = linspace(xrange(2),xrange(3),size(x{i},1));
% %     yline2 = a2*xev+b2;
% %     plot(xev,yline2);
% end
hold off;
legend('Sponge - Horizontal','Stressball','Sponge - Vertical','Kinova Cube','Green Cube','FontSize',20);
xlabel('Object compression - [mm]','FontSize',20);
ylabel('Applied force - [N]','FontSize',20);
set(gca,'FontSize',20)
axis([0 100 20 120]);

