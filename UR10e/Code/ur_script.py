"""
Test client for URscript commands
"""
# coding=utf-8
import socket

PORT = 30002  # Based on URscript manual
HOST = '100.66.66.2'


def client():
    host = HOST
    port = PORT

    #    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s = socket.socket()
    s.connect((host, port))

    command = 'def moje_funkce():\n' + \
              ' thread lost_grip_thread():\n'+ \
              '  while True:\n'+ \
              '   set_tool_voltage(24)\n'+ \
              '   sync()\n'+ \
              '  end\n'+ \
              ' end\n'   + \
              ' lg_thr = run lost_grip_thread()'+ \
              ' set_tool_output_mode(0)\n'+\
              ' set_tool_digital_output_mode(0,1)\n'+\
              ' set_tool_digital_output_mode(1,1)\n'+\
              ' def bit(input):\n' + \
              '  msb=65536\n' + \
              '  local i=0\n' + \
              '  local output=0\n' + \
              '  while i<17:\n' + \
              '   set_tool_digital_out(0,True)\n' + \
              '   if input>=msb:\n' + \
              '    input=input-msb\n' + \
              '    set_tool_digital_out(1,False)\n' + \
              '   else:\n' + \
              '    set_tool_digital_out(1,True)\n' + \
              '   end\n' + \
              '   if get_tool_digital_in(0):\n' + \
              '    output=1\n' + \
              '   end\n' + \
              '   sync()\n' + \
              '   set_tool_digital_out(0,False)\n' + \
              '   sync()\n' + \
              '   input=input*2\n' + \
              '   output=output*2\n' + \
              '   i=i+1\n' + \
              '  end\n' + \
              '  textmsg("output=",to_str(output))\n' + \
              '  return output\n' + \
              ' end\n' + \
              ' l = [1,2,3,4,5]\n' + \
              ' i = 0\n' + \
              ' while i < 5:\n' + \
              '   l[i] = l[i]*2\n' + \
              '   i = i + 1\n' + \
              '   textmsg("i=",to_str(i))\n' + \
              ' end\n' + \
              ' target_width=0\n' + \
              ' target_force=60\n' + \
              ' rg_data=floor(target_width)*4\n' + \
              ' rg_data=rg_data+floor(target_force/5)*4*161\n' + \
              ' rg_data=rg_data+32768\n' + \
              ' textmsg("rg data=",to_str(rg_data))\n' + \
              ' bit(rg_data)\n'+\
              'end\n'

    command2 = 'def mackani():\n'+\
                ' sec bit(input):\n'+\
                '  msb=65536\n'+\
                '  local i=0\n'+\
                '  local output=0\n'+\
                '  while i<17:\n'+\
                '   set_digital_out(8,True)\n'+\
                '   if input>=msb:\n'+\
                '    input=input-msb\n'+\
                '    set_digital_out(9,False)\n'+\
                '   else:\n'+\
                '    set_digital_out(9,True)\n'+\
                '   end\n'+\
                '   if get_digital_in(8):\n'+\
                '    output=1\n'+\
                '   end\n'+\
                '   set_digital_out(8,False)\n'+\
                '   input=input*2\n'+\
                '   output=output*2\n'+\
                '   i=i+1\n'+\
                '  end\n'+\
                '  return output\n'+\
                ' end\n'+\
                ' target_width=10\n'+\
                ' target_force=30\n'+\
                ' rg_data=floor(target_width)*4\n'+\
                ' rg_data=rg_data+floor(target_force/5)*4*161\n'+\
                ' rg_data=rg_data+32768\n'+\
                ' bit(rg_data)'+\
                'end\n'
    s.send(command)
    received_data = s.recv(1024)

    s.close()

    print(repr(received_data))


if __name__ == '__main__':
    client()
