# ipalm-grasping

Code and other tools related to haptic object exploration with different robotic grippers.

## Experimental setups
For this project we use the following devices or setups
*  The Barrett Technology BarrettHand
*  The qbRobotics SoftHand
*  The Kinova Gen3 collaborative robot with the Robotiq 2f-85 gripper
*  The Universal Robots UR10e collaborative robot with the OnRobot RG6 gripper

The source files can be found in the coresponding subdirectories

## Important documents
### Robot manuals and know-how
* [KinovaGen3 @ KN E210](https://docs.google.com/document/d/17TN1J_pOTi6WGJQ23UofUn77uDG3092jMDhxPUaClKY/edit#heading=h.n7g09typi0mn)
* [KUKA and Barrett, KN E210](https://docs.google.com/document/d/1EY-63v-oWdqDuD94FctwN2-2-Z-q8zRa1mrzpzIjZQg/edit)
* [UR10-Airskin-QBSoftHand-RG6gripper](https://docs.google.com/document/d/1cfgzop0EwP3AfNRKQspgS-jNxYqVKe-dyktrHlk8u4U/edit?usp=sharing)

### Research reports and slides
* [2019_IPALM-ChistERA](https://docs.google.com/document/d/1e8DP2IUYkFtYa9ii3mcK5WybqKm1uVw0TsIFWQ5wavI/edit#heading=h.o8ogtvv7me7d)
* [Grasping with Barrett Hand (GraspIt, MuJoCo, Dex-Net…)](https://docs.google.com/document/d/1ryH0jeZBb0DJpbPLAzxDP2Vl1bcRnHO2WtBFiTZ1BJU/edit#heading=h.7id4o2olw2ag)
* [Haptic object exploration (Michal)](https://docs.google.com/document/d/1IE1H0XwWpnJo9rCZEKanDioP-KSOT62-I0HIVYRg2pc/edit#)
* [Haptic exploration and categorization (Pavel)](https://docs.google.com/document/d/1U4CgaH4Y1qKx_V3urmkvt6lTcuSZTuTDT19_o_ZM23E/edit?usp=sharing)
* [Related work - notes](https://docs.google.com/document/d/1x882A0m22a5J4ne9s1c-4nUgFF17JRwrFW85tbc7aSI/edit#heading=h.f1j4zo5bwm57)

# Folder description
## BarrettHand
Contains ROS package for data colletion using the Barrett Hand, pre-processing and visualization tools for the measurements and LSTM neural network for object classification.