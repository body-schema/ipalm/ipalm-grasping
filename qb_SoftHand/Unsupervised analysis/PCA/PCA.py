import os
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import random
matplotlib.use('Agg')
from features import extract_features

# https://www.colorhexa.com/b8b8b8
list_of_colors = ['#dacc2b', '#1b5e9e', '#b93d6a', '#868686', '#c5c5c5', '#7b4c42', '#ede697', '#41b8de', '#141d73']

def make_dataset(folder, labels):
    data = {"points": [], "labels": []}
    marker = {'marker': None, 'ms': None, 'fill': None, 'crossed': None}
    if 'a1' in folder:
        marker['marker'] = '<'
    if 'a2' in folder:
        marker['marker'] = '>'
    if 's1' in folder:
        marker['fill'] = False
    if 's2' in folder:
        marker['fill'] = True
    if 'release' in folder:
        marker['crossed'] = True
    else:
        marker['crossed'] = False
    marker['ms'] = 3.5
    markers = []

    for file in os.listdir(folder):
        if file.endswith(".txt"):
            timeseries = np.loadtxt(folder + file)
            position_features = extract_features(timeseries[:, 0])
            current_features = extract_features(timeseries[:, 1])
            point = np.concatenate((position_features, current_features))
            data["points"].append(point)

            for index, label in enumerate(labels):
                if file.startswith(label):
                    data["labels"].append(index)
                    break
            markers.append(marker)
    return data, markers


labels = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
          "yellowcube", "bluecube", "darkbluedie"]
posible_folders = ["a1s1-squeez",  "a1s2-squeez", "a2s1-squeez", "a2s2-squeez",
           "a1s1-squeez_and_release", "a1s2-squeez_and_release", "a2s1-squeez_and_release", "a2s2-squeez_and_release"]

folders = [0]
name = 'xxx'

data = {"points": [], "labels": []}
markers = []
folders_list = ''
for i in folders:
    folder = posible_folders[i]
    folders_list = folders_list + '-' + folder
    trn_folder = "DATASETs/" + folder + "/trn/"
    val_folder = "DATASETs/" + folder + "/val/"
    test_folder = "DATASETs/" + folder + "/test/"
    memory, mark1 = make_dataset(trn_folder, labels)
    validation, mark2 = make_dataset(val_folder, labels)
    test, mark3 = make_dataset(test_folder, labels)
    data = {"points": data['points'] + memory["points"] + validation["points"] + test["points"],
            "labels": data['labels'] + memory["labels"] + validation["labels"] + test["labels"]}
    markers = markers + mark1 + mark2 + mark3

scaler = StandardScaler()
data["points"] = scaler.fit_transform(data["points"])

pca = PCA(n_components=2)
principalComponents = pca.fit_transform(data["points"])
print(pca.explained_variance_ratio_)
print(pca.explained_variance_ratio_.cumsum())
plt.figure()
for i, point in enumerate(principalComponents):
    if random.random() < 0.3:
        #plt.scatter(point[0], point[1], c=list_of_colors[data['labels'][i]], s=0.9)
        if markers[i]['fill']:
            fill = list_of_colors[data['labels'][i]]
        else:
            fill = (0, 0, 0, 0)
        plt.plot(point[0], point[1], marker=markers[i]['marker'], ms=markers[i]['ms'], mfc=fill,
                 mec=list_of_colors[data['labels'][i]])
        if markers[i]['crossed']:
            plt.plot(point[0], point[1], marker='.', ms=1, mfc=(0, 0, 0, 1),
                     mec=(0, 0, 0, 1))

plt.xlabel('Principal Component 1', fontsize=15)
plt.ylabel('Principal Component 2', fontsize=15)
plt.title('qb SoftHand', fontsize=20)

ms = 4
a1 = mlines.Line2D([], [], color='grey', marker='<', linestyle='None',
                          markersize=ms, label='a1')
a2 = mlines.Line2D([], [], color='grey', marker='>', linestyle='None',
                          markersize=ms, label='a2')
s1 = mlines.Line2D([], [], color='black', marker='s', linestyle='None', mec=(0, 0, 0, 1), mfc=(0, 0, 0, 0),
                          markersize=ms, label='speed 1')
s2 = mlines.Line2D([], [], color='black', marker='s', linestyle='None',
                          markersize=ms, label='speed 2')
s = mlines.Line2D([], [], color='black', marker='|', linestyle='None', mfc=(0, 0, 0, 0),
                          markersize=ms, label='squeeze')
sq = mlines.Line2D([], [], color='black', marker='.', linestyle='None',
                          markersize=ms, label='squeeze and release')
handels_markers = [a1, a2, s1, s2, s, sq]
#plt.legend(handles=[a1, a2, s1, s2, s, sq])


colors_legend = dict(zip(labels, list_of_colors))
labels = list(colors_legend.keys())
color_handels = [plt.Rectangle((0, 0), 1, 1, color=colors_legend[label], label=label) for label in labels]
handels = handels_markers + color_handels
print(handels)
#plt.legend(handles=handels)
plt.legend(handles=handels, loc='best', prop={'size': 4})

plt.savefig('Results/' + name + '-folders:' + folders_list, dpi=300)
plt.clf()

