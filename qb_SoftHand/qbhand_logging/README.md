# qbhand_logging package
ROS package for logging the qb SoftHand feedback when grasping the objects.

## Getting Started

These instructions will get you a copy of the package up and running on your local machine for measuring purposes.

### Prerequisites

For this all to work you need

* PC with Ubuntu (16. or 18.)
* Installed ROS (Kinetic or Melodic)
* ROS workspace set up and running with qbdevice-ros and qbhand-ros packages installed
* Connection to the SoftHand (USB)


### Installing
First follow the tutorials here (if you haven't already)
* [qb_hand/Tutorials/ROS Packages Installation](http://wiki.ros.org/qb_hand/Tutorials/ROS%20Packages%20Installation)

Then copy the package file into the src folder in your workspace.
The path should be something like this

```
your_catkin_workspace/src/qbhand-ros/qbhand_logging
```
then you return to the root workspace folder and build all the files with
```
catkin_make
```
do not forget to
```
source devel/setup.bash
```

## Runing the node

* First you need to establish a connection with the SoftHand
	* there might be a problem with user rights and accesing the USB port
* Then, launch the communication handler
```
roslaunch qb_device_driver communication_handler.launch
```
* When the connection is set up, launch  the soft hand controller

```
roslaunch qb_hand_control control.launch standalone:=false activate_on_initialization:=true device_id:=<actual_device_id> use_waypoints:=false use_controller_gui:=true
```
* **standalone:=false** since we have already run the communcation handler	
* **device_id:** id of the device
    * in our case always 1 (for we have but 1 softhand)
* **use_waypoints:** use waypoint mode
    * true or false
    * The waypoints are saved in the <robot_package>_control/config/<robot_name>_waypoints.yaml file
    * The <robot_name> in the filename can be different than the true <robot_name> which can be found in the list of topics
* **use_controller_gui:** use controller gui
    * true or false

* Now, you can run the logging node by

```
rosrun qbhand_logging qbhand_logging_node
```
Important feedback service - [GetMeasurements](http://docs.ros.org/api/qb_device_srvs/html/srv/GetMeasurements.html), it’s used for getting the motor position and the motor current.


## Further reading
I followed these tutorials 
* [WritingServiceClient](http://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28c%2B%2B%29)
* [CreatingPackage](http://wiki.ros.org/ROS/Tutorials/CreatingPackage)
* [BuildingPackages](http://wiki.ros.org/ROS/Tutorials/BuildingPackages)

More details here [qb SoftHand](https://docs.google.com/document/d/1cfgzop0EwP3AfNRKQspgS-jNxYqVKe-dyktrHlk8u4U/edit#heading=h.1pvfuvh4xkce)



