#include "ros/ros.h"
#include "qb_device_srvs/qb_device_srvs.h"

#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <iostream>
#include <fstream>
#include <string> 

using namespace std;


/**
 * This is a code for the service client node, that writes down the Softhand
 * statistics into a textfile.
 * I based this on the following tutorials:
 * 
 * https://wiki.ros.org/ROS/Tutorials/UnderstandingServicesParams
 * https://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28c%2B%2B%29
 * 
 */


const double PERIOD_SEC = 0.25;
static bool running = true;
bool should_continue = true;
struct sigaction stop_action;

clock_t this_time;
clock_t last_time;
ofstream file;

string buffer="// Time Position Current \n";

void mySigintHandler(int sig)
{
	/** Do some custom action.
	* In my case save and close the file
	*/
	/*
	file << buffer;
	file.close();*/
	// All the default sigint handler does is call shutdown()
	should_continue = false;
}



int main(int argc, char **argv)
{	
	ros::init(argc, argv, "hand_logging_client_node",ros::init_options::NoSigintHandler);
	ROS_INFO("Starting hand logging.");
	ros::NodeHandle n;
	string object_name="";
	int counter=0;
	
	signal(SIGINT, mySigintHandler);
	
	/** Reading the current time so we can name the textfile accordingly. 	 */
	time_t now = time(0);
	tm *ltm = localtime(&now);	
	string filename="SoftHand-"+to_string(1900 + ltm->tm_year)+"-";
				
	if(1+ltm->tm_mon<10){
		filename+="0";
	}
	filename+=to_string(1 + ltm->tm_mon)+"-";
	if(ltm->tm_mday<10){
		filename+="0";
	}
	filename+=to_string(ltm->tm_mday)+"-";
	if(ltm->tm_hour<10){
		filename+="0";
	}
	filename+=to_string(ltm->tm_hour)+"-";
	if(ltm->tm_min<10){
		filename+="0";
	}
	filename+=to_string(ltm->tm_min);
	
	if (!ros::param::get("~object_name", object_name))
	{
		std::string error_string = "Parameter object_name was not specified, defaulting to " + object_name + " as an object name";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using object_name " + object_name + " as an object name";
		ROS_INFO("%s", error_string.c_str());
	}
	filename+=("-"+object_name);
	
	//opening the filestream
	file.open("SoftHandLogs/"+filename+".txt",ios::in | ios::trunc);
	//Checking if the process was successful
	if (file.is_open()){
		ROS_INFO("Output operation successfully performed\n");
	}else{
		ROS_INFO("Error opening file");
		ros::shutdown();
		return 1;
	}  

	/** Creating a client */
	ros::ServiceClient client = n.serviceClient<qb_device_srvs::GetMeasurements>("/communication_handler/get_measurements");

	qb_device_srvs::GetMeasurements srv;
	srv.request.id = 1;
	srv.request.max_repeats = 3;
	srv.request.get_currents = true;
	srv.request.get_positions = true;

	

	double time_counter = 0;
	this_time = clock();
	last_time = this_time;
	double startTime = ros::Time::now().toSec();
	/** main loop */
	
	ros::Rate r(10);
	while(should_continue)
	{
		
		
		/** calling the service with 10Hz frequency */
		
		double t= ros::Time::now().toSec()-startTime;
		if (client.call(srv)){
			//service call was successful
			ROS_INFO("Time: %f Current: %d Position: %d",t ,srv.response.currents[0],srv.response.positions[0]);
			buffer+=to_string(t) + " " + to_string(srv.response.positions[0]) + " " + to_string(srv.response.currents[0]) + "\n";
		}else{
			ROS_INFO("Current: NaN Position: NaN");
			file << "Nan NaN \n";
		}      
		if(counter==50){
			
			file << buffer;
			
			buffer="";
			counter=0;
		}	
		counter++;
		ros::spinOnce();      
		
		r.sleep();
	}
	file << buffer;
	file.close();
	ros::param::del("~object_name");
	ROS_INFO("Shutting down");
	cout<<("\nShutting down logging!\n");
	ros::shutdown();

	return 0;
}
