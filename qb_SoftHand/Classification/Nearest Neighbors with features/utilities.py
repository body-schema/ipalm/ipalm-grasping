import numpy as np
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42

def float_to_str(input_float, precision=2, separator="\'"):
    """
    Formats float to string with desired precision and specified separator instead of decimal point.

    Parameters
    ----------
    input_float : float
        Input value to be formatted.
    precision : int
        Desired precision.
    separator : str
        Separator to be used instead of decimal point.

    Returns
    -------
    float_str : str
        Input formatted as string by specifications.
    """
    multiplier = 10**precision
    decimal = int(input_float*multiplier) - int(input_float)*multiplier

    integer_str = str(int(input_float))
    decimal_str = '{:0>2d}'.format(decimal)
    float_str = integer_str + '\'' + decimal_str
    return float_str

def print_confusion_matrix(fn, y_true, y_pred, labels, save=True):
    """
    Creates confusion matrix and optionally saves it as a file.

    Parameters
    ----------
    fn : string
        Name of the created image.
    y_true : array-like of shape (n_samples,)
        Ground truth (correct) target values.
    y_pred : array-like of shape (n_samples,)
        Estimated targets as returned by a classifier.
    labels : array
        String labeles for classes, must be ordered from 0 to C, where C is the number of classes.
    save : bool
        If True, confusion matrix is visualized and saved as an image.
    normalize : {'true', 'pred', 'all'}, default=None
        Normalizes confusion matrix over the true (rows), predicted (columns) conditions or all the population. If None, confusion matrix will not be normalized.

    Returns
    -------
    none
    """
    file_format = "pdf"
    cm = confusion_matrix(y_true, y_pred)
    cm = np.array(cm)
    row_sums = cm.sum(axis=1)
    cm = cm / row_sums[:, np.newaxis]
    if save:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax = ax.imshow(cm, cmap=plt.cm.Blues)

        for (i, j), z in np.ndenumerate(cm):
            if z == 0:
                continue
            z_str = '{:.2f}'.format(z)
            ax.text(j, i, z_str, ha='center', va='center',
                    size='xx-small', color='w')

        fig.colorbar(cax)
        ax.set_xticks(np.arange(len(labels)))
        ax.set_yticks(np.arange(len(labels)))
        ax.set_xticklabels(labels)
        ax.set_yticklabels(labels)
        plt.setp(ax.get_xticklabels(), rotation=45,
                 ha="right", rotation_mode="anchor")

        plt.title('Confusion matrix of the classifier')
        plt.xlabel('Predicted')
        plt.ylabel('True')
        fig.tight_layout()
        plt.savefig(fn + '.' + file_format, format=file_format)
        plt.close()
    else:
        print(cm)
    return 0


