import os
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')
from utilities import print_confusion_matrix
from features import extract_features


def make_dataset(folder, labels):
    data = {"points": [], "labels": []}

    for file in os.listdir(folder):
        if file.endswith(".txt"):
            timeseries = np.loadtxt(folder + file)
            position_features = extract_features(timeseries[:, 0])
            current_features = extract_features(timeseries[:, 1])
            point = np.concatenate((position_features, current_features))
            # point = current
            data["points"].append(point)

            for index, label in enumerate(labels):
                if file.startswith(label):
                    data["labels"].append(index)
                    break
    return data


labels = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
          "yellowcube", "bluecube", "darkbluedie"]
folders = ["DATASETs/squeez/", "DATASETs/squeez_and_release/","DATASETs/a1s1-squeez/",
           "DATASETs/a1s1-squeez_and_release/", "DATASETs/a1s2-squeez/", "DATASETs/a1s2-squeez_and_release/",
           "DATASETs/a2s1-squeez/", "DATASETs/a2s1-squeez_and_release/", "DATASETs/a2s2-squeez/",
           "DATASETs/a2s2-squeez_and_release/"]

k_acc = []
for k in range(1, 30):
    acc = []
    for folder in folders:
        trn_folder = folder + "trn/"
        val_folder = folder + "val/"
        memory = make_dataset(trn_folder, labels)
        validation = make_dataset(val_folder, labels)
        scaler = StandardScaler()
        scaler.fit(memory["points"])
        memory["points"] = scaler.transform(memory["points"])
        validation["points"] = scaler.transform(validation["points"])

        knn = KNeighborsClassifier(n_neighbors=k)
        knn.fit(memory["points"], memory["labels"])
        predictions = knn.predict(validation["points"])

        correct = (predictions == validation["labels"]).sum().item()
        total = len(validation["labels"])
        acc.append(correct / total * 100)
    k_acc.append(sum(acc) / len(acc))
    print("-", end="")
    acc = []

best_k = np.argmax(np.array(k_acc)) + 1
print("Best K:", best_k, "with acc", np.max(np.array(k_acc)))
for folder in folders:
    trn_folder = folder + "trn/"
    test_folder = folder + "test/"
    memory = make_dataset(trn_folder, labels)
    test = make_dataset(test_folder, labels)
    scaler = StandardScaler()
    scaler.fit(memory["points"])
    memory["points"] = scaler.transform(memory["points"])
    test["points"] = scaler.transform(test["points"])

    knn = KNeighborsClassifier(n_neighbors=best_k)
    knn.fit(memory["points"], memory["labels"])
    predictions = knn.predict(test["points"])

    correct = (predictions == test["labels"]).sum().item()
    total = len(test["labels"])
    acc = correct / total * 100
    name = folder
    name = name.replace("DATASETs", "")
    name = name.replace("/", "")
    print("ACC on " + name + " test set: ", acc)
    print_confusion_matrix("Results/Dataset:" + name + f' ACC:{acc} K:{best_k}', test["labels"], predictions,
                           labels, save=True)

print(k_acc)
plt.plot(np.linspace(1, len(k_acc), len(k_acc)), k_acc)
plt.xlabel('K')
plt.ylabel('Accuracy')
plt.title('Accuracy over K')
plt.savefig('Accuracy over K.png')
