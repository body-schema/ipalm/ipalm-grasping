import numpy as np
import scipy.stats
import scipy.signal


def extract_features(array):
    # features computed default: Min, Max, Mean, Kurtosis, Skewness, Median, Std, Var, Diff1_mean, Diff2_mean, Diff1_max, Diff2_max, Trapz, Hilbert
    features = []

    max_val = np.max(array)
    features.append(max_val)

    min_val = np.min(array)
    features.append(min_val)

    mean = np.mean(array)
    features.append(mean)

    kurtosis = scipy.stats.kurtosis(array, fisher=False)
    features.append(kurtosis)

    skewness = scipy.stats.skew(array)
    features.append(skewness)

    median = np.median(array)
    features.append(median)

    std = np.std(array)
    features.append(std)

    var = np.var(array)
    features.append(var)

    """
    diff_1_mean = np.mean(np.diff(array))
    features.append(diff_1_mean)

    diff_2_mean = np.mean(np.diff(array, n=2))
    features.append(diff_2_mean)

    diff_1_max = np.max(np.diff(array))
    features.append(diff_1_max)

    diff_2_max = np.max(np.diff(array, n=2))
    features.append(diff_2_max)
    """

    sum_val = np.sum(array)
    features.append(sum_val)

    zero_mean_array = array - mean
    hilbert_transform = scipy.signal.hilbert(zero_mean_array)
    amp_hilbert = np.sqrt(np.real(hilbert_transform) ** 2 + np.imag(hilbert_transform) ** 2)
    amp_mean_hilbert = np.mean(amp_hilbert)
    features.append(amp_mean_hilbert)

    return features


