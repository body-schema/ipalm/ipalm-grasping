import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.use('Agg')

from utilities import print_confusion_matrix
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from features import extract_features
from sklearn.model_selection import GridSearchCV


def make_dataset(folder, labels):
    data = {"points": [], "labels": []}

    for file in os.listdir(folder):
        if file.endswith(".txt"):
            timeseries = np.loadtxt(folder + file)
            position_features = extract_features(timeseries[:, 0])
            current_features = extract_features(timeseries[:, 1])
            point = np.concatenate((position_features, current_features))
            # point = current
            data["points"].append(point)

            for index, label in enumerate(labels):
                if file.startswith(label):
                    data["labels"].append(index)
                    break
    return data


labels = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
          "yellowcube", "bluecube", "darkbluedie"]
folders = ["DATASETs/squeez/", "DATASETs/squeez_and_release/","DATASETs/a1s1-squeez/",
           "DATASETs/a1s1-squeez_and_release/", "DATASETs/a1s2-squeez/", "DATASETs/a1s2-squeez_and_release/",
           "DATASETs/a2s1-squeez/", "DATASETs/a2s1-squeez_and_release/", "DATASETs/a2s2-squeez/",
           "DATASETs/a2s2-squeez_and_release/"]
param_grid = {'C': np.logspace(1, 3, 4, endpoint=True),
              'gamma': np.logspace(-3, 1, 5, endpoint=True),
              'kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
              'cache_size': [10000]}

for folder in folders:
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"
    test_folder = folder + "test/"

    memory = make_dataset(trn_folder, labels)
    validation = make_dataset(val_folder, labels)
    test = make_dataset(test_folder, labels)
    memory["points"] = np.concatenate((memory["points"], validation["points"]))
    memory["labels"] = np.concatenate((memory["labels"], validation["labels"]))
    scaler = StandardScaler()
    scaler.fit(memory["points"])
    memory["points"] = scaler.transform(memory["points"])
    test["points"] = scaler.transform(test["points"])

    grid = GridSearchCV(SVC(), param_grid, refit=True, verbose=1)
    grid.fit(memory["points"], memory["labels"])
    predictions = grid.predict(test["points"])

    correct = (predictions == test["labels"]).sum().item()
    total = len(test["labels"])
    acc = correct / total * 100
    name = folder
    name = name.replace("DATASETs", "")
    name = name.replace("/", "")
    print("ACC on " + name + " test set: ", acc)
    print_confusion_matrix("Results/Dataset:" + name + f' ACC:{acc} ' + str(grid.best_params_), test["labels"], predictions,
                           labels, save=True)

