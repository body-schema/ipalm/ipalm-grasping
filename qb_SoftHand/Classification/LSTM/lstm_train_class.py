import os
import shutil
import sys
from os import path
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F
from lstm_model import LSTM
from utilities import get_free_gpu, create_dataset, create_loader, print_confusion_matrix, float_to_str, cleanup_dir
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
position = True
current = False

folders = ["DATASETs/squeez/", "DATASETs/squeez_and_release/","DATASETs/a1s1-squeez/",
           "DATASETs/a1s1-squeez_and_release/", "DATASETs/a1s2-squeez/", "DATASETs/a1s2-squeez_and_release/",
           "DATASETs/a2s1-squeez/", "DATASETs/a2s1-squeez_and_release/", "DATASETs/a2s2-squeez/",
           "DATASETs/a2s2-squeez_and_release/"]

data = sys.argv[1]
position = 'True' == sys.argv[2]
current = 'True' == sys.argv[3]
folder = "DATASETs/" + data + "/"
print("Using data ", folder)
trn_folder = folder + "trn/"
val_folder = folder + "val/"

if torch.cuda.is_available():
    device = torch.device(get_free_gpu())
else:
    device = 'cpu'
print("Working with", device)

labels = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
          "yellowcube", "bluecube", "darkbluedie"]
# Parameters
output_dim = len(labels)
n_epochs = 25000

hidden_dims = [256, 32, 64, 128]  # 128
layer_dims = [4, 2]  # 2
lrs = np.logspace(-5, -3, 7*6)

print("Preparing datasets")
trn_dataset, weights = create_dataset(trn_folder, labels, position=position, current=current)
val_dataset, _ = create_dataset(val_folder, labels, position=position, current=current)
input_dim = list(trn_dataset[0][0][0].size())[0]
bss = [len(trn_dataset)]
print("Bs: ", len(trn_dataset))
print('Input dim: ', input_dim)
print('Weights: ', weights)

if position and current:
    results = "Results" if not torch.cuda.is_available() else "/local/temporary/pliskmic/Results"
elif position and not current:
    results = "Results_ablation_current" if not torch.cuda.is_available() else "/local/temporary/pliskmic/Results_ablation_current"
elif not position and current:
    results = "Results_ablation_position" if not torch.cuda.is_available() else "/local/temporary/pliskmic/Results_ablation_position"
if not path.exists(results):
    os.mkdir(results)

results = results + "/" + data
print("Preparing directory")
if path.exists(results):
    shutil.rmtree(results)
os.mkdir(results)

counter = 1
for layer_dim in layer_dims:
    for hidden_dim in hidden_dims:
        for bs in bss:
            for lr in lrs:
                best_acc = 0
                patience, trials = 300, 0
                print("Preparing directories")
                result_dir = results + f"/layer= {layer_dim} hidden= {hidden_dim} bs= {bs} lr= {lr}/"
                os.mkdir(result_dir)
                matrix_dir = results + f"/layer= {layer_dim} hidden= {hidden_dim} bs= {bs} lr= {lr}/Confusion matrices/"
                os.mkdir(matrix_dir)

                print("Running ", counter, ". of ", len(layer_dims) * len(hidden_dims) * len(bss) * len(lrs), ". ",
                      f'Current parameters are: layer_dim: {layer_dim}, hidden_dim: {hidden_dim}, bs: {bs} '
                      f'lr: {lr}')
                print(f'Creating data loaders, batch size = {bs}')
                trn_loader = create_loader(trn_dataset, bs)
                val_loader = create_loader(val_dataset, bs)

                print("Creating model")
                model = LSTM(input_dim, layer_dim, hidden_dim, output_dim, device)
                model = model.to(device)
                # optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.5)
                optimizer = optim.Adam(model.parameters(), lr=lr, amsgrad=True)
                class_weights = torch.FloatTensor(weights).to(device)
                criterion = nn.CrossEntropyLoss(weight=class_weights)

                loss_arr = []
                acc_arr = []

                # Training loop
                print("Start model training")
                for epoch in range(1, n_epochs + 1):
                    # Model training
                    model.train()
                    for i, (x_batch, len_batch, y_batch) in enumerate(trn_loader):
                        x_batch = x_batch.to(device)
                        len_batch = len_batch.to(device)
                        y_batch = y_batch.to(device)

                        optimizer.zero_grad()
                        output = model(x_batch, len_batch)
                        loss = criterion(output, y_batch)
                        loss.backward()
                        optimizer.step()

                    # Model evaluation
                    correct, total = 0, 0
                    model.eval()
                    y_val_complete, y_pred_complete = [], []
                    with torch.no_grad():
                        for (x_val, len_val, y_val) in val_loader:
                            x_val = x_val.to(device)
                            len_val = len_val.to(device)
                            y_val = y_val.to(device)

                            out = model(x_val, len_val)
                            preds = F.log_softmax(out, dim=1).argmax(dim=1)
                            total += y_val.size(0)
                            correct += (preds == y_val).sum().item()

                            y_val_complete = y_val_complete + y_val.tolist()
                            y_pred_complete = y_pred_complete + preds.tolist()

                    acc = correct / total
                    print(counter, "/", len(layer_dims) * len(hidden_dims) * len(bss) * len(lrs),
                          f':   Epoch: {epoch:3d}. Loss: {loss.item():.4f}. Acc.: {acc:2.2%}')
                    loss_arr.append(loss.item())
                    acc_arr.append(acc)

                    if epoch % 10 == 0:
                        filename = result_dir + "Confusion matrix"
                        print_confusion_matrix(filename, y_val_complete, y_pred_complete, labels)
                        plt.plot(loss_arr)
                        plt.plot(acc_arr)
                        plt.title(f'layer_dim:{layer_dim} hidden_dim:{hidden_dim} bs:{bs} lr:{lr}')
                        plt.ylabel('Loss, Accuracy')
                        plt.xlabel('Epoch')
                        plt.legend(['Train Loss', 'Val Acc'], loc='upper left')
                        plt.savefig(result_dir + 'Training plot')
                        cleanup_dir(matrix_dir)

                    if acc > best_acc:
                        trials = 0
                        best_acc = acc
                        text_acc = float_to_str(best_acc * 100)
                        torch.save(model.state_dict(), result_dir + "weights.pth")
                        matrix = f'Epoch= {epoch} ACC= {acc:2.2%}'
                        print_confusion_matrix(
                            matrix_dir + matrix, y_val_complete, y_pred_complete, labels)
                        cleanup_dir(matrix_dir)
                        print(f'Saved model from epoch {epoch}, accuracy: {best_acc:2.2%}')
                        if int(acc) == 1:
                            print("Reached 100% accuracy")
                            break
                    else:
                        trials += 1
                        if trials >= patience and epoch > 1500:
                            print(
                                f'Early stopping on epoch {epoch}, best accuracy {best_acc:2.2%} in epoch {epoch - 100}.')
                            break
                np.savez(result_dir + "/loss&acc", loss=loss.cpu().detach().numpy(), acc=acc)
                os.rename(result_dir,
                          results + f'/ACC= {best_acc:2.2%} layer= {layer_dim} hidden= {hidden_dim} bs= {bs} lr= {lr}/')
                plt.close()
                counter += 1
                print("Configuration done..")
print("Grid-search is  finished!")
