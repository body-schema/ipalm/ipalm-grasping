clear, clc;

data.start = 30;
data.step =  65; %45;
data.num_lines = 21;
data.s1 = [];
data.s2 = [];
data.checkbox = ones(data.num_lines);
%data.files = [ "yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube", "yellowcube", "bluecube", "darkbluecube"];
data.index = ones(1,9);

directory = "DATA/*.txt";
tmp  = dir(directory);
tmp = struct2cell(tmp);
data.todo = string(tmp(1, :));

data.file = data.todo(1);
data.todo = data.todo(2:end);
file = fopen("DATA/"+data.file, "r");
formatSpec = '%f %f %f';
sizeA = [3 inf];
fgetl(file);
A = fscanf(file,formatSpec,sizeA);
A = A.';
A(:, 2) = A(:, 2)/19000;
A(:, 3) = A(:, 3)/1000;
data.A = A;

dispSize = get(0, 'ScreenSize'); 
figSize = [dispSize(3) dispSize(4)/2];
hfig = figure('name', data.file, 'pos', [(dispSize(3)-figSize(1))/2 (dispSize(4)-figSize(2))/2 figSize(1)  figSize(2)]);
guidata(hfig, data);
hfig.KeyPressFcn = @keyboardCallback;
hfig.WindowButtonDownFcn = @mytestcallback;
hold on
ploting(hfig)

function ploting(hfig) 
    
    data = guidata(hfig);
    A = data.A;
    delete(data.s1)
    delete(data.s2)
    
    data.s1 = subplot(2,1,1);
    set(gca,'tag',"cutting")
    hold on
    plot(A(:,1), A(:,2), "b");
    plot(A(:,1), A(:,3), "k");
    plot(makelines(data), repmat([-1.5 1.5 nan], data.num_lines + 1), "r+:");
    axis([-inf inf -1.5 1.5])
    
    data.s2 = subplot(2,1,2);
    set(gca,'tag',"selecting")
    plot(makelines(data), repmat([-1.5 1.5 nan], data.num_lines + 1), "k");
    axis([0 A(end, 1) -1.5 1.5])
    rectangles(data);
    
    guidata(hfig, data);
end

function keyboardCallback(src,event)
data = guidata(src);

switch event.Key  
    case 'uparrow'
        data.num_lines = data.num_lines + 1;
    case 'downarrow' 
        data.num_lines = max(data.num_lines - 1, 1);
    case 'f1' 
        data.step = max(data.step - 1, 1);
        disp(data.step);
    case 'f2' 
        data.step = data.step + 1;
        disp(data.step);
    case 'leftarrow' 
        data.start = max(data.start - 1, 0);
    case 'rightarrow' 
        data.start = data.start + 1;
    case 'space' 
        data.start = data.start + 5;
    case 'return'
        my_save(data);
        data.file = data.todo(1);
        data.todo = data.todo(2:end);
        file = fopen("/Users/michalpliska/Desktop/qbSofthand/Cut/DATA/"+data.file, "r");
        formatSpec = '%f %f %f';
        sizeA = [3 inf];
        fgetl(file);
        A = fscanf(file,formatSpec,sizeA);
        A = A.';
        A(:, 2) = A(:, 2)/19000;
        A(:, 3) = A(:, 3)/1000;
        data.A = A;
        set(src, 'name', data.file);
        data.checkbox = ones(data.num_lines);
end
guidata(src,data);
ploting(src);
end

function mytestcallback(src,~)
data = guidata(src);
pt = get(gca,'CurrentPoint');

if get(gca,'tag') == "cutting"
    tmp = abs(data.A(:,1) - pt(1));
    [ ~, tmp] = min(tmp);
    data.start = tmp;
    disp(tmp);
else
    tmp = data.A((0:data.num_lines) * data.step + data.start, 1);
    if pt(1) >= tmp(1) && pt(1) <= tmp(end)
        tmp = tmp - pt(1);
        tmp = max(tmp, 0);
        tmp = find(tmp, 1)-1;
        if data.checkbox(tmp) == 1
         data.checkbox(tmp) = 0;
        else
         data.checkbox(tmp) = 1;
        end
    end
end
guidata(src,data);
ploting(src);    
end

function lines = makelines(data)
    start = data.start;
    step = data.step;
    num = data.num_lines;
    A = data.A;
    tmp = [];
    for i = 0:num
        tmp = [tmp [A(start + i*step,1)  A(start + i*step,1) nan]];
    end
    lines = tmp;
end

function rectangles(data)
    start = data.start;
    step = data.step;
    num = data.num_lines;
    A = data.A;
    tmp = A((0:num) * step + start, 1);
    for i = 1:num
        if data.checkbox(i) == 1
            rectangle('Position',[tmp(i) -1.5 tmp(i+1)-tmp(i) 2.99],'FaceColor',[0 0.9 0 0.2]);
        else
            rectangle('Position',[tmp(i) -1.5 tmp(i+1)-tmp(i) 2.99],'FaceColor',[0.9 0 0 0.2]);
        end
    end
    
end

function my_save(data)
    start = data.start;
    step = data.step;
    num = data.num_lines;
    A = data.A;
    marks = (0:num)*step + start;
    file = erase(data.file, ".txt");
    dir1 = "/Users/michalpliska/Desktop/qbSofthand/Cut/time/";
    dir2 = "/Users/michalpliska/Desktop/qbSofthand/Cut/no time/";
    dir3 = "/Users/michalpliska/Desktop/qbSofthand/Cut/mat/";
    
    for i = 1:num
        if data.checkbox(i) == 1
            tmp = A(marks(i):marks(i)+step-1, :);
            dlmwrite(dir1+file+"-"+string(i)+".txt", tmp, 'delimiter',' ');
            dlmwrite(dir2+file+"-"+string(i)+".txt", tmp(:,2:3), 'delimiter',' '); 
            save(dir3+file+"-"+string(i), "tmp");
        end
    end
    
end
        
    
    
    
    
    







