import os
import shutil

pairs = [("a2s1-squeez", "a1s1-squeez"), ("a2s2-squeez", "a1s2-squeez"),
          ("a2s1-squeez_and_release", "a1s1-squeez_and_release"), ("a2s2-squeez_and_release", "a1s2-squeez_and_release")]

for pair in pairs:
    for file in os.listdir(pair[0]+"/trn/"):
        if file.endswith(".txt") and ("yellowsponge" in file or ("bluedie" in file and not "darkbluedie" in file) or "ycbcube" in file):
            shutil.copyfile(pair[0]+"/trn/"+file, pair[1]+"/trn/"+file)
    for file in os.listdir(pair[0]+"/val/"):
        if file.endswith(".txt") and ("yellowsponge" in file or ("bluedie" in file and not "darkbluedie" in file) or "ycbcube" in file):
            shutil.copyfile(pair[0]+"/val/"+file, pair[1]+"/val/"+file)
    for file in os.listdir(pair[0]+"/test/"):
        if file.endswith(".txt") and ("yellowsponge" in file or ("bluedie" in file and not "darkbluedie" in file) or "ycbcube" in file):
            shutil.copyfile(pair[0]+"/test/"+file, pair[1]+"/test/"+file)
