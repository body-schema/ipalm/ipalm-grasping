clear, clc;

data.directory = "Processed/mat/";
tmp  = dir(data.directory+"*.mat");
tmp = struct2cell(tmp);
todo = string(tmp(1, :));
hfig = figure;
hold on
data.todo = todo;
data.index = 1;
data.sub = [];
guidata(hfig, data);
hfig.KeyPressFcn = @keyboardCallback;

A = load(data.directory + data.todo(data.index));
A = cell2mat(struct2cell(A));
A(:,1) = A(:,1) - A(1,1);

delete(data.sub)
data.sub = subplot(1,1,1);
hold on 
plot(A(:,1), A(:,2), "+");
plot(A(:,1), A(:,3), "+");
plot(A(:,1), A(:,2));
plot(A(:,1), A(:,3));
axis tight;
set(hfig, 'name', data.todo(data.index));
display(data.todo(data.index));


function keyboardCallback(src,event)
data = guidata(src);

switch event.Key  
    case 'leftarrow' 
        data.index = data.index - 1;
    case 'rightarrow' 
        data.index = data.index + 1;
end

A = load(data.directory + data.todo(data.index));
A = cell2mat(struct2cell(A));
A(:,1) = A(:,1) - A(1,1);

%delete(data.sub)
data.sub = subplot(1,1,1);
hold on 
plot(A(:,1), A(:,2));
plot(A(:,1), A(:,3));
axis tight;
set(src, 'name', data.todo(data.index));
display(data.todo(data.index));

guidata(src,data);
end






    
    
    
    



