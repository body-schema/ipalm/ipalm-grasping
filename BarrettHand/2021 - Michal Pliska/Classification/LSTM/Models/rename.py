#!/usr/bin/env python3
import os

folder = os.listdir('Models/')
folder.sort()

for fol in folder:
    for action in ['all']:
        for ab in ['None', 'tact', 'effort', 'onlycoords']:
                if action in fol and ab in fol:
                    os.rename('Models/' + fol, 'Models/' + action + '-ab-' + ab + '.pth')
            
