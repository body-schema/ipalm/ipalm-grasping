import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F
import os

from lstm_model import BarrettLSTM
from lstm_utilities import create_dataset, create_loader, print_confusion_matrix, float_to_str

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

labels = ["gv5030", "gv5040", "n4072", "nf2140", "rl3529", "rl4040", "rl5045", "rp1725", "rp2440",
              "rp2865", "rp3555", "rp27045", "rp30048", "rp50080", "t1820", "t2030", "t2545", "t3240", "v4515", "v5015"]
labels = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
          "yellowcube", "bluecube", "darkbluedie"]

names = ['a3v1.2', 'a1', 'a3', 'all']
ablations = ['onlycoords', None, 'tact', 'effort']

for name in names:
    folder = 'datasets/' + name
    trn_folder = folder + "/trn/"
    val_folder = folder + "/val/"
    for ablation in ablations:
        name_2 = name + '_' + str(ablation)

        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print("Working with", device)

        # Parameters
        input_dim = None  # this will be overriden by creating dataset
        layer_dim = 2
        output_dim = len(labels)

        hidden_dim = 256
        n_epochs = 15000
        lr = 0.00005
        best_acc = 0
        patience, trials = 500, 0
        bs = 128

        print("Preparing datasets")
        trn_dataset, input_dim, _ = create_dataset(trn_folder, ablation=ablation)
        val_dataset, input_dim, _ = create_dataset(val_folder, ablation=ablation)

        print(f'Creating data loaders, batch size = {bs}')
        trn_loader = create_loader(trn_dataset, bs)
        val_loader = create_loader(val_dataset, bs)

        print("Creating model")
        model = BarrettLSTM(input_dim, layer_dim, hidden_dim, output_dim)
        model = model.to(device)
        # optimizer = optim.SGD(model.parameters(), lr=0.003, momentum=0.9)
        # optimizer = optim.Adam(model.parameters(), lr=0.003, amsgrad=True)
        optimizer = optim.Adam(model.parameters(), lr=lr, amsgrad=True)
        criterion = nn.CrossEntropyLoss()

        loss_arr = []
        acc_arr = []
        # Training loop
        print("Start model training")
        for epoch in range(1, n_epochs + 1):
            # Model training
            model.train()
            for i, (x_batch, len_batch, y_batch) in enumerate(trn_loader):
                x_batch = x_batch.to(device)
                len_batch = len_batch.to(device)
                y_batch = y_batch.to(device)

                optimizer.zero_grad()
                output = model(x_batch, len_batch)
                loss = criterion(output, y_batch)
                loss.backward()
                optimizer.step()

            # Model evaluation
            correct, total = 0, 0
            model.eval()
            y_val_complete, y_pred_complete = [], []
            with torch.no_grad():
                for (x_val, len_val, y_val) in val_loader:
                    x_val = x_val.to(device)
                    len_val = len_val.to(device)
                    y_val = y_val.to(device)

                    out = model(x_val, len_val)
                    preds = F.log_softmax(out, dim=1).argmax(dim=1)
                    total += y_val.size(0)
                    correct += (preds == y_val).sum().item()

                    y_val_complete = y_val_complete + y_val.tolist()
                    y_pred_complete = y_pred_complete + preds.tolist()

            acc = correct / total
            filename = "conf_matrix"
            print_confusion_matrix(filename, y_val_complete, y_pred_complete, labels)

            if epoch % 1 == 0:
                print(f'Epoch: {epoch:3d}. Loss: {loss.item():.4f}. Acc.: {acc:2.2%}')
                loss_arr.append(loss.item())
                acc_arr.append(acc)

            if epoch == n_epochs:
                text_acc = float_to_str(acc * 100)
                model_name = name_2 + '_' + str(ablation) + '_' + text_acc + "_e" + str(epoch)
                if not os.path.exists("./models/"): os.mkdir("./models/")
                if not os.path.exists("./models/" + name_2 + '/'): os.mkdir("./models/" + name_2 + '/')
                torch.save(model.state_dict(), "models/" +
                           name_2 + '/' + model_name + ".pth")
                cm_name = "models/" + name_2 + '/' + model_name + "_cm"
                print_confusion_matrix(cm_name, y_val_complete,
                                       y_pred_complete, labels)
                print(f'Saved model from epoch {epoch}, accuracy: {acc:2.2%}')

            if acc > best_acc:
                trials = 0
                best_acc = acc
                text_acc = float_to_str(best_acc * 100)
                model_name = name_2 + '_' + str(ablation) + '_' + text_acc + "_e" + str(epoch)
                if not os.path.exists("./models/"): os.mkdir("./models/")
                if not os.path.exists("./models/" + name_2 + '/'): os.mkdir("./models/" + name_2 + '/')
                torch.save(model.state_dict(), "models/" +
                           name_2 + '/' + model_name + ".pth")
                cm_name = "models/" + name_2 + '/' + model_name + "_cm"
                print_confusion_matrix(cm_name, y_val_complete,
                                       y_pred_complete, labels)
                print_confusion_matrix(
                    filename + "_best", y_val_complete, y_pred_complete, labels)
                print(f'Saved model from epoch {epoch}, accuracy: {best_acc:2.2%}')
            #       if int(acc) == 1:
            #           print("Reached 100% accuracy")
            #           break
            else:
                trials += 1
                if trials >= patience:
                    print(
                        f'Early stopping on epoch {epoch}, best accuracy {best_acc:2.2%} in epoch {epoch - 100}.')
                    break

        print("The training is finished!")
        # Plot training & validation loss values
        plt.plot(loss_arr)
        plt.plot(acc_arr)
        plt.title('Model loss, Accuracy')
        plt.ylabel('Loss, Accuracy')
        plt.xlabel('Epoch')
        plt.legend(['Train Loss', 'Val Acc'], loc='upper left')
        plt.savefig(name.replace(".", "") + '_training_curves')
