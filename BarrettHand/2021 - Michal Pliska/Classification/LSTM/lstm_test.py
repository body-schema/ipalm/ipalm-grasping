import os
import shutil
import sys
from os import path
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F
from lstm_model import BarrettLSTM
from lstm_utilities import create_dataset, create_loader, print_confusion_matrix
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
position = True
current = False

labels_objects = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
              "yellowcube", "bluecube", "darkbluedie"]
labels_foams = ["GV5030", "GV5040", "N4072", "NF2140", "RL3529", "RL4040", "RL5045", "RP1725", "RP2440",
              "RP2865", "RP3555", "RP27045", "RP30048", "RP50080", "T1820", "T2030", "T2545", "T3240", "V4515", "V5015"]
labels_smaller_foams = ["NF2140", "RL5045", "RP1725", "RP30048", "RP50080", "V4515"]
none = (None, 107)
tact = ('tact', 11)
effort = ('effort', 104)
onlycoords = ('onlycoords', 8)

models = ['all', 'a1', 'a3', 'a1v0.6', 'a1v1.2', 'a3v0.6', 'a3v1.2'] #'a1', 'a3',
datasets = ['all', 'a1', 'a3', 'a1v0.6', 'a1v1.2', 'a3v0.6', 'a3v1.2']
ab = tact
labels = labels_objects

test_models = [0, 1, 2 ,3 ,4 ,5 ,6]
test_datasets = [1]

for num_model in test_models:
    for num_dataset in test_datasets:
        test_model = models[num_model]
        dataset = datasets[num_model]
        test_folder = 'DATASETs/' + dataset + "/test/"
        test_dataset, input_dim,  _ = create_dataset(test_folder, ablation=ab[0])
        test_loader = create_loader(test_dataset, 32)
        lstm = BarrettLSTM(input_dim, 2, 256, len(labels))
        lstm.load_state_dict(torch.load('Models/' + test_model + '-ab-' + str(ab[0]) + '.pth', map_location=torch.device('cpu')))
        lstm.eval()

        correct, total = 0, 0
        y_val_complete, y_pred_complete = [], []
        with torch.no_grad():
            for (x_test, len_test, y_test) in test_loader:
                out = lstm(x_test, len_test)
                preds = F.log_softmax(out, dim=1).argmax(dim=1)
                total += y_test.size(0)
                correct += (preds == y_test).sum().item()

                y_val_complete = y_val_complete + y_test.tolist()
                y_pred_complete = y_pred_complete + preds.tolist()

        acc = correct / total

        slogan = "Model: " + test_model + '-ab-' + str(ab[0]) + ', Dataset: ' + dataset + f', Acc.: {acc:2.2%}'
        print(slogan)
        print_confusion_matrix('Test/' + slogan, y_val_complete, y_pred_complete, labels)






