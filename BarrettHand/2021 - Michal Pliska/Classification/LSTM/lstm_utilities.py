#!/usr/bin/env python3
import os

import numpy as np
import torch
import torch.nn.utils.rnn as rnn_utils
from sklearn.metrics import confusion_matrix
from torch.utils.data import DataLoader

import matplotlib.pyplot as plt
#type 3 font fixed
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42


def print_confusion_matrix(fn, y_true, y_pred, labels, save=True, normalize='true'):
    """
    Creates confusion matrix and optionally saves it as a file.

    Parameters
    ----------
    fn : string
        Name of the created image.
    y_true : array-like of shape (n_samples,)
        Ground truth (correct) target values.
    y_pred : array-like of shape (n_samples,)
        Estimated targets as returned by a classifier.
    labels : array
        String labeles for classes, must be ordered from 0 to C, where C is the number of classes.
    save : bool
        If True, confusion matrix is visualized and saved as an image.
    normalize : {'true', 'pred', 'all'}, default=None
        Normalizes confusion matrix over the true (rows), predicted (columns) conditions or all the population. If None, confusion matrix will not be normalized.

    Returns
    -------
    none
    """
    file_format = "pdf"
    cm = confusion_matrix(y_true, y_pred)
    if save:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax = ax.imshow(cm, cmap=plt.cm.Blues)

        for (i, j), z in np.ndenumerate(cm):
            if z == 0:
                continue

            if normalize == 'true':
                z_str = '{:.2f}'.format(z)
                # if z < 1:
                #     z_str = z_str[1:]
                # elif z == 1:
                #     z_str = z_str[:1]
            else:
                z_str = '{:.0f}'.format(z)
            ax.text(j, i, z_str, ha='center', va='center',
                    size='xx-small', color='w')

        fig.colorbar(cax)
        ax.set_xticks(np.arange(len(labels)))
        ax.set_yticks(np.arange(len(labels)))
        ax.set_xticklabels(labels)
        ax.set_yticklabels(labels)
        plt.setp(ax.get_xticklabels(), rotation=45,
                 ha="right", rotation_mode="anchor")

        plt.title('Confusion matrix of the classifier')
        plt.xlabel('Predicted')
        plt.ylabel('True')
        fig.tight_layout()
        plt.savefig(fn + '.' + file_format, format=file_format)
        plt.close()
    else:
        print(cm)
    return 0


def float_to_str(input_float, precision=2, separator="\'"):
    """
    Formats float to string with desired precision and specified separator instead of decimal point.

    Parameters
    ----------
    input_float : float
        Input value to be formatted.
    precision : int
        Desired precision.
    separator : str
        Separator to be used instead of decimal point.

    Returns
    -------
    float_str : str
        Input formatted as string by specifications.
    """
    multiplier = 10**precision
    decimal = int(input_float*multiplier) - int(input_float)*multiplier

    integer_str = str(int(input_float))
    decimal_str = '{:0>2d}'.format(decimal)
    float_str = integer_str + '\'' + decimal_str
    return float_str


def get_labels_from_folder(folder):
    """
    Returns labels (folder names) found in the folder.

    Parameters
    ----------
    folder : str
        Path to the folder.

    Returns
    -------
    labels : array-like
        All the labels.
    """
    labels = []
    directories = os.scandir(folder)
    for directory in directories:
        if directory.is_dir:
            labels += [directory.name_2]

    return labels


def get_reduced_tact(tact_palm, tact_f1, tact_f2, tact_f3, tact_t, taxel_averages):
    """
    Detects the active taxels for each tactile surface and subtracts the resting average so the values start at zero.

    Parameters
    ----------
    tact_palm, tact_f1, tact_f2, tact_f3 : array-like
        Array of arrays containing values from taxels for each time step.
    tact_t : array-like
        Array of time steps.
    taxel_averages : array-like
        Array of arrays containing an resting average value for each taxel.

    Returns
    -------
    new_tactile : array-like
        3d array containing the new tactile data.
    """
    new_tact = [None] * 4
    for t, data in enumerate([tact_palm, tact_f1, tact_f2, tact_f3]):
        data_series = data.transpose()  # (24, t)

        range_i = range(0, len(data_series))
        for i in range_i[::-1]:
            if max(data_series[i]) < 1.3 * taxel_averages[t][i]:
                data_series = np.delete(data_series, i, axis=0)
            else:
                data_series[i] = data_series[i] - taxel_averages[t][i]

        data_series = data_series.transpose()  # (t, 24 - x)
        if data_series.shape == (len(tact_t), 0):
            data_series = np.zeros((len(tact_t), 1))

        new_tact[t] = np.average(data_series, axis=1)

    new_tactile = np.array(
        [new_tact[0], new_tact[1], new_tact[2], new_tact[3]])
    return new_tactile


def npz_to_tensor(entry_path, reduced=False, ablation=None):
    """
    Returns tensor of size: batch_size * seq_len * input_size from npz file.

    Parameters
    ----------
    entry_path : string
        Path to where npz to be converted is located.
    reduced : bool
        Flag for reducing dimensionality. Available for truncated data only.
    ablation : string
        None leaves all the data, "effort" removes effort data, "tact" removes tactile data. Use "onlytact/effort/coords" to use only the one channel.

    Returns
    -------
    ret_tensor : tensor
    """
    meas = np.load(entry_path)
    tact_palm = meas['palm'].astype('float32')
    tact_f1 = meas['f1'].astype('float32')
    tact_f2 = meas['f2'].astype('float32')
    tact_f3 = meas['f3'].astype('float32')
    tact_t = meas['tact_t'].astype('float32')
    joint_coords = meas['coords'].astype('float32')
    joint_effort = meas['effort'].astype('float32')
    joint_t = meas['joint_t'].astype('float32')
    taxel_averages = None

    if reduced:
        try:
            taxel_averages = meas["taxel_averages"]
        except Exception:
            print(
                "Data not truncated, can not detect active taxels. Disable the reduced option.")
            exit()

    seq_len = min(len(joint_t), len(tact_t))

    joint_effort = joint_effort.transpose()
    joint_effort = np.array(
        [joint_effort[0], joint_effort[4], joint_effort[5]])  # select just the fingertip sensors
    joint_effort = joint_effort.transpose()

    if reduced and ablation is None:
        new_tact_arr = get_reduced_tact(
            tact_palm, tact_f1, tact_f2, tact_f3, tact_t, taxel_averages)
        avg_tact = new_tact_arr.transpose()
        stacked_array = np.hstack(
            [avg_tact[:seq_len], joint_effort[:seq_len], joint_coords[:seq_len]])
    elif ablation == "tact":
        # whether reduced is True or False, tactile data will be omitted
        stacked_array = np.hstack(
            [joint_effort[:seq_len], joint_coords[:seq_len]])
    elif ablation == "effort":
        if reduced:
            new_tact_arr = get_reduced_tact(
                tact_palm, tact_f1, tact_f2, tact_f3, tact_t, taxel_averages)
            avg_tact = new_tact_arr.transpose()
            stacked_array = np.hstack(
                [avg_tact[:seq_len], joint_coords[:seq_len]])
        else:
            stacked_array = np.hstack([tact_palm[:seq_len], tact_f1[:seq_len],
                                       tact_f2[:seq_len], tact_f3[:seq_len], joint_coords[:seq_len]])
    elif ablation == "onlytact":
        stacked_array = np.hstack([tact_palm[:seq_len], tact_f1[:seq_len],
                                       tact_f2[:seq_len], tact_f3[:seq_len]])

    elif ablation == "onlycoords":
        stacked_array = np.hstack([joint_coords[:seq_len]])

    elif ablation == "onlyeffort":
        stacked_array = np.hstack([joint_effort[:seq_len]])

    else:
        stacked_array = np.hstack([tact_palm[:seq_len], tact_f1[:seq_len], tact_f2[:seq_len],
                                   tact_f3[:seq_len], joint_effort[:seq_len], joint_coords[:seq_len]])

    ret_tensor = torch.from_numpy(stacked_array)
    return ret_tensor


def create_dataset(folder, reduced=False, ablation=None, return_weights=True):
    """
    Creates dataset from npz files in desired folder and can have either full or reduced dimensionality.
    Reduced dimensionality takes into account only active tactile cells and averages them into one value per tactile surface. TODO mention ablation
    A cell is considered as active when it crosses specified threshold.
    Full dimensionality: 4*24 tactile + 3 torque + 8 joint coordinates features.
    Reduced dimensionality: 4 tactile + 3 torque + 8 joint coordinates features.

    Parameters
    ----------
    folder : string
        Path to where npz files for desired dataset are stored.
    reduced : bool
        Flag for reducing dimensionality. Available for truncated data only.
    ablation : string
        None leaves all the data, "effort" removes effort data, "tact" removes tactile data.
    return_weights : bool
        Whether to return the weights or not.

    Returns
    -------
    ret_value : tuple
        Contains dataset, number of features and optionally weights.
    """
    if reduced:
        print("Creating a dataset with reduced dimensionality.")

    classes = 0
    dataset = []
    directories = os.scandir(folder)
    for directory in directories:
        if directory.is_dir:
            classes += 1

    count = np.array([0] * classes)

    directories = os.scandir(folder)
    for directory in directories:
        if directory.is_dir and directory.name != '.DS_Store':
            class_folder = folder + directory.name + '/'
            entries = os.scandir(class_folder)
            for entry in entries:
                if entry.name.endswith('.npz'):
                    file_path = folder + directory.name + '/' + entry.name
                    data = npz_to_tensor(file_path, reduced, ablation)
                    label = int(directory.name)
                    dataset.append((data, label))
                    if return_weights:
                        count[label] += 1
    features = list(dataset[0][0][0].size())[0]
    print(features)
    #print(len(dataset))
    #print(dataset[30][1])
    #print(len(dataset[0][0]))
    #print(len(dataset[0][0][0]))

    if return_weights:
        weights = count/max(count)
        ret_values = dataset, features, weights
    else:
        ret_values = dataset, features
    return ret_values


def create_loader(dataset, batch_size):
    """
    Creates loaders from datasets with desired batch size.

    Parameters
    ----------
    dataset : list of touples
        Datasets created by the create_dataset(...) function from which the loader should be created.
    batch_size : int
        Batch size of loaders.

    Returns
    -------
    loader : DataLoader
    """
    class PadSequence:
        # This function was taken from a website:
        # https://www.codefull.org/2018/11/use-pytorchs-dataloader-with-variable-length-sequences-for-lstm-gru/
        def __call__(self, batch):
            # Let's assume that each element in "batch" is a tuple (data, label).
            # Sort the batch in the descending order
            sorted_batch = sorted(
                batch, key=lambda x: x[0].shape[0], reverse=True)
            # Get each sequence and pad it
            sequences = [x[0] for x in sorted_batch]
            sequences_padded = rnn_utils.pad_sequence(
                sequences, batch_first=True)
            # Also need to store the length of each sequence
            # This is later needed in order to unpad the sequences
            lengths = torch.LongTensor([len(x) for x in sequences])
            # Don't forget to grab the labels of the *sorted* batch
            labels = torch.LongTensor(list(map(lambda x: x[1], sorted_batch)))
            return sequences_padded, lengths, labels

    loader = DataLoader(dataset, batch_size=batch_size,
                        shuffle=True, collate_fn=PadSequence())
    return loader


if __name__ == "__main__":
    print("This file contains just utilities used by training script or network architecture and does nothing by itself. "
          "Run 'lstm_train.py' if you want to train it.")
