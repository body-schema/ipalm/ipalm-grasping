import os
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')
from utilities import print_confusion_matrix

def make_dataset(folder, labels):
    data = {"points": [], "labels": []}
    for file in os.listdir(folder):
        if file.endswith(".npz"):
            measurement = np.load(folder + file)
            tact_palm = measurement['palm'].astype('float32')
            tact_f1 = measurement['f1'].astype('float32')
            tact_f2 = measurement['f2'].astype('float32')
            tact_f3 = measurement['f3'].astype('float32')
            joint_coords = measurement['coords'].astype('float32')
            joint_effort = measurement['effort'].astype('float32')
            measurement = [tact_f1, tact_f2, tact_f3, tact_palm, joint_effort, joint_coords]
            point = []
            for sensor in measurement:
                for channel in sensor.transpose():
                    point = point + channel.tolist()
            data["points"].append(point)

            for index, label in enumerate(labels):
                if file[7:].startswith(label):
                    data["labels"].append(index)
                    break
    return data

labels = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
              "yellowcube", "bluecube", "darkbluedie"]
folders = ["DATASETs/a1v0.6 - new/", "DATASETs/a1v1.2 - new/", "DATASETs/a3v0.6 - new/", "DATASETs/a3v1.2 - new/"]

k_acc = []
for k in range(1, 30):
    acc = []
    for folder in folders:
        trn_folder = folder + "trn/"
        val_folder = folder + "val/"
        memory = make_dataset(trn_folder, labels)
        validation = make_dataset(val_folder, labels)

        knn = KNeighborsClassifier(n_neighbors=k)
        knn.fit(memory["points"], memory["labels"])
        predictions = knn.predict(validation["points"])

        correct = (predictions == validation["labels"]).sum().item()
        total = len(validation["labels"])
        acc.append(correct / total * 100)
    k_acc.append(sum(acc)/len(acc))
    print("-", end="")
    acc = []

best_k = np.argmax(np.array(k_acc)) + 1
print("Best K:", best_k, "with acc", np.max(np.array(k_acc)))
for folder in folders:
    trn_folder = folder + "trn/"
    test_folder = folder + "test/"
    memory = make_dataset(trn_folder, labels)
    test = make_dataset(test_folder, labels)

    knn = KNeighborsClassifier(n_neighbors=best_k)
    knn.fit(memory["points"], memory["labels"])
    predictions = knn.predict(test["points"])

    correct = (predictions == test["labels"]).sum().item()
    total = len(test["labels"])
    acc = correct / total * 100
    name = folder
    name = name.replace("DATASETs", "")
    name = name.replace("/", "")
    print("ACC on " + name + " test set: ", acc)
    print_confusion_matrix("Results/Dataset:" + name + f' ACC:{acc} K:{best_k}', test["labels"], predictions,
                           labels, save=True)

print(k_acc)
plt.plot(np.linspace(1, len(k_acc), len(k_acc)), k_acc)
plt.xlabel('K')
plt.ylabel('Accuracy')
plt.title('Accuracy over K')
plt.savefig('Accuracy over K.png')

