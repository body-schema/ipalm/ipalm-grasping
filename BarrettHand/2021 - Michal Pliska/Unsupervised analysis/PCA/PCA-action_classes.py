import os
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import random
matplotlib.use('Agg')
from features import extract_features

# https://www.colorhexa.com/b8b8b8
list_of_colors = ['#dacc2b', '#1b5e9e', '#b93d6a', '#868686', '#c5c5c5', '#7b4c42', '#ede697', '#41b8de', '#141d73']
special_colors = ['#72be30',  '#be3530', '#7c30be', '#30b9be']

def make_dataset(folder, labels):
    data = {"points": [], "labels": []}
    marker = {'marker': None, 'ms': None, 'fill': None}
    if 'a1' in folder:
        marker['marker'] = '^'
    if 'a3' in folder:
        marker['marker'] = 'v'
    if 'v0.6' in folder:
        marker['fill'] = False
    if 'v1.2' in folder:
        marker['fill'] = True

    marker['ms'] = 3.5
    markers = []

    for file in os.listdir(folder):
        if file.endswith(".npz"):
            measurement = np.load(folder + file)
            tact_palm = measurement['palm'].astype('float32')
            tact_f1 = measurement['f1'].astype('float32')
            tact_f2 = measurement['f2'].astype('float32')
            tact_f3 = measurement['f3'].astype('float32')
            joint_coords = measurement['coords'].astype('float32')
            joint_effort = measurement['effort'].astype('float32')
            measurement = [tact_f1, tact_f2, tact_f3, tact_palm, joint_effort, joint_coords]
            point = []
            for sensor in measurement:
                for channel in sensor.transpose():
                    point = point + extract_features(channel)
            np.save(folder + file[:-4] + '-features.npy', point)
            data["points"].append(point)

            for index, label in enumerate(labels):
                if file[7:].startswith(label):
                    data["labels"].append(index)
                    break
            markers.append(marker)
    return data, markers


labels = ["yellowsponge", "bluedie", "pinkdie", "kinovacube", "whitedie", "ycbcube",
          "yellowcube", "bluecube", "darkbluedie"]
posible_folders = ["a1v0.6",  "a1v1.2", "a3v0.6", "a3v1.2"]

folders = [0, 1, 2, 3]
name = 'All-actions_and_speeds:'

data = {"points": [], "labels": []}
markers = []
folders_list = ''
for i in folders:
    folder = posible_folders[i]
    folders_list = folders_list + '-' + folder.replace('.', '')
    trn_folder = "DATASETs/" + folder + " - new/trn/"
    val_folder = "DATASETs/" + folder + " - new/val/"
    test_folder = "DATASETs/" + folder + " - new/test/"
    memory, mark1 = make_dataset(trn_folder, labels)
    validation, mark2 = make_dataset(val_folder, labels)
    test, mark3 = make_dataset(test_folder, labels)
    data = {"points": data['points'] + memory["points"] + validation["points"] + test["points"],
            "labels": data['labels'] + memory["labels"] + validation["labels"] + test["labels"]}
    markers = markers + mark1 + mark2 + mark3

scaler = StandardScaler()
data["points"] = scaler.fit_transform(data["points"])

pca = PCA(n_components=2)
principalComponents = pca.fit_transform(data["points"])
plt.figure()
for i, point in enumerate(principalComponents):
    if random.random() < 0.3:
        #plt.scatter(point[0], point[1], c=list_of_colors[data['labels'][i]], s=0.9)
        if markers[i]['marker'] == '^' and not markers[i]['fill']:
            plt.plot(point[0], point[1], marker='.', ms=3.5, mfc=special_colors[0],
                     mec=special_colors[0])
        if markers[i]['marker'] == '^' and markers[i]['fill']:
            plt.plot(point[0], point[1], marker='.', ms=3.5, mfc=special_colors[1],
                     mec=special_colors[1])
        if markers[i]['marker'] == 'v' and markers[i]['fill']:
            plt.plot(point[0], point[1], marker='.', ms=3.5, mfc=special_colors[2],
                     mec=special_colors[2])
        if markers[i]['marker'] == 'v' and not markers[i]['fill']:
            plt.plot(point[0], point[1], marker='.', ms=3.5, mfc=special_colors[3],
                     mec=special_colors[3])


plt.xlabel('Principal Component 1', fontsize=15)
plt.ylabel('Principal Component 2', fontsize=15)
plt.title('BarrettHand', fontsize=20)

ms = 4
a1 = mlines.Line2D([], [], color='grey', marker='^', linestyle='None',
                          markersize=ms, label='a1')
a2 = mlines.Line2D([], [], color='grey', marker='v', linestyle='None',
                          markersize=ms, label='a3')
s1 = mlines.Line2D([], [], color='black', marker='s', linestyle='None', mec=(0, 0, 0, 1), mfc=(0, 0, 0, 0),
                          markersize=ms, label='speed 0.6')
s2 = mlines.Line2D([], [], color='black', marker='s', linestyle='None',
                          markersize=ms, label='speed 1.2')
handels_markers = [a1, a2, s1, s2]
#plt.legend(handles=[a1, a2, s1, s2, s, sq])

special_lables = ['a1v0.6', 'a1v1.2', 'a3v0.6', 'a3v1.2']
colors_legend = dict(zip(special_lables, special_colors))
labels = list(colors_legend.keys())
color_handels = [plt.Rectangle((0, 0), 1, 1, color=colors_legend[label], label=label) for label in labels]
handels = handels_markers + color_handels
print(handels)
handels = color_handels
#plt.legend(handles=handels)
plt.legend(handles=handels, loc=1, prop={'size': 8})

plt.savefig('Results/' + name + '-folders:' + folders_list, dpi=300)
plt.clf()

