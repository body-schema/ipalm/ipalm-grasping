folder_path = ''

out_format = "pdf"
bags_path = folder_path + 'rosbag/'
check_path = folder_path + "rosbag/check_plots/"
datasets_path = folder_path + "datasets/"
raw_dataset_path = folder_path + "raw_dataset/"
raw_dataset_check_path = folder_path + "raw_dataset/check_plots/"

graph_path = folder_path + "graphs/"
npz_trunc_path = folder_path + "npz/"
