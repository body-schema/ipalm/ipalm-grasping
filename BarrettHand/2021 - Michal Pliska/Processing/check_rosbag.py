import os
import shutil
import numpy as np
import rosbag
import matplotlib.pyplot as plt
from config import bags_path, check_path, out_format


def average_all_taxels(tact_tuple, tact_t):
    avg_all_tact_tuple = [None] * 4
    num_tact = len(tact_tuple)

    # Average values for each tactile surface
    for i in range(num_tact):
        average_tact = np.average(tact_tuple[i][0], axis=1)
        tact_label = tact_tuple[i][1]
        avg_all_tact_tuple[i] = (average_tact, tact_label)

    return avg_all_tact_tuple


def plot_effort(npz_name, joint_t, joint_effort_tuple):
    # Effort graph, torque sensor is only at the base of the finger tips
    for i in [5, 0, 4]:
        plt.step(joint_t, joint_effort_tuple[i]
        [0], label=joint_effort_tuple[i][1])
    plt.xlabel("time [s]")
    plt.ylabel("torque [N/m]")
    plt.legend()
    # plt.title(npz_name + " joint effort")
    plt.savefig(check_path + npz_name + "_effort." +
                out_format, format=out_format)
    plt.close()
    return 0


def plot_all_avg_tact(npz_name, tact_t, avg_all_tact_tuple, joint_t, joint_coords_tuple, joint_effort_tuple):
    # Plot averaged tactile data
    colors = ["#ff3333", "#175887", "#db6600", "#217821"]
    for i, data in enumerate(avg_all_tact_tuple):
        plt.step(tact_t, data[0], label=data[1], c=colors[i])

    for i in [1, 2, 3, 5, 0, 4, 6, 7]:
        plt.step(joint_t, joint_coords_tuple[i]
        [0], label=joint_coords_tuple[i][1])

    for i in [5, 0, 4]:
        plt.step(joint_t, joint_effort_tuple[i]
        [0], label=joint_effort_tuple[i][1]+"-effort")

    plt.xlabel("time [s]")
    plt.ylabel("torque [N/m]")
    plt.legend()
    plt.xlabel("time [s]")
    plt.ylabel("pressure [N/cm2]")
    plt.legend()
    # fig = plt.gcf()
    # plt.title(npz_name + " averaged all tactile sensors")
    plt.savefig(check_path + npz_name + "_avg-all-tact." +
                out_format, format=out_format)
    plt.close()
    return 0


def extract_data(bag_name, truncate=True):
    bag_path = bags_path + bag_name
    bag = rosbag.Bag(bag_path)

    # Array definitions
    tact_palm, tact_f1, tact_f2, tact_f3 = [], [], [], []
    joint_coords, joint_effort = [], []
    tact_time, joint_time = [], []
    events = {"waiting_opened": None, "closing": None, "stop": None}

    if truncate:
        valid_data = False
    else:
        valid_data = True

    # Play rosbag
    for topic, msg, t in bag.read_messages(topics=['/bhand_node/tact_array', '/joint_states', '/rosout']):
        if topic == '/bhand_node/tact_array' and valid_data:
            tact_time.append(t.to_sec())
            tact_palm.append(msg.palm)
            tact_f1.append(msg.finger1)
            tact_f2.append(msg.finger2)
            tact_f3.append(msg.finger3)

        elif topic == '/joint_states' and valid_data:
            joint_time.append(t.to_sec())
            joint_coords.append(msg.position)
            joint_effort.append(msg.effort)

        elif topic == '/rosout':
            # for older raw_measurements, the node name is /gather_control_node
            node_name = "/bhand_gather_node"
            # if msg.msg == node_name + " Waiting opened":
            if msg.msg == "Subscribing to /rosout":
                valid_data = True
                events["waiting_opened"] = t.to_sec()
            elif msg.msg == node_name + " CLOSING":
                events["closing"] = t.to_sec() - events["waiting_opened"]
            elif truncate and msg.msg == node_name + " Trial finished":
                events["stop"] = t.to_sec() - events["waiting_opened"]
                break
    events["start"] = 0
    bag.close()

    # Convert to numpy arrays
    arrays = [tact_palm, tact_f1, tact_f2, tact_f3,
              tact_time, joint_coords, joint_effort, joint_time]
    for i, _ in enumerate(arrays):
        arrays[i] = np.asarray(arrays[i])

    # Set t=0 to start
    arrays[4] = arrays[4] - arrays[4][0]
    arrays[7] = arrays[7] - arrays[7][0]

    tact_palm = arrays[0]
    tact_f1 = arrays[1]
    tact_f2 = arrays[2]
    tact_f3 = arrays[3]
    tact_t = arrays[4]
    joint_coords = arrays[5]
    joint_effort = arrays[6]
    joint_t = arrays[7]

    joint_coords_t = joint_coords.transpose()
    joint_effort_t = joint_effort.transpose()

    joint_names = ['F2_TIP', 'F1', 'F2', 'F3',
                   'F3_TIP', 'F1_TIP', 'SPREAD_1', 'SPREAD_2']
    joint_coords_tuple = [(joint_coords_t[i], joint_names[i])
                          for i in range(len(joint_names))]
    joint_effort_tuple = [(joint_effort_t[i], joint_names[i])
                          for i in range(len(joint_names))]
    tact_tuple = [(tact_palm, 'PALM'), (tact_f1, 'F1'),
                  (tact_f2, 'F2'), (tact_f3, 'F3')]

    avg_all_tact_tuple = average_all_taxels(tact_tuple, tact_t)
    plot_all_avg_tact(bag_name[:-4], tact_t, avg_all_tact_tuple, joint_t, joint_coords_tuple, joint_effort_tuple)

    # Plot parameters
    plt.rcParams['axes.grid'] = True
    plt.rcParams['axes.axisbelow'] = True
    # plt.rcParams['grid.linestyle'] = 'dashed'
    plt.rcParams['grid.linewidth'] = 0.5
    plt.rcParams['grid.alpha'] = 0.5
    plt.rcParams['lines.markersize'] = 1
    plt.rcParams['legend.fontsize'] = 'small'

    l = []
    for i in joint_effort:
        l.append(max(i))
    return max(l)


print("Bag file source set to " + bags_path)
if os.path.exists(check_path):
    shutil.rmtree(check_path)
os.mkdir(check_path)

tmp = []
entries = sorted(os.scandir(bags_path), key=lambda x: (x.is_dir(), x.name))
for entry in entries:
    if entry.name.endswith('.bag'):
        print("\n" + entry.name)
        tmp.append(extract_data(entry.name))
print("Max effor", max(tmp))
