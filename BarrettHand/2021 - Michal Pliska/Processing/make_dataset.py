import os
import shutil
import numpy as np
import rosbag
from config import bags_path, out_format, raw_dataset_path, raw_dataset_check_path
import matplotlib.pyplot as plt


def average_all_taxels(tact_tuple, tact_t):
    avg_all_tact_tuple = [None] * 4
    num_tact = len(tact_tuple)

    # Average values for each tactile surface
    for i in range(num_tact):
        average_tact = np.average(tact_tuple[i][0], axis=1)
        tact_label = tact_tuple[i][1]
        avg_all_tact_tuple[i] = (average_tact, tact_label)

    return avg_all_tact_tuple


def plot_all_avg_tact(npz_name, tact_t, avg_all_tact_tuple, joint_t, joint_coords_tuple, joint_effort_tuple):
    # Plot averaged tactile data
    colors = ["#ff3333", "#175887", "#db6600", "#217821"]
    for i, data in enumerate(avg_all_tact_tuple):
        plt.step(tact_t, data[0], label=data[1], c=colors[i])

    for i in [1, 2, 3, 5, 0, 4, 6, 7]:
        plt.step(joint_t, joint_coords_tuple[i]
        [0], label=joint_coords_tuple[i][1])

    for i in [5, 0, 4]:
        plt.step(joint_t, joint_effort_tuple[i]
        [0], label=joint_effort_tuple[i][1] + "-effort")

    plt.xlabel("time [s]")
    plt.ylabel("torque [N/m]")
    plt.legend()
    plt.xlabel("time [s]")
    plt.ylabel("pressure [N/cm2]")
    plt.legend()
    # fig = plt.gcf()
    # plt.title(npz_name + " averaged all tactile sensors")
    plt.savefig(raw_dataset_check_path + npz_name + "_avg-all-tact." +
                out_format, format=out_format)
    plt.close()
    return 0


def plot_npz(npz_file):
    npz_source = raw_dataset_path
    npz_name = npz_file[:-4] + ".npz"

    measurement = np.load(npz_source + npz_name)
    tact_palm = measurement['palm'].astype('float32')
    tact_f1 = measurement['f1'].astype('float32')
    tact_f2 = measurement['f2'].astype('float32')
    tact_f3 = measurement['f3'].astype('float32')
    tact_t = measurement['tact_t']
    tact_tuple = [(tact_palm, 'PALM'), (tact_f1, 'F1'),
                  (tact_f2, 'F2'), (tact_f3, 'F3')]
    joint_coords = measurement['coords'].astype('float32')
    joint_effort = measurement['effort'].astype('float32')
    joint_t = measurement['joint_t']
    joint_coords_t = joint_coords.transpose()
    joint_effort_t = joint_effort.transpose()

    joint_names = ['F2_TIP', 'F1', 'F2', 'F3',
                   'F3_TIP', 'F1_TIP', 'SPREAD_1', 'SPREAD_2']
    joint_coords_tuple = [(joint_coords_t[i], joint_names[i])
                          for i in range(len(joint_names))]
    joint_effort_tuple = [(joint_effort_t[i], joint_names[i])
                          for i in range(len(joint_names))]

    avg_all_tact_tuple = average_all_taxels(tact_tuple, tact_t)
    plot_all_avg_tact(npz_name, tact_t, avg_all_tact_tuple, joint_t, joint_coords_tuple, joint_effort_tuple)

    return len(tact_t)


def taxel_average(tact_palm, tact_f1, tact_f2, tact_f3, indice):
    num_tactsurf = 4
    num_sensors = 24
    taxel_averages = np.zeros((num_tactsurf, num_sensors))

    start_indice = indice - 100
    if start_indice < 0:
        start_indice = 0

    for r, each in enumerate([tact_palm, tact_f1, tact_f2, tact_f3]):
        each_new = np.transpose(each[start_indice:indice])
        for c in range(num_sensors):
            taxel_averages[r][c] = np.average(each_new[c])

    return taxel_averages


def data_statistics(bag_name):
    bag_path = bags_path + bag_name
    bag = rosbag.Bag(bag_path)
    events_t = {"start": None, "waiting_opened": None, "closing": None, "stop": None}
    events_i = {"start": None, "waiting_opened": None, "closing": None, "stop": None}

    counter = 0
    valid_data = False
    for topic, msg, t in bag.read_messages(topics=['/bhand_node/tact_array', '/joint_states', '/rosout']):
        if topic == '/rosout':
            # for older raw_measurements, the node name is /gather_control_node
            node_name = "/bhand_gather_node"
            # if msg.msg == node_name + " Waiting opened":
            if msg.msg == "Subscribing to /rosout":
                valid_data = True
                events_t["start"] = t.to_sec()
                events_i["start"] = counter
            if msg.msg == node_name + " Waiting opened":
                events_t["waiting_opened"] = t.to_sec() - events_t["start"]
                events_i["waiting_opened"] = counter
            elif msg.msg == node_name + " CLOSING":
                events_t["closing"] = t.to_sec() - events_t["start"]
                events_i["closing"] = counter
            elif msg.msg == node_name + " Trial finished":
                events_t["stop"] = t.to_sec() - events_t["start"]
                events_i["stop"] = counter
                break
        elif topic == '/joint_states' and valid_data:
            counter = counter + 1
    events_t["start"] = 0
    bag.close()
    return events_t, events_i


def extract_data(bag_name, length):
    correction = 0
    bag_path = bags_path + bag_name
    bag = rosbag.Bag(bag_path)
    npz_file = raw_dataset_path + bag_name[:-4]

    tact_palm, tact_f1, tact_f2, tact_f3 = [], [], [], []
    joint_coords, joint_effort = [], []
    tact_time, joint_time = [], []

    index = None
    closing = False
    stuck = False

    valid_data = False
    for topic, msg, t in bag.read_messages(topics=['/bhand_node/tact_array', '/joint_states', '/rosout']):
        if topic == '/bhand_node/tact_array' and valid_data:
            tact_time.append(t.to_sec())
            tact_palm.append(msg.palm)
            tact_f1.append(msg.finger1)
            tact_f2.append(msg.finger2)
            tact_f3.append(msg.finger3)

        elif topic == '/joint_states' and valid_data:
            joint_time.append(t.to_sec())
            if closing:
                joint_coords.append(msg.position)
            else:
                joint_coords.append((0, 0, 0, 0, 0, 0, 0, 0))
            joint_effort.append(msg.effort)

        elif topic == '/rosout':
            # for older raw_measurements, the node name is /gather_control_node
            node_name = "/bhand_gather_node"
            # if msg.msg == node_name + " Waiting opened":
            if msg.msg == "Subscribing to /rosout":
                valid_data = True
            elif "stuck" in msg.msg:
                stuck = True
            elif "CLOSING" in msg.msg:
                index = len(tact_time) - 1
                closing = True
            elif msg.msg == node_name + " Trial finished":
                break
    bag.close()

    # Convert to numpy arrays
    arrays = [tact_palm, tact_f1, tact_f2, tact_f3,
              tact_time, joint_coords, joint_effort, joint_time]

    if stuck:
        for j, _ in enumerate(arrays):
            arrays[j] = arrays[j][:-40]

    koefs = [1 / 4, 1 / 4, 1 / 4, 1 / 4, None, 1 / 2.7, 1. / 1.2, None]
    for j, _ in enumerate(arrays):
        arrays[j] = np.asarray(arrays[j])
        if j != 4 and j != 7:
            arrays[j] = koefs[j] * (arrays[j] - np.average(arrays[j][(index - 100):index], axis=0))
        arrays[j] = arrays[j][-(length + correction):]



    # Set t=0 to start
    arrays[4] = arrays[4] - arrays[4][0]
    arrays[7] = arrays[7] - arrays[7][0]


    # Save as npz files
    np.savez(npz_file, palm=arrays[0], f1=arrays[1], f2=arrays[2],
             f3=arrays[3], tact_t=arrays[4], coords=arrays[5], effort=arrays[6], joint_t=arrays[7])


if __name__ == "__main__":
    print("Bag file source set to " + bags_path)
    if os.path.exists(raw_dataset_path):
        shutil.rmtree(raw_dataset_path)
    os.mkdir(raw_dataset_path)
    os.mkdir(raw_dataset_check_path)

    events_t = {"start": [], "waiting_opened": [], "closing": [], "stop": []}
    events_i = {"start": [], "waiting_opened": [], "closing": [], "stop": []}

    closing = []
    entries = sorted(os.scandir(bags_path), key=lambda x: (x.is_dir(), x.name))
    print("Statistics..\n\n")
    for entry in entries:
        if entry.name.endswith('.bag'):
            #print("\n" + entry.name)
            t, i = data_statistics(entry.name)
            events_t["start"].append(t["start"])
            events_t["waiting_opened"].append(t["waiting_opened"])
            events_t["closing"].append(t["closing"])
            events_t["stop"].append(t["stop"])

            events_i["start"].append(i["start"])
            events_i["waiting_opened"].append(i["waiting_opened"])
            events_i["closing"].append(i["closing"])
            events_i["stop"].append(i["stop"])
    events_t["start"] = np.array(events_t["start"])
    events_t["waiting_opened"] = np.array(events_t["start"])
    events_t["closing"] = np.array(events_t["waiting_opened"])
    events_t["stop"] = np.array(events_t["closing"])
    events_i["start"] = np.array(events_i["stop"])
    events_i["waiting_opened"] = np.array(events_i["waiting_opened"])
    events_i["closing"] = np.array(events_i["closing"])
    events_i["stop"] = np.array(events_i["stop"])

    min_len = min(events_i["stop"]) + 1
    max_len = max(events_i["stop"]) + 1 - 40
    min_len_only_waiting = min(events_i["stop"] - events_i["waiting_opened"])
    max_len_only_waiting = max(events_i["stop"] - events_i["waiting_opened"]) - 40
    min_len_closing = min(events_i["stop"] - events_i["closing"])
    max_len_closing = max(events_i["stop"] - events_i["closing"]) - 40
    print(min_len, max_len, min_len_only_waiting, max_len_only_waiting, min_len_closing, max_len_closing)

    print("Dataset..\n\n")
    for entry in entries:
        if entry.name.endswith('.bag'):
            #print("\n" + entry.name)
            extract_data(entry.name, max_len_closing)

    print("Check..\n\n")
    tmp = []
    for entry in entries:
        if entry.name.endswith('.bag'):
            #print("\n" + entry.name)
            tmp.append(plot_npz(entry.name))
    print(min(tmp), max(tmp))
