import random
import os
import shutil
from config import raw_dataset_path, datasets_path

listed_dir = os.listdir(raw_dataset_path)
listed_dir.sort()

if os.path.exists(datasets_path):
    shutil.rmtree(datasets_path)
os.mkdir(datasets_path)

original = datasets_path + "original/"
new = datasets_path + "new/"
os.mkdir(original)
os.mkdir(new)

original_trn = original + "trn/"
original_val = original + "val/"
original_test = original + "test/"
os.mkdir(original_trn)
os.mkdir(original_val)
os.mkdir(original_test)

new_trn = new + "trn/"
new_val = new + "val/"
new_test = new + "test/"
os.mkdir(new_trn)
os.mkdir(new_val)
os.mkdir(new_test)
labels = ["darkbluedie", "yellowsponge", "bluedie", "pinkdie",
          "kinovacube", "whitedie", "ycbcube", "yellowcube", "bluecube"]

labels_dict = {"yellowsponge": [], "bluedie": [], "pinkdie": [], "kinovacube": [],
               "whitedie": [], "ycbcube": [], "yellowcube": [], "bluecube": [], "darkbluedie": []}

while listed_dir:
    file = listed_dir.pop()
    if file.endswith(".npz"):
        for label in labels:
            if label in file:
                labels_dict[label].append(file)
                break

samples = 30
for label, files in labels_dict.items():
    print(label + "-trn: ", len(files)-samples*2, label + "-val: ", samples, label + "-test: ", samples)
    random.shuffle(files)
    random.shuffle(files)
    random.shuffle(files)
    # more is of course better

    for file in files[0:samples]:
        shutil.copy(raw_dataset_path + file, new_test + file)
    for file in files[samples:(2*samples)]:
        shutil.copy(raw_dataset_path + file, new_val + file)
    for file in files[(2*samples):]:
        shutil.copy(raw_dataset_path + file, new_trn + file)

for num, (label, files) in enumerate(labels_dict.items()):
    random.shuffle(files)
    random.shuffle(files)
    random.shuffle(files)
    num_new_trn = original_trn + str(num) + "/"
    num_new_val = original_val + str(num) + "/"
    num_new_test = original_test + str(num) + "/"
    os.mkdir(num_new_trn)
    os.mkdir(num_new_val)
    os.mkdir(num_new_test)
    # more is of course better

    for file in files[0:samples]:
        shutil.copy(raw_dataset_path + file, num_new_test + file)
    for file in files[samples:(2*samples)]:
        shutil.copy(raw_dataset_path + file, num_new_val + file)
    for file in files[(2*samples):]:
        shutil.copy(raw_dataset_path + file, num_new_trn + file)





