import torch
import torch.nn as nn
import torch.optim as optim

from lstm_utilities import npz_to_tensor, create_loader, float_to_str
from lstm_model import BarrettLSTM
import math
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import numpy as np
import os


def create_dataset_exp(folder, desired_labels, reduced=False, ablation=None, return_weights=True):
    """
    Creates dataset from npz files in desired folder and can have either full or reduced dimensionality.
    Reduced dimensionality takes into account only active tactile cells and averages them into one value per tactile surface. TODO mention ablation
    A cell is considered as active when it crosses specified threshold.
    Full dimensionality: 4*24 tactile + 3 torque + 8 joint coordinates features.
    Reduced dimensionality: 4 tactile + 3 torque + 8 joint coordinates features.

    Parameters
    ----------
    folder : string
        Path to where npz files for desired dataset are stored.
    reduced : bool
        Flag for reducing dimensionality. Available for truncated data only.
    ablation : string
        None leaves all the data, "effort" removes effort data, "tact" removes tactile data.
    """
    if reduced:
        print("Creating a dataset with reduced dimensionality.")

    classes = 0
    dataset = []
    directories = os.scandir(folder)
    for directory in directories:
        if directory.is_dir:
            classes += 1

    count = np.array([0] * classes)

    directories = os.scandir(folder)
    for directory in directories:
        if directory.is_dir:
            class_folder = folder + directory.name + '/'
            entries = os.scandir(class_folder)
            for entry in entries:
                if entry.name.endswith('.npz'):
                    file_path = folder + directory.name + '/' + entry.name
                    data = npz_to_tensor(file_path, reduced, ablation)
                    label = int(directory.name)
                    dataset.append((data, label))
                    if return_weights:
                        count[label] += 1
    features = list(dataset[0][0][0].size())[0]

    if return_weights:
        weights = count/max(count)
        ret_values = dataset, features, weights
    else:
        ret_values = dataset, features
    return ret_values


def regression_plot_exp(all_outputs, desired_labels, all_labels, name):
    out_format = "pdf"
    outputs_by_labels = []
    for i, label in enumerate(all_labels):
        results_one_label = set()
        for j, one_output in enumerate(all_outputs):
            if one_output[0] == i:
                results_one_label.add(one_output[1])

        if len(results_one_label) > 0:
            results_one_label_tensor = torch.FloatTensor(
                list(results_one_label))
            outputs_by_labels.append(results_one_label_tensor)
        else:
            results_one_label_tensor = torch.FloatTensor(list([]))
            outputs_by_labels.append(results_one_label_tensor)
            print("Label '{}' missing.".format(label))

    outputs_by_labels = [x for _, x in sorted(
        zip(all_labels, outputs_by_labels))]
    all_labels = sorted(all_labels)

    for i, label in enumerate(all_labels):
        if len(outputs_by_labels[i]) != 0:
            x_values = [i] * len(outputs_by_labels[i])
            plt.scatter(x_values, outputs_by_labels[i], marker='+')
            plt.scatter(label, np.average(
                outputs_by_labels[i]), color='black', s=15)
        else:
            plt.scatter([], outputs_by_labels[i], marker='+')
            plt.scatter(label, [0], color='black', marker='', s=15)

        plt.scatter(i, int(label[: 2]), edgecolors='black',
                    facecolor='none', marker='s')

    legend_elements = [Line2D([0], [0], marker='o', color='w', label='Average', mfc='black'),
                       Line2D([0], [0], marker='s', color='w',
                              label='Reference', mfc='none', mec='black'),
                       Line2D([0], [0], marker='+', color='w', label='Measurement', mfc='black', mec='black')]
    plt.legend(handles=legend_elements)
    plt.xlabel("True stiffness [hPa]")
    plt.ylabel("Predicted stiffness [hPa]")
    # plt.xticks(len(all_labels))
    # plt.xlabel(all_labels)
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt2 = plt.gcf()
    plt.savefig(name + '.' + out_format, format=out_format)
    plt2.savefig(ds_name + ".png", format='png')
    plt.close()
    return 0


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print("Working with", device)

# Parameters
input_dim = None  # this will be overriden by creating dataset
hidden_dim = 256
layer_dim = 2
output_dim = 1  # this must be 1 if I want just one output value

n_epochs = 1000
best_mae = math.inf
patience, trials = 100, 0
bs = 256

labels = ["30 (gv5030)", "40 (gv5040)", "72 (n4072)", "40 (nf2140)", "29 (rl3529)", "40 (rl4040)", "45 (rl5045)", "25 (rp1725)", "40 (rp2440)", "65 (rp2865)",
          "55 (rp3555)", "45 (rp27045)", "48 (rp30048)", "80 (rp50080)", "20 (t1820)", "30 (t2030)", "45 (t2545)", "40 (t3240)", "15 (v4515)", "15 (v5015)"]
desired_labels = [30, 40, 72, 40, 29, 40, 45, 25,
                  40, 65, 55, 45, 48, 80, 20, 30, 45, 40, 15, 15]

print("Preparing datasets")
ds_name = "experiment_a3"
trn_folder = "/home.nfs/maresm13/Documents/" + ds_name + "/trn/"
trn_ds, input_dim = create_dataset_exp(
    trn_folder, desired_labels, return_weights=False)

print(f'Creating data loaders, batch size = {bs}')
trn_loader = create_loader(trn_ds, bs)

print("Creating model")
model = BarrettLSTM(input_dim, layer_dim, hidden_dim, output_dim)
model = model.to(device)
optimizer = optim.SGD(model.parameters(), lr=0.003, momentum=0.9)
# optimizer = optim.Adam(model.parameters(), lr=0.003, amsgrad=True)

criterion = nn.SmoothL1Loss()

# Training loop
print("Start model training")
for epoch in range(1, n_epochs + 1):
    # Model training
    model.train()
    error, total = 0, 0
    trn_outputs = []
    for i, (x_batch, len_batch, y_batch) in enumerate(trn_loader):
        x_batch = x_batch.to(device)
        len_batch = len_batch.to(device)
        # convert from int64 to float32
        y_batch = y_batch.type(torch.FloatTensor)
        y_batch = y_batch.to(device)

        optimizer.zero_grad()
        out = model(x_batch, len_batch)
        out = out.squeeze()  # need to get rid of the unecessary dimension
        y_batch_desired = [desired_labels[int(x)] for x in y_batch.tolist()]
        y_batch_desired = torch.FloatTensor(y_batch_desired).to(device)
        loss = criterion(out, y_batch_desired)
        loss.backward()
        optimizer.step()

        total += y_batch.size(0)
        error += torch.sum(abs(out - y_batch_desired))
        trn_outputs += [(y_batch[i], out[i]) for i in range(len(y_batch))]

    # Use mean absolute error instead of accuracy
    mae = error / total

    if epoch % 1 == 0:
        model_name = ds_name + '/' + 'e' + str(epoch) + '_' + float_to_str(mae)
        regression_plot_exp(trn_outputs, desired_labels,
                            labels, model_name + "_trn")
        print(f'Epoch: {epoch:3d}. Loss: {loss.item():.4f}. MAE: {mae:.4f}')
    else:
        trials += 1
        if trials >= patience:
            print(
                f'Early stopping on epoch {epoch}, best accuracy {best_mae:.4f} in epoch {epoch - 100}')
            break

print("The training is finished! Restoring the best model weights")
