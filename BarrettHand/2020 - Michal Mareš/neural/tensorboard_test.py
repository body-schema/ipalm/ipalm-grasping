# WORK IN PROGRESS

import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F

from lstm_model import BarrettLSTM
from lstm_utilities import create_dataset, create_loader, print_confusion_matrix, float_to_str
from torch.utils.tensorboard import SummaryWriter

writer = SummaryWriter("runs")
# ssh -L 16006:127.0.0.1:6006 maresm13@cantor.felk.cvut.cz to tunnel tensorboard port to my computer

name = "objects6"
labels = None
folder = None
ablation = None

full_object_labels = ["bbdie", "bdiecube", "germandie", "kinova",
                      "mpdie", "sbdie", "ycb", "yellowsponge", "yspongecube"]
reduced_object_labels = ["bdiecube", "germandie",
                         "kinova", "sbdie", "ycb", "yellowsponge", "yspongecube"]

if name == "foam20":
    # a1 + a3, s0.3, val 4 examples (2 for a1, 2 for a3)
    labels = ["gv5030", "gv5040", "n4072", "nf2140", "rl3529", "rl4040", "rl5045", "rp1725", "rp2440",
              "rp2865", "rp3555", "rp27045", "rp30048", "rp50080", "t1820", "t2030", "t2545", "t3240", "v4515", "v5015"]
    folder = "/home.nfs/maresm13/Documents/datasets/ds_foams/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"

elif name == "foam20_ableff":
    # a1 + a3, s0.3, val 4 examples (2 for a1, 2 for a3)
    labels = ["gv5030", "gv5040", "n4072", "nf2140", "rl3529", "rl4040", "rl5045", "rp1725", "rp2440",
              "rp2865", "rp3555", "rp27045", "rp30048", "rp50080", "t1820", "t2030", "t2545", "t3240", "v4515", "v5015"]
    folder = "/home.nfs/maresm13/Documents/datasets/ds_foams/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"
    ablation = "effort"

elif name == "foam20_abltact":
    # a1 + a3, s0.3, val 4 examples (2 for a1, 2 for a3)
    labels = ["gv5030", "gv5040", "n4072", "nf2140", "rl3529", "rl4040", "rl5045", "rp1725", "rp2440",
              "rp2865", "rp3555", "rp27045", "rp30048", "rp50080", "t1820", "t2030", "t2545", "t3240", "v4515", "v5015"]
    folder = "/home.nfs/maresm13/Documents/datasets/ds_foams/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"
    ablation = "tact"

elif name == "objects1":
    # action 1, trn s0.3, val s0.6
    labels = full_object_labels
    folder = "/home.nfs/maresm13/Documents/datasets/ds_objects1/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"

elif name == "objects1s":
    # action1, trn s0.6, val s0.3
    labels = full_object_labels
    folder = "/home.nfs/maresm13/Documents/datasets/ds_objects1/"
    trn_folder = folder + "val/"
    val_folder = folder + "trn/"

elif name == "objects2":
    # s0.3, trn a1, val a3
    labels = reduced_object_labels
    folder = "/home.nfs/maresm13/Documents/datasets/ds_objects2/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"

elif name == "objects3":
    # a1 + a3, trn s0.3, val s0.6
    labels = full_object_labels
    folder = "/home.nfs/maresm13/Documents/datasets/ds_objects3/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"

elif name == "objects4":
    # action3, trn s0.3, val s0.6
    labels = reduced_object_labels
    folder = "/home.nfs/maresm13/Documents/datasets/ds_objects4/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"

elif name == "objects4s":
    # action3, trn s0.6, val s0.3
    labels = reduced_object_labels
    folder = "/home.nfs/maresm13/Documents/datasets/ds_objects4/"
    trn_folder = folder + "val/"
    val_folder = folder + "trn/"

elif name == "objects5":
    # action1, trn s0.3, val s1.2
    labels = full_object_labels
    folder = "/home.nfs/maresm13/Documents/datasets/ds_objects5/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"

elif name == "objects6":
    # action1, trn s0.3, val s1.2
    labels = full_object_labels
    folder = "/home.nfs/maresm13/Documents/datasets/ds_objects6/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"

elif name == "action1-03":
    # action 1, speed 0.3
    labels = full_object_labels
    folder = "/home.nfs/maresm13/Documents/datasets/ds_action1-03/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"

elif name == "action3-03":
    # action 3, speed 0.3
    labels = reduced_object_labels
    folder = "/home.nfs/maresm13/Documents/datasets/ds_action3-03/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print("Working with", device)

# Parameters
input_dim = None  # this will be overriden by creating dataset
hidden_dim = 256
layer_dim = 2
output_dim = len(labels)

n_epochs = 1000
best_acc = 0
patience, trials = 100, 0
bs = 128

print("Preparing datasets")
trn_dataset, input_dim, _ = create_dataset(trn_folder, ablation=ablation)
val_dataset, input_dim, _ = create_dataset(val_folder, ablation=ablation)

print(f'Creating data loaders, batch size = {bs}')
trn_loader = create_loader(trn_dataset, bs)
val_loader = create_loader(val_dataset, bs)

print("Creating model")
model = BarrettLSTM(input_dim, layer_dim, hidden_dim, output_dim)
model = model.to(device)
# optimizer = optim.SGD(model.parameters(), lr=0.003, momentum=0.9)
optimizer = optim.Adam(model.parameters(), lr=0.003, amsgrad=True)
criterion = nn.CrossEntropyLoss()

# Training loop
print("Start model training")
for epoch in range(1, n_epochs + 1):
    # Model training
    model.train()
    for i, (x_batch, len_batch, y_batch) in enumerate(trn_loader):
        x_batch = x_batch.to(device)
        len_batch = len_batch.to(device)
        y_batch = y_batch.to(device)

        # optimizer.zero_grad()
        # output = model(x_batch, len_batch)
        # loss = criterion(output, y_batch)
        # loss.backward()
        # optimizer.step()

        writer.add_graph(model, (x_batch, len_batch), verbose=False)
        writer.close()
        exit()

    # Model evaluation
    correct, total = 0, 0
    model.eval()
    y_val_complete, y_pred_complete = [], []
    with torch.no_grad():
        for (x_val, len_val, y_val) in val_loader:
            x_val = x_val.to(device)
            len_val = len_val.to(device)
            y_val = y_val.to(device)

            out = model(x_val, len_val)
            preds = F.log_softmax(out, dim=1).argmax(dim=1)
            total += y_val.size(0)
            correct += (preds == y_val).sum().item()

            y_val_complete = y_val_complete + y_val.tolist()
            y_pred_complete = y_pred_complete + preds.tolist()

    acc = correct / total
    filename = "conf_matrix"
    print_confusion_matrix(filename, y_val_complete, y_pred_complete, labels)

    if epoch % 1 == 0:
        print(f'Epoch: {epoch:3d}. Loss: {loss.item():.4f}. Acc.: {acc:2.2%}')

    if acc > best_acc:
        trials = 0
        best_acc = acc
        text_acc = float_to_str(best_acc*100)
        model_name = name + '_' + text_acc + "_e" + str(epoch)
        torch.save(model.state_dict(), "models/" +
                   name + '/' + model_name + ".pth")
        cm_name = "models/" + name + '/' + model_name + "_cm"
        print_confusion_matrix(cm_name, y_val_complete,
                               y_pred_complete, labels)
        print_confusion_matrix(
            filename + "_best", y_val_complete, y_pred_complete, labels)
        print(f'Saved model from epoch {epoch}, accuracy: {best_acc:2.2%}')
        if int(acc) == 1:
            print("Reached 100% accuracy")
            break
    else:
        trials += 1
        if trials >= patience:
            print(
                f'Early stopping on epoch {epoch}, best accuracy {best_acc:2.2%} in epoch {epoch - 100}.')
            break

print("The training is finished!")
