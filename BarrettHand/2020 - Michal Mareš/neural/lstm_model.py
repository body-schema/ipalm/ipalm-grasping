import torch
import torch.nn as nn
import torch.nn.utils.rnn as rnn_utils


class BarrettLSTM(nn.Module):
    def __init__(self, input_dim, layer_dim, hidden_dim, output_dim):
        super().__init__()

        self.input_dim = input_dim
        self.layer_dim = layer_dim
        self.hidden_dim = hidden_dim
        self.output_dim = output_dim

        self.lstm = nn.LSTM(input_dim, hidden_dim, layer_dim, batch_first=True)
        self.fc1 = nn.Linear(hidden_dim, hidden_dim)
        self.fc2 = nn.Linear(hidden_dim, output_dim)

    def forward(self, x, x_lengths):
        # reset the LSTM hidden state. Must be done before you run a new batch. Otherwise the LSTM will treat
        # a new batch as a continuation of a sequence
        batch_size, seq_len, _ = x.size()

        # hn has the other layer's last hidden states as well
        x = rnn_utils.pack_padded_sequence(
            x, x_lengths, batch_first=True)
        # if (h_0, c_0) is not provided, both h_0 and c_0 default to zero.
        x, _ = self.lstm(x)
        x, x_lengths = rnn_utils.pad_packed_sequence(
            x, batch_first=True)  # these lengths are the same as input

        # select last unpadded hidden state for each sequence
        x = x.reshape(batch_size*seq_len, self.hidden_dim)
        last_unpadded_hidden = [i*seq_len + (l-1)
                                for i, l in enumerate(x_lengths)]
        last_unpadded_hidden = torch.tensor(
            last_unpadded_hidden, dtype=torch.int64)
        if torch.cuda.is_available():
            last_unpadded_hidden = last_unpadded_hidden.to('cuda')
        x = x.index_select(0, last_unpadded_hidden)
        x = x.view(batch_size, self.hidden_dim)

        x = self.fc1(x)
        out = self.fc2(x)
        return out


if __name__ == "__main__":
    print("This file contains just the network architecture and does nothing by itself. "
          "Run 'lstm_train.py' if you want to train it.")
