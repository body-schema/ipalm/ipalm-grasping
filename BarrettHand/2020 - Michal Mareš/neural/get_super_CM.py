import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F
import os

from lstm_model import BarrettLSTM
from lstm_utilities import create_dataset, create_loader, print_confusion_matrix, float_to_str

name = "neural_a1-03_256_r"
model_name = ["91'66_e116","91'66_e148","97'22_e306"]
model_path_arr = []
for it in range(1,4):
    model_path_arr.append('../' + name + str(it) + '/models/action1-03/action1-03_' + model_name[it-1] + '.pth')

print(model_path_arr[0])
print(model_path_arr[1])
labels = None
folder = None
ablation = None

full_object_labels = ["bbdie", "bdiecube", "germandie", "kinova",
                      "mpdie", "sbdie", "ycb", "yellowsponge", "yspongecube"]

labels = full_object_labels
folder = "../datasets/ds_action1-03/"
#here valset = testset
test_folder = folder + "val/"
filename = "conf_matrix_"+name[:-2]


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print("Working with", device)

# Parameters
input_dim = None  # this will be overriden by creating dataset
hidden_dim = 256
layer_dim = 2
output_dim = len(labels)
bs=128



print("Preparing datasets")
#trn_dataset, input_dim, _ = create_dataset(trn_folder, ablation=ablation)
#val_dataset, input_dim, _ = create_dataset(val_folder, ablation=ablation)
test_dataset, input_dim, _ = create_dataset(test_folder)



print(f'Creating data loaders, batch size = {bs}')
test_loader = create_loader(test_dataset, bs)


print("Creating model")
model = BarrettLSTM(input_dim, layer_dim, hidden_dim, output_dim)

for i, model_path in enumerate(model_path_arr):
    model.load_state_dict(torch.load(model_path))
    model = model.to(device)


    # Model evaluation
    correct, total = 0, 0
    model.eval()
    y_test_complete, y_pred_complete = [], []
    with torch.no_grad():
        for j, (x_test, len_test, y_test) in enumerate(test_loader):
            x_test = x_test.to(device)
            len_test = len_test.to(device)
            y_test = y_test.to(device)

            out = model(x_test, len_test)
            preds = F.log_softmax(out, dim=1).argmax(dim=1)
            total += y_test.size(0)
            correct += (preds == y_test).sum().item()


            y_test_complete = y_test_complete + y_test.tolist()
            y_pred_complete = y_pred_complete + preds.tolist()
            print_confusion_matrix(filename+'_r'+str(i+1)+'_nonnorm', y_test_complete, y_pred_complete, labels, normalize=None)

        if i==0:
            y_test_complete_all = y_test_complete
            y_pred_complete_all = y_pred_complete
        else:
            y_test_complete_all=y_test_complete_all+y_test_complete
            y_pred_complete_all=y_pred_complete_all+y_pred_complete
        #print(y_test_complete_all)
        #print(y_pred_complete_all)


    acc = correct / total
    ii=i+1
    print(f'Acc. r{ii}: {acc:2.2%}, correct: {correct}, total: {total}')



print_confusion_matrix(filename+'_nonnorm', y_test_complete_all, y_pred_complete_all, labels, normalize=None)
print_confusion_matrix(filename, y_test_complete_all, y_pred_complete_all, labels)
