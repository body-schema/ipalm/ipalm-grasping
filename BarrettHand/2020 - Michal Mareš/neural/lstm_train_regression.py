import torch
import torch.nn as nn
import torch.optim as optim

from lstm_utilities import create_dataset, create_loader, get_labels_from_folder, float_to_str
from lstm_model import BarrettLSTM
import math
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import numpy as np


def regression_plot(all_outputs, all_labels, name):
    out_format = "pdf"
    outputs_by_labels = []
    for i, label in enumerate(all_labels):
        results_one_label = set()
        for j, one_output in enumerate(all_outputs):
            if one_output[0] == label:
                results_one_label.add(one_output[1])

        if len(results_one_label) > 0:
            results_one_label_tensor = torch.tensor(list(results_one_label))
            outputs_by_labels.append(results_one_label_tensor)
        else:
            print("Label '{}' missing.".format(label))

    for i, label in enumerate(all_labels):
        x_values = [label] * len(outputs_by_labels[i])
        plt.scatter(x_values, outputs_by_labels[i], marker='+')

        plt.scatter(label, label, edgecolors='black',
                    facecolor='none', marker='s')
        plt.scatter(label, np.average(outputs_by_labels[i]), color='black')
    legend_elements = [Line2D([0], [0], marker='o', color='w', label='Average', mfc='black'),
                       Line2D([0], [0], marker='s', color='w',
                              label='Reference', mfc='none', mec='black'),
                       Line2D([0], [0], marker='+', color='w', label='Measurement', mfc='black', mec='black')]
    plt.legend(handles=legend_elements)
    plt.xlabel("True stiffness [hPa]")
    plt.ylabel("Predicted stiffness [hPa]")
    plt.tight_layout()
    plt.savefig(name + '.' + out_format, format=out_format)
    plt.close()
    return 0


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print("Working with", device)

# Parameters
input_dim = None  # this will be overriden by creating dataset
hidden_dim = 256
layer_dim = 2
output_dim = 1  # this must be 1 if I want just one output value

n_epochs = 1000
best_mae = math.inf
patience, trials = 100, 0
bs = 256

ds_name = "stiffregres_new"
print("Preparing datasets")
trn_folder = "/home.nfs/maresm13/Documents/datasets/" + ds_name + "/trn/"
val_folder = "/home.nfs/maresm13/Documents/datasets/" + ds_name + "/val/"
trn_ds, input_dim = create_dataset(trn_folder, return_weights=False)
val_ds, input_dim = create_dataset(val_folder, return_weights=False)

print(f'Creating data loaders, batch size = {bs}')
trn_loader = create_loader(trn_ds, bs)
val_loader = create_loader(val_ds, bs)

print("Creating model")
model = BarrettLSTM(input_dim, layer_dim, hidden_dim, output_dim)
model = model.to(device)
optimizer = optim.SGD(model.parameters(), lr=0.003, momentum=0.9)
# optimizer = optim.Adam(model.parameters(), lr=0.003, amsgrad=True)

criterion = nn.SmoothL1Loss()

# Training loop
print("Start model training")
for epoch in range(1, n_epochs + 1):
    # Model training
    model.train()
    trn_outputs = []
    for i, (x_batch, len_batch, y_batch) in enumerate(trn_loader):
        x_batch = x_batch.to(device)
        len_batch = len_batch.to(device)
        # convert from int64 to float32
        y_batch = y_batch.type(torch.FloatTensor)
        y_batch = y_batch.to(device)

        optimizer.zero_grad()
        out = model(x_batch, len_batch)
        out = out.squeeze()  # need to get rid of the unecessary dimension
        loss = criterion(out, y_batch)
        loss.backward()
        optimizer.step()

        trn_outputs += [(y_batch[i], out[i]) for i in range(len(y_batch))]

    trn_labels = get_labels_from_folder(trn_folder)
    trn_labels = [int(x) for x in trn_labels]
    regression_plot(trn_outputs, trn_labels, "stiffregres_trn")

    # Model evaluation
    error, total = 0, 0
    model.eval()
    val_outputs = []
    with torch.no_grad():
        for (x_val, len_val, y_val) in val_loader:
            x_val = x_val.to(device)
            len_val = len_val.to(device)
            y_val = y_val.type(torch.FloatTensor)
            y_val = y_val.to(device)

            out = model(x_val, len_val)
            out = out.squeeze()

            total += y_val.size(0)
            error += torch.sum(abs(out - y_val))
            val_outputs += [(y_val[i], out[i]) for i in range(len(y_val))]

    val_labels = get_labels_from_folder(val_folder)
    val_labels = [int(x) for x in val_labels]
    regression_plot(val_outputs, val_labels, "stiffregres_val")
    # Use mean absolute error instead of accuracy
    mae = error / total

    if epoch % 1 == 0:
        print(f'Epoch: {epoch:3d}. Loss: {loss.item():.4f}. MAE: {mae:.4f}')

    if mae < best_mae:
        trials = 0
        best_mae = mae
        best_model_name = "models/" + ds_name + "/" + \
            ds_name + '_' + float_to_str(mae) + '_e' + str(epoch)

        torch.save(model.state_dict(), best_model_name + ".pth")
        regression_plot(trn_outputs, trn_labels, best_model_name + "_trn")
        regression_plot(val_outputs, val_labels, best_model_name + "_val")
        print(f'Saved model from epoch {epoch}, MAE: {best_mae:.4f}')
    else:
        trials += 1
        if trials >= patience:
            print(
                f'Early stopping on epoch {epoch}, best accuracy {best_mae:.4f} in epoch {epoch - 100}')
            break

print("The training is finished! Restoring the best model weights")
