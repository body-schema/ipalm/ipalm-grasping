import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F

from lstm_model import BarrettLSTM
from lstm_utilities import create_dataset, create_loader, float_to_str, npz_to_tensor

import numpy as np
import matplotlib.pyplot as plt

import os


def create_dataset_bin(folder, label, reduced=False, tactile_thresh=0.8):
    if reduced:
        print(
            "Creating a dataset with reduced dimensionality, threshold = ", tactile_thresh)

    dataset = []

    entries = os.scandir(folder)
    for entry in entries:
        if entry.name.endswith('.npz'):
            file_path = folder + entry.name
            data = npz_to_tensor(file_path, reduced, tactile_thresh)
            dataset.append((data, label))

    return dataset


def confusion_matrix_bin(fn, cm, labels, labels_text, label_bins, save=True, normalize='true'):
    """
    Labels need to be ordered from 0 to n_classes
    """

    file_format = "pdf"

    # sorting
    cm_sorted = [x for _, x in sorted(zip(labels_text, cm))]
    labels_sorted = sorted(labels_text)

    if normalize == 'true':
        for r, row in enumerate(cm_sorted):
            row_sum = sum(row)
            for c, cell in enumerate(row):
                cm_sorted[r][c] = cell/row_sum

    if save:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax = ax.imshow(cm_sorted, cmap=plt.cm.Blues)

        for (i, j), z in np.ndenumerate(cm_sorted):
            if z == 0:
                continue

            if normalize == 'true':
                z_str = '{:.2f}'.format(z)
                if z < 1:
                    z_str = z_str[1:]
                elif z == 1:
                    z_str = z_str[:1]
            else:
                z_str = '{:.0f}'.format(z)
            ax.text(j, i, z_str, ha='center', va='center',
                    size='xx-small', color='w')

        fig.colorbar(cax)
        ax.get_yaxis().set_tick_params(pad=80)
        ax.set_xticks(np.arange(3))
        ax.set_yticks(np.arange(len(labels_text)))
        ax.set_xticklabels(label_bins)
        ax.set_yticklabels(labels_sorted, ha="left")
        plt.setp(ax.get_xticklabels(), rotation=45,
                 ha="right", rotation_mode="anchor")

        plt.title('Confusion matrix of the classifier')
        plt.xlabel('Predicted')
        plt.ylabel('True')
        # fig.subplots_adjust(left=-0.1, right=0.20)

        fig.tight_layout()
        box = ax.get_position()
        if len(labels_text) == 4:
            box.x0 = box.x0 - 0.06
            box.x1 = box.x1 - 0.06
        else:
            box.x0 = box.x0 - 0.2
            box.x1 = box.x1 - 0.2
        ax.set_position(box)
        plt.savefig(fn + '.' + file_format, format=file_format)
        plt.close()
    else:
        print(cm)
    return 0


name = "stiffbin"

if name == "stiffbin":
    labels_text = ["30 (gv5030)", "40 (gv5040)", "72 (n4072)", "40 (nf2140)", "29 (rl3529)", "40 (rl4040)", "45 (rl5045)", "25 (rp1725)", "40 (rp2440)", "65 (rp2865)",
                   "55 (rp3555)", "45 (rp27045)", "48 (rp30048)", "80 (rp50080)", "20 (t1820)", "30 (t2030)", "45 (t2545)", "40 (t3240)", "15 (v4515)", "15 (v5015)"]
    labels = [0, 1, 2, 1, 0, 1, 1, 0,
              1, 2, 1, 1, 1, 2, 0, 0, 1, 1, 0, 0]
    folder = "/home.nfs/maresm13/Documents/datasets/ds_stiffbin/"
    label_bins = ["15-30", "40-55", "65-80"]

elif name == "stiffbin2":
    labels_text = ["72 (n4072)", "40 (rl4040)", "48 (rp30048)", "20 (t1820)"]
    labels = [2, 1, 1, 0]
    folder = "/home.nfs/maresm13/Documents/datasets/ds_stiffbin2/"
    label_bins = ["15-30", "40-55", "65-80"]

elif name == "stiffbin3":
    labels_text = ["72 (n4072)", "40 (rl4040)", "48 (rp30048)", "20 (t1820)"]
    labels = [2, 1, 1, 0]
    folder = "/home.nfs/maresm13/Documents/datasets/ds_stiffbin3/"
    label_bins = ["15-30", "40-55", "65-80"]

elif name == "stiffbin4":
    labels_text = ["72 (n4072)", "40 (rl4040)", "48 (rp30048)", "20 (t1820)"]
    labels = [2, 1, 1, 0]
    folder = "/home.nfs/maresm13/Documents/datasets/ds_stiffbin4/"
    label_bins = ["15-30", "40-55", "65-80"]

elif name == "densebin":
    # same dataset as stiffbin, but different labels
    labels_text = ["050 (gv5030)", "050 (gv5040)", "040 (n4072)", "021 (nf2140)", "035 (rl3529)", "040 (rl4040)", "050 (rl5045)", "017 (rp1725)", "024 (rp2440)", "028 (rp2865)",
                   "035 (rp3555)", "270 (rp27045)", "300 (rp30048)", "500 (rp50080)", "018 (t1820)", "020 (t2030)", "025 (t2545)", "032 (t3240)", "045 (v4515)", "050 (v5015)"]
    labels = [1, 1, 1, 0, 0, 1, 1, 0,
              0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 1, 1]
    folder = "/home.nfs/maresm13/Documents/datasets/ds_densebin/"
    label_bins = ["15-35", "40-50", "270-500"]

elif name == "densebin2":
    labels_text = ["020 (t2030)", "028 (rp2865)",
                   "050 (gv5040)", "300 (rp30048)"]
    labels = [0, 0, 1, 2]
    folder = "/home.nfs/maresm13/Documents/datasets/ds_densebin2/"
    label_bins = ["15-35", "40-50", "270-500"]

elif name == "densebin3":
    labels_text = ["020 (t2030)", "028 (rp2865)",
                   "050 (gv5040)", "300 (rp30048)"]
    labels = [0, 0, 1, 2]
    folder = "/home.nfs/maresm13/Documents/datasets/ds_densebin3/"
    label_bins = ["15-35", "40-50", "270-500"]

elif name == "densebin4":
    labels_text = ["020 (t2030)", "028 (rp2865)",
                   "050 (gv5040)", "300 (rp30048)"]
    labels = [0, 0, 1, 2]
    folder = "/home.nfs/maresm13/Documents/datasets/ds_densebin4/"
    label_bins = ["15-35", "40-50", "270-500"]

trn_folder = folder + "trn/"
val_folder = folder + "val/"
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print("Working with", device)

# Parameters
input_dim = None  # this will be overriden by creating dataset
hidden_dim = 256
layer_dim = 2
output_dim = len(label_bins)  # i have 3 bins to sort foams

n_epochs = 1000
best_acc = 0
patience, trials = 100, 0
bs = 128

print("Preparing datasets")
trn_dataset, input_dim, weights = create_dataset(trn_folder)
val_dataset_arr = [None] * len(labels_text)

for i in range(len(labels_text)):
    val_dataset_arr[i] = create_dataset_bin(val_folder + str(i) + '/', i)

print(f'Creating data loaders, batch size = {bs}')
trn_loader = create_loader(trn_dataset, bs)
val_loader_arr = [None] * len(labels_text)
for i in range(len(labels_text)):
    val_loader_arr[i] = create_loader(val_dataset_arr[i], bs)

print("Creating model")
model = BarrettLSTM(input_dim, layer_dim, hidden_dim, output_dim)
model = model.to(device)
# optimizer = optim.SGD(model.parameters(), lr=0.003, momentum=0.9)
optimizer = optim.Adam(model.parameters(), lr=0.003, amsgrad=True)
class_weights = torch.FloatTensor(weights).to(device)
criterion = nn.CrossEntropyLoss(weight=class_weights)

# Training loop
print("Start model training")
for epoch in range(1, n_epochs + 1):
    # Model training
    model.train()
    for i, (x_batch, len_batch, y_batch) in enumerate(trn_loader):
        x_batch = x_batch.to(device)
        len_batch = len_batch.to(device)
        y_batch = y_batch.to(device)

        optimizer.zero_grad()
        output = model(x_batch, len_batch)
        loss = criterion(output, y_batch)
        loss.backward()
        optimizer.step()

    # Model evaluation
    correct, total = 0, 0
    model.eval()
    cm = []
    with torch.no_grad():
        for i in range(len(labels_text)):
            y_val_complete, y_pred_complete = [], []

            for (x_val, len_val, y_val) in val_loader_arr[i]:
                x_val = x_val.to(device)
                len_val = len_val.to(device)
                y_val = y_val.to(device)

                out = model(x_val, len_val)
                preds = F.log_softmax(out, dim=1).argmax(dim=1)
                total += y_val.size(0)
                y_translated = [labels[y] for y in y_val]
                y_translated = torch.FloatTensor(y_translated).to(device)
                correct += (preds == y_translated).sum().item()

                y_val_complete = y_val_complete + y_val.tolist()
                y_pred_complete = y_pred_complete + preds.tolist()

            y_pred_complete = np.array(y_pred_complete)
            arr2 = [np.count_nonzero(y_pred_complete == 0), np.count_nonzero(
                y_pred_complete == 1), np.count_nonzero(y_pred_complete == 2)]
            cm.append(arr2)

    acc = correct / total
    filename = "conf_matrix"
    confusion_matrix_bin(filename, cm, labels, labels_text, label_bins)

    if epoch % 1 == 0:
        print(f'Epoch: {epoch:3d}. Loss: {loss.item():.4f}. Acc.: {acc:2.2%}')

    if acc > best_acc:
        trials = 0
        best_acc = acc
        text_acc = float_to_str(best_acc*100)
        torch.save(model.state_dict(), "models/" + name +
                   '/' + name + '_' + text_acc + "_e" + str(epoch) + ".pth")
        cm_name = "models/" + name + '/' + name + \
            '_' + text_acc + "_e" + str(epoch) + "_cm"
        confusion_matrix_bin(cm_name, cm, labels, labels_text, label_bins)
        confusion_matrix_bin(filename + "_best", cm,
                             labels, labels_text, label_bins)
        print(f'Saved model from epoch {epoch}, accuracy: {best_acc:2.2%}')
        if int(acc) == 1:
            print("Reached 100% accuracy")
            break
    else:
        trials += 1
        if trials >= patience:
            print(
                f'Early stopping on epoch {epoch}, best accuracy {best_acc:2.2%} in epoch {epoch - 100}.')
            break

print("The training is finished!")
