# LSTM NN

## `lstm_train_*.py`
Loads datast, creates a data loader and trains model. After each epoch the model is evaluated. If it is better than the best one so far, weights are saved. If there's no advancement in the last 100 epochs, training is stopped.
1. `lstm_train_bin.py` – calssifies objects into categories ("bins")
1. `lstm_train_class.py` – classifies objects
1. `lstm_train_regression.py` – estimates stiffness, i.e. continuous output

## `lstm_model.py`
Neural network architecture. Padded sequences are passed into a LSTM layer. From the output of the LSTM, the last hidden state corresponding to an unpadded input is selected and passed into a linear layer. A second linear layer follows with number of outputs corresponding to the number of classes.

## `lstm_utilities.py`
Contains methods for dataset and data loader creation, printing confusion matrices and detecting active taxels.

## `get_super_CM.py`
Get a "super" confusion matrix which sum up more repetitions of the experiment. Also output accuracy of each experiment and confusion matrix of each experiment.

## `lstm_test_class.py`
Get a confusion matrix and accuracy of the experiment.
