# LSTM NN - other grippers than BarrettHand

## `lstm_utilities.py`
Modified for 2 channels in contrast with more channels in BarrettHand. It assumes the channels are named (position, current).  
However, the names of the channels might be different for different grippers (depends on preprocessing). However, as it is an internal parameter, it would work even if the channels of the gripper have originally different names (e.g. force, width) and are only named as (position, current) during preprocessing.

Used preprocessing Matlab script is in /ipalm-grasping/MATLAB_functions/cycles_separation_for_obj_class
