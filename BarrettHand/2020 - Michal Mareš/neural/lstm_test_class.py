import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F
import os

from lstm_model import BarrettLSTM
from lstm_utilities import create_dataset, create_loader, print_confusion_matrix, float_to_str

name = "foam20_ti"
model_name = "88'88_e5000"
model_path = "./models/" + name + '/' + name + '_' + model_name + '.pth'
labels = None
folder = None
ablation = None

full_object_labels = ["bbdie", "bdiecube", "germandie", "kinova",
                      "mpdie", "sbdie", "ycb", "yellowsponge", "yspongecube"]
reduced_object_labels = ["bdiecube", "germandie",
                         "kinova", "sbdie", "ycb", "yellowsponge", "yspongecube"]

if name == "foam20":
    # a1 + a3, s0.3, val 4 examples (2 for a1, 2 for a3)
    labels = ["gv5030", "gv5040", "n4072", "nf2140", "rl3529", "rl4040", "rl5045", "rp1725", "rp2440",
              "rp2865", "rp3555", "rp27045", "rp30048", "rp50080", "t1820", "t2030", "t2545", "t3240", "v4515", "v5015"]
    folder = "../datasets/ds_foams/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"

if name == "foam20_ti":
    # a1 + a3, s0.3, val 4 examples (2 for a1, 2 for a3)
    labels = ["gv5030", "gv5040", "n4072", "nf2140", "rl3529", "rl4040", "rl5045", "rp1725", "rp2440",
              "rp2865", "rp3555", "rp27045", "rp30048", "rp50080", "t1820", "t2030", "t2545", "t3240", "v4515", "v5015"]
    folder = "../datasets/ds_foams_ti/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"
    test_folder = folder + "test/"

elif name == "foam20_ableff":
    # a1 + a3, s0.3, val 4 examples (2 for a1, 2 for a3)
    labels = ["gv5030", "gv5040", "n4072", "nf2140", "rl3529", "rl4040", "rl5045", "rp1725", "rp2440",
              "rp2865", "rp3555", "rp27045", "rp30048", "rp50080", "t1820", "t2030", "t2545", "t3240", "v4515", "v5015"]
    folder = "../datasets/ds_foams/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"
    ablation = "effort"


elif name == "objects1s":
    # action1, trn s0.6, val s0.3
    labels = full_object_labels
    folder = "../datasets/ds_objects1/"
    trn_folder = folder + "val/"
    val_folder = folder + "trn/"

elif name == "test0":
    # a1 + a3, s0.3, val 4 examples (2 for a1, 2 for a3)
    labels = ["obj1", "obj2"]
    folder = "../datasets/test0/"
    trn_folder = folder + "trn/"
    val_folder = folder + "val/"

elif name == "Michal-set_a1-03eq":
    # action1, trn s0.6, val s0.3
    labels = full_object_labels
    folder = "../datasets/Michal-set_a1-03eq/"
    trn_folder = folder + "val/"
    val_folder = folder + "trn/"
    test_folder = folder + "test/"

elif name == "action1-03_ti":
    # action 1, speed 0.3
    labels = full_object_labels
    folder = "../datasets/ds_action1-03_test_incl/"
    test_folder = folder + "test/"
    




else:
    print("Dataset not found, exiting...")
    exit()


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print("Working with", device)

# Parameters
input_dim = None  # this will be overriden by creating dataset
hidden_dim = 256
layer_dim = 2
output_dim = len(labels)

n_epochs = 1000
best_acc = 0
patience, trials = 1000, 0
bs = 128

print("Preparing datasets")
#trn_dataset, input_dim, _ = create_dataset(trn_folder, ablation=ablation)
#val_dataset, input_dim, _ = create_dataset(val_folder, ablation=ablation)
test_dataset, input_dim, _ = create_dataset(test_folder)



print(f'Creating data loaders, batch size = {bs}')
test_loader = create_loader(test_dataset, bs)


print("Creating model")
model = BarrettLSTM(input_dim, layer_dim, hidden_dim, output_dim)
model.load_state_dict(torch.load(model_path))
model = model.to(device)


# Model evaluation
correct, total = 0, 0
model.eval()
y_test_complete, y_pred_complete = [], []
with torch.no_grad():
    for (x_test, len_test, y_test) in test_loader:
        x_test = x_test.to(device)
        len_test = len_test.to(device)
        y_test = y_test.to(device)

        out = model(x_test, len_test)
        preds = F.log_softmax(out, dim=1).argmax(dim=1)
        total += y_test.size(0)
        correct += (preds == y_test).sum().item()

        y_test_complete = y_test_complete + y_test.tolist()
        y_pred_complete = y_pred_complete + preds.tolist()

acc = correct / total
filename = "conf_matrix_test_"+model_name
print_confusion_matrix(filename, y_test_complete, y_pred_complete, labels)

print(f'Acc.: {acc:2.2%}, correct: {correct}, total: {total}')
