# source: http://environnement.ens.fr/IMG/file/DavidPDF/SSA_beginners_guide_v9.pdf

import glob
import os

import matplotlib.pyplot as plt
import numpy as np


def load_npz(npz_path, use_effort=True, use_tactile=True, use_joint=True):
    meas = np.load(npz_path)
    tact_palm = meas['palm'].astype('float32')
    tact_f1 = meas['f1'].astype('float32')
    tact_f2 = meas['f2'].astype('float32')
    tact_f3 = meas['f3'].astype('float32')
    tact_t = meas['tact_t'].astype('float32')
    joint_coords = meas['coords'].astype('float32')
    effort = meas['effort'].astype('float32')
    joint_t = meas['joint_t'].astype('float32')

    effort = effort.transpose()
    effort = np.array([effort[5], effort[0], effort[4]])  # select just the fingertip sensors
    effort = effort.transpose()

    seq_len = min(len(tact_t), len(joint_t))

    effort = effort[:seq_len]
    tact = np.hstack((tact_palm[:seq_len], tact_f1[:seq_len], tact_f2[:seq_len], tact_f3[:seq_len]))
    joint = joint_coords[:seq_len]

    if use_effort and use_tactile and use_joint:
        ret = np.hstack((effort, tact, joint))
    elif use_effort and not use_tactile and use_joint:
        ret = np.hstack((effort, joint))
    elif use_effort and use_tactile and not use_joint:
        ret = np.hstack((effort, tact))
    elif use_effort and not use_tactile and not use_joint:
        ret = np.hstack(([effort]))
    elif not use_effort and use_tactile and use_joint:
        ret = np.hstack((tact, joint))
    elif not use_effort and use_tactile and not use_joint:
        ret = np.hstack(([tact]))
    elif not use_effort and not use_tactile and use_joint:
        ret = np.hstack(([joint]))

    return ret


def ssa_decomposition(time_series, embed_dim=4, view_output=False, csv_file_eigenvalues=None):
    # returns -1 if data contain nan
    # normalize the time series, that is remove the mean value and divide it by the standard deviation (for each series separately)
    normalized_ts = np.ones(time_series.shape)
    mean_f1 = np.mean(time_series, 0)
    std_f1 = np.std(time_series, 0)

    time_series = time_series.transpose()
    normalized_ts = normalized_ts.transpose()
    for c in range(0, len(time_series)):
        normalized_ts[c] = (time_series[c] - mean_f1[c]) / std_f1[c]
    X = normalized_ts

    M = embed_dim  # window length = embedding dimension
    N = len(X[0])  # length of the time series
    D = X.shape[0]  # dimension of input signal

    Y = np.zeros((N, D * M))
    Y = Y.transpose()
    for c in range(0, D):  # normalize the input signal
        for i in range(0, M):
            new_column = c * M + i
            Y[new_column][0:N - i] = X[c][i:N]

    Y = Y.transpose()

    C = np.transpose(Y) @ Y / N  # covariance matrix, from definition if time series X has zero mean and unit variance

    if np.isnan(np.sum(C)):
        return -1

    # Calculate eigenvalues LAMBDA and eigenvectors RHO
    LAMBDA, RHO = np.linalg.eig(C)

    indices = LAMBDA.argsort()  #  sort ascending --> largest eigenvalue at the end
    LAMBDA = LAMBDA[indices[::-1]]
    RHO = RHO[:, indices[::-1]]

    # figure(3)
    # set(gcf,'name','Eigenvectors RHO and eigenvalues LAMBDA')
    # clf
    # subplot(3,1,1)
    # plot(LAMBDA,'o-')
    # subplot(3,1,2)
    # plot(RHO(:,1:2), '-')
    # legend('1', '2')
    # subplot(3,1,3)
    # plot(RHO(:,3:4), '-')
    # legend('3', '4')

    # Print eigeinvalues
    if csv_file_eigenvalues is not None:
        first_eigen_percent = LAMBDA[0] / np.sum(LAMBDA[0:4])
        f.write("{:.2}".format(first_eigen_percent))

    # Calculate principal components PC
    PC = Y @ RHO

    PC = PC.transpose()

    if view_output:
        fig, axes = plt.subplots(M)
        fig.suptitle('Principal components PCs')
        for i in range(0, M):
            if M > 1:
                axes[i].plot(PC[i][:], 'b-')
            else:
                axes.plot(PC[i][:], 'b-')
            # ylabel(sprintf('PC %d', i))
        plt.show()

    return PC[:M]


if __name__ == "__main__":
    src_folder = os.path.join("/home.nfs/maresm13/Documents/clustering/npy_objects/")
    dst_folder = os.path.join("/home.nfs/maresm13/Documents/clustering/joint_only/ssa/")
    npz_file_list = glob.glob(os.path.join(src_folder, "*.npz"))

    total = len(npz_file_list)

    f = open(os.path.join(dst_folder, "../0_eigenvalues.csv"), "w+")  # for the eigenvalues

    for i, file in enumerate(npz_file_list):
        f.write("{},".format(os.path.split(file)[-1]))
        sensor_data = load_npz(file, use_effort=False, use_tactile=False, use_joint=True)
        ssa_pc_series = ssa_decomposition(sensor_data, embed_dim=1, csv_file_eigenvalues=f)
        if isinstance(ssa_pc_series, int):
            if ssa_pc_series == -1:
                print("File {} contains nan and cannot be used", file)
                # this is caused by the barrett hand controller
                # possible improvement: replace the nans by average of the left and right value
                continue

        file_dst = os.path.join(dst_folder, os.path.split(file[:-4])[-1])
        np.save(file_dst, ssa_pc_series)
        print("{} / {}".format(i + 1, total), end="\r")
        f.write("\n")

    f.close()

    print("All done! {} / {}".format(i + 1, total))
