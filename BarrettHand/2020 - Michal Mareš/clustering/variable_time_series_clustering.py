# sources:
# https://tslearn.readthedocs.io/en/stable/variablelength.html
# https://tslearn.readthedocs.io/en/stable/auto_examples/clustering/plot_kmeans.html#sphx-glr-auto-examples-clustering-plot-kmeans-py
import glob
import os

import matplotlib.pyplot as plt
import numpy as np
from tslearn.clustering import KernelKMeans, TimeSeriesKMeans
from tslearn.utils import to_time_series_dataset

min_clusters = 2
max_clusters = 9

src_folder = os.path.join("/home.nfs/maresm13/Documents/clustering/tact_only/ssa/")
dst_folder = os.path.join("/home.nfs/maresm13/Documents/clustering/tact_only/output/")

list_of_names = ["bbdie", "bdiecube", "germandie", "kinova", "mpdie", "sbdie", "ycb", "yellowsponge", "yspongecube"]
list_of_colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
list_of_actions = ["a1", "a3"]
list_of_line_types = [":", "-."]


def save_csv(f, clusters, y_pred, y_label):
    num_meas = len(y_label)
    for i in range(clusters):
        f.write('"Cluster {}",'.format(i))
        for j in range(num_meas):
            if i == y_pred[j]:
                f.write("{},".format(y_label[j]))
        f.write("\n")


seed = 2020
np.random.seed(seed)

npz_file_list = glob.glob(os.path.join(src_folder, "*.npy"))
num_meas = len(npz_file_list)

X_input = [[None]] * num_meas
y_label = [None] * num_meas
for i, file in enumerate(npz_file_list):
    loaded_data = np.squeeze(np.load(file))
    X_input[i] = list(loaded_data)
    y_label[i] = os.path.split(file[:-4])[-1]
    print("Loaded {} / {}".format(i + 1, num_meas), end="\r")

print("Loaded {} / {}".format(i + 1, num_meas))
num_meas = len(y_label)
print(num_meas)
X_input = to_time_series_dataset(X_input)

# All SSAs
plt.figure()
for pos, xx in enumerate(X_input):
    line_color, line_type = None, None
    for index, name in enumerate(list_of_names):
        if name in y_label[pos]:
            line_color = list_of_colors[index]
    for index, action in enumerate(list_of_actions):
        if action in y_label[pos]:
            line_type = list_of_line_types[index]
        # make it dashed or something else
        pass

    # prasárnička
    if xx[0] < 0:
        xx = -xx
        X_input[pos] = -X_input[pos]
    plt.plot(xx.ravel(), color=line_color, linestyle=line_type, linewidth=.5)

colors_legend = dict(zip(list_of_names, list_of_colors))
labels = list(colors_legend.keys())
handles = [plt.Rectangle((0, 0), 1, 1, color=colors_legend[label]) for label in labels]
plt.legend(handles, labels)
plt.savefig(os.path.join(dst_folder, "ssa_all.pdf"))
plt.clf()
print("Figure of all SSA's done")

for clusters in range(min_clusters, max_clusters + 1):
    # Compute clusters
    km = TimeSeriesKMeans(n_clusters=clusters, metric="dtw", n_jobs=-1, verbose=True)
    y_pred = km.fit_predict(X_input)

    # Plot clusters as time series
    plt.figure()
    for yi in range(clusters):
        plt.subplot(3, 3, yi + 1)
        for pos, xx in enumerate(X_input):
            line_color, line_type = None, None
            if y_pred[pos] == yi:
                for index, name in enumerate(list_of_names):
                    if name in y_label[pos]:
                        line_color = list_of_colors[index]
                for index, action in enumerate(list_of_actions):
                    if action in y_label[pos]:
                        line_type = list_of_line_types[index]
                    # make it dashed or something else
                    pass

                plt.plot(xx.ravel(), color=line_color, linestyle=line_type, linewidth=.5, alpha=.2)
        plt.plot(km.cluster_centers_[yi].ravel(), "r-")
        plt.title("C {}".format(yi + 1))
        # plt.xlim(0, resample_size)
        # plt.ylim(-4, 4)
        # plt.text(0.55, 0.85, 'Cluster %d' % (yi + 1), transform=plt.gca().transAxes)
    # sorry, there's no space for legend
    # colors_legend = dict(zip(list_of_names, list_of_colors))
    # labels = list(colors_legend.keys())
    # handles = [plt.Rectangle((0, 0), 1, 1, color=colors_legend[label]) for label in labels]
    # plt.legend(handles, labels, bbox_to_anchor=(1, 0), loc="lower right", ncol=len(labels))
    plt.tight_layout()
    plt.savefig(os.path.join(dst_folder, "tskm_dtw_c{}.pdf".format(clusters)))
    plt.clf()

    # Plot bar graphs
    B1 = np.array([0] * clusters)
    B3 = np.array([0] * clusters)
    plt.figure()
    for i_name, name in enumerate(list_of_names):  # for each name
        A1 = np.array([0] * clusters)
        A3 = np.array([0] * clusters)
        for i_cluster, _ in enumerate(range(clusters)):  # look at each cluster
            for i_meas, pred in enumerate(y_pred):  # for each prediction which is in j-th cluster
                if pred == i_cluster:  # if the prediction is j-th cluster
                    if name in y_label[i_meas]:  # is the current object name in the real label?
                        if list_of_actions[0] in y_label[i_meas]:
                            A1[i_cluster] += 1  # action 1
                        elif list_of_actions[1] in y_label[i_meas]:
                            A3[i_cluster] += 1  # action 3

        plt.bar(np.arange(clusters) - 0.2, A1, bottom=B1, color=list_of_colors[i_name], width=0.4)  # action 1
        plt.bar(np.arange(clusters) + 0.2, A3, bottom=B3, color=list_of_colors[i_name], width=0.4)  # action 3
        cluster_xticks = ["C {}".format(x + 1) for x in np.arange(clusters)]
        plt.xticks(np.arange(clusters), cluster_xticks)
        B1, B3 = B1 + A1, B3 + A3

    colors_legend = dict(zip(list_of_names, list_of_colors))
    labels = list(colors_legend.keys())
    handles = [plt.Rectangle((0, 0), 1, 1, color=colors_legend[label]) for label in labels]
    plt.legend(handles, labels)
    plt.savefig(os.path.join(dst_folder, "tskm_dtw_c{}_bar.pdf".format(clusters)))
    plt.clf()

    # Save CSV file
    f = open(os.path.join(dst_folder, "tskm_dtw_c{}.csv".format(clusters)), "w+")
    save_csv(f, clusters, y_pred, y_label)
    f.close()

""" KernelKMeans with gak kernel not working ATM """
# for clusters in range(min_clusters, max_clusters + 1):
#     gak_km = KernelKMeans(n_clusters=clusters, kernel="gak", n_init=20, n_jobs=-1, verbose=True)
#     y_pred = gak_km.fit_predict(X_input)

#     plt.figure()
#     for yi in range(3):
#         plt.subplot(4, 5, 1 + yi)
#         for xx in X_input[y_pred == yi]:
#             plt.plot(xx.ravel(), "k-", alpha=.2)
#         # plt.xlim(0, sz)
#         # plt.ylim(-4, 4)
#         # plt.title("Cluster %d" % (yi + 1))
#     plt.savefig(os.path.join(dst_folder, "kkm_gak_c{}.pdf".format(clusters)))
#     plt.clf()

#     f = open(os.path.join(dst_folder, "kkm_gak_c{}.csv".format(clusters)), "w+")
#     save_csv(clusters, y_pred, y_label)
#     f.close()
