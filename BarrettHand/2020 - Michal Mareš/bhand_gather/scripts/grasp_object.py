#!/usr/bin/env python3
import getopt
import shlex
import subprocess
import sys
import time

from config import output_path, object_name, number, config, speed, thresh

import rospy
from bhand_controller.msg import Service, State, TactileArray
from bhand_controller.srv import Actions, SetControlMode
from rospy.exceptions import ROSException
from sensor_msgs.msg import JointState


class BarrettHandControl():
    def __init__(self, speed, thresh, config):
        """Sets up variables and starts subscribers, publishers and services."""
        self.freq = 50
        self.bhand_data = None
        self.tact_data = None
        self.desired_ref = JointState()
        self.bhand_node_name = "bhand_node"  # pre-defined by the controller
        self.hand_ready = False
        self.stop = False
        self.maximum_finger_jc = 2.44
        self.last_pos = None
        self.timer = None

        rospy.init_node('bhand_gather_node', log_level=rospy.INFO)
        # args = rospy.myargv(argv=sys.argv)

        # if the parameter is inside the node tag, ~ is required
        self.speed = speed
        rospy.logwarn("Setting speed to %f", self.speed)
        self.thresh = thresh
        rospy.logwarn("Setting tactile threshold to %f", self.thresh)
        self.config = config

        # Topics
        self.topic = '/%s/state' % self.bhand_node_name
        self.tact_topic = '/%s/tact_array' % self.bhand_node_name
        self.command_topic = '/%s/command' % self.bhand_node_name
        self.actions_service_name_topic = '/%s/actions' % self.bhand_node_name
        self.set_mode_service_name_topic = '/%s/set_control_mode' % self.bhand_node_name
        self.joint_states_topic = '/joint_states'

        # Subscribers
        try:
            self.subscriber = rospy.Subscriber(
                self.topic, State, self.receive_state_data)
        except ValueError as e:
            rospy.logerr(rospy.get_caller_id() +
                         " Error connecting topic (%s)" % e)
        try:
            self.tact_subscriber = rospy.Subscriber(
                self.tact_topic, TactileArray, self.receive_tact_data)
        except ValueError as e:
            rospy.logerr(rospy.get_caller_id() +
                         " Error connecting topic (%s)" % e)
        try:
            self.joint_subscriber = rospy.Subscriber(
                self.joint_states_topic, JointState, self.receive_joints_data)
        except ValueError as e:
            rospy.logerr(rospy.get_caller_id() +
                         " Error connecting topic (%s)" % e)

        # Services
        try:
            self.service_set_mode = rospy.ServiceProxy(
                self.set_mode_service_name_topic, SetControlMode)
        except ValueError as e:
            rospy.logerr(rospy.get_caller_id() +
                         " Error connecting service (%s)" % e)
        try:
            self.service_bhand_actions = rospy.ServiceProxy(
                self.actions_service_name_topic, Actions)
        except ValueError as e:
            rospy.logerr(rospy.get_caller_id() +
                         " Error connecting service (%s)" % e)

        # Publishers
        try:
            self.talker()
        except rospy.ROSInterruptException:
            pass

    def talker(self):
        """Publishes values to close barret hand."""
        try:
            self.publisher_command = rospy.Publisher(
                self.command_topic, JointState, queue_size=10)
        except ROSException as e:
            rospy.logerr(rospy.get_caller_id() + " Error creating publisher for topic %s (%s)" % (
                self.command_topic, e))

        rate = rospy.Rate(self.freq)
        while not rospy.is_shutdown():
            if self.stop:
                break
            elif self.hand_ready:
                # Send command to the barrett
                self.desired_ref.name = [
                    'bh_j11_joint', 'bh_j32_joint', 'bh_j12_joint', 'bh_j22_joint']
                self.desired_ref.position = [0, 0, 0, 0]
                self.desired_ref.velocity = [
                    0, self.speed, self.speed, self.speed]
                self.desired_ref.effort = [0.0, 0.0, 0.0, 0.0]
                self.publisher_command.publish(self.desired_ref)
                rospy.logdebug(rospy.get_caller_id() +
                               " Sent command to %s" % self.command_topic)
            rate.sleep()

        rospy.logwarn(rospy.get_caller_id() + " Trial finished, opening hand")
        self.send_bhand_action(Service.OPEN_GRASP)

    def receive_state_data(self, msg):
        """Handles the messages from the barrett hand controller."""
        if self.bhand_data is None:
            self.bhand_data = State()
            if not msg.hand_initialized:
                rospy.logwarn(rospy.get_caller_id() + " Initializing hand")
                self.send_bhand_action(Service.INIT_HAND)

        elif self.bhand_data.control_mode != 'VELOCITY' and self.bhand_data.state == 300 and not self.hand_ready:
            rospy.logwarn("Setting finger configuration to '%s'",
                          self.config)
            if self.config == 1 or self.config == 2:
                self.send_bhand_action(Service.SET_GRASP_1)
            elif self.config == 3:
                self.send_bhand_action(Service.SET_GRASP_2)
            else:
                rospy.logerr(rospy.get_caller_id(
                ) + " Unknown finger configuration '%s', using the default", self.config)
            time.sleep(2)  # wait till the configuration is set

            self.set_control_mode('VELOCITY')
            rospy.logwarn(rospy.get_caller_id() +
                          " LET'S GET THIS PARTY STARTED!")
            self.hand_ready = True

        rospy.logdebug(rospy.get_caller_id() +
                       " Received msg from %s" % self.topic)
        self.bhand_data = msg

    def receive_tact_data(self, msg):
        """Handles the messages from tactile sensors."""
        rospy.logdebug(rospy.get_caller_id() +
                       " Received msg from %s" % self.tact_topic)

        max_tact_value = 0
        if msg.palm:
            max_tact_value = max(max(msg.finger1), max(
                msg.finger2), max(msg.finger3), max(msg.palm))

        if not self.stop and max_tact_value > self.thresh:
            rospy.logwarn("Reached threshold pressure on tactile sensors")
            self.stop = True

    def receive_joints_data(self, msg):
        """Handles the messages from joints."""
        diff = [1, 1, 1, 1, 1, 1]
        if not self.hand_ready:
            pass
        elif self.last_pos is None:
            self.last_pos = msg.position
            self.timer = time.time()
        elif time.time() - self.timer > 1:
            for i in range(0, 6):
                diff[i] = msg.position[i] - self.last_pos[i]
            self.last_pos = msg.position
            self.timer = time.time()

        if not self.stop:
            if max(msg.position[0:6]) >= self.maximum_finger_jc:
                rospy.logwarn("Reached maximum joint coordinates on fingers")
                self.stop = True
            elif (self.last_pos is not None) and (max(diff) <= 0):
                rospy.logwarn("Hand stuck, opening now")
                self.stop = True

    def set_control_mode(self, mode):
        """
        Calls the service to set the control mode of the hand.

        Parameters
        ----------
        mode : str
            Desired barret hand mode of operation ('PID', 'VELOCITY')
        """
        try:
            self.service_set_mode(mode)
            rospy.logwarn("Control mode set to %s", mode)
        except ValueError as e:
            rospy.logerr('BHandGUI::set_control_mode: (%s)' % e)  # TODO
        except rospy.ServiceException as e:
            rospy.logerr('BHandGUI::set_control_mode: (%s)' % e)  # TODO

    def send_bhand_action(self, action):
        """
        Positions the hand into pre-defined position according to numerical value.

        Parameters
        ----------
        action : int
            Action number (defined in msgs/Service.msg)
        """
        try:
            self.service_bhand_actions(action)
        except ValueError as e:
            rospy.logerr(rospy.get_caller_id() +
                         " send_bhand_action: (%s)" % e)
        except rospy.ServiceException as e:
            rospy.logerr(rospy.get_caller_id() +
                         " send_bhand_action: (%s)" % e)


"""
Configurations
1: figers are opposide to each other and object is placed on the palm
2: figers are opposite to each other and object is held in the air, so only fingertips touch it
3: fingers are located on one side and squeeze the object against the palm
"""

options, remainder = getopt.getopt(
    sys.argv[1:], 'c:s:t:', ['name=', 'number=', 'config=', 'speed=', 'thresh='])
for opt, arg in options:
    if opt in ('--name'):
        object_name = arg
    elif opt in ('--number'):
        number = int(arg)
    elif opt in ('-c', '--config'):
        config = int(arg)
    elif opt in ('-s', '--speed'):
        speed = float(arg)
    elif opt in ('-t', '--thresh'):
        thresh = float(arg)

bag_name = object_name + "_c" + \
    str(config) + "_s" + str(speed) + "_t" + \
    str(thresh) + "_n" + str(number) + ".bag"
bag_path = output_path + bag_name
print("Measurement will be saved as ", bag_path)

robag_cmd = "rosbag record -a __name:=rosbag_barrett -O" + bag_path
robag_cmd = shlex.split(robag_cmd)
rosbag_record = subprocess.Popen(robag_cmd)

run_experiment = BarrettHandControl(speed, thresh, config)

kill_cmd = "rosnode kill /rosbag_barrett"
kill_cmd = shlex.split(kill_cmd)
rosbag_kill = subprocess.Popen(kill_cmd)

print("Experiment successfully finished!")
