import os

import numpy as np

import rosbag
from config import bags_path, npz_path


def extract_data(bag_name, truncate=True):
    """
    Analyzes rosbag messages, divides them into categories and saves them as a numpy npz file.

    Parameters
    ----------
    bag_name : string
        Name of the bag file.
    truncate : bool
        When True, measurement starts after the initialization phase and ends after the finished message is received.
    """
    bag_path = bags_path + bag_name
    bag = rosbag.Bag(bag_path)
    npz_file = npz_path + bag_name[:-4]

    # if os.path.exists(npz_file + ".npz"):
    #    return 0
    # print(bag)

    # Array definitions
    tact_palm, tact_f1, tact_f2, tact_f3 = [], [], [], []
    joint_coords, joint_effort = [], []
    tact_time, joint_time = [], []

    if truncate:
        valid_data = False
    else:
        valid_data = True

    # Play rosbag
    for topic, msg, t in bag.read_messages(topics=['/bhand_node/tact_array', '/joint_states', '/rosout']):
        if topic == '/bhand_node/tact_array' and valid_data:
            tact_time.append(t.to_sec())
            tact_palm.append(msg.palm)
            tact_f1.append(msg.finger1)
            tact_f2.append(msg.finger2)
            tact_f3.append(msg.finger3)

        elif topic == '/joint_states' and valid_data:
            joint_time.append(t.to_sec())
            joint_coords.append(msg.position)
            joint_effort.append(msg.effort)

        elif topic == '/rosout':
            # for older measurements, the node name is /gather_control_node
            node_name = "/bhand_gather_node"
            if msg.msg == node_name + " LET'S GET THIS PARTY STARTED!":
                valid_data = True
            elif truncate and msg.msg == node_name + " Trial finished, opening hand":
                break
    bag.close()

    if len(tact_time) != 0 and len(joint_time) != 0:
        # Convert to numpy arrays
        arrays = [tact_palm, tact_f1, tact_f2, tact_f3,
                  tact_time, joint_coords, joint_effort, joint_time]
        for i, _ in enumerate(arrays):
            arrays[i] = np.asarray(arrays[i])

        # Set t=0 to start
        arrays[4] = arrays[4] - arrays[4][0]
        arrays[7] = arrays[7] - arrays[7][0]

        # Save as npz files
        np.savez(npz_file, palm=arrays[0], f1=arrays[1], f2=arrays[2],
                 f3=arrays[3], tact_t=arrays[4], coords=arrays[5], effort=arrays[6], joint_t=arrays[7])
    else:
        print("No valid data found, skipping " + bag_path)
    return 0


if __name__ == "__main__":
    print("Bag file source set to " + bags_path)
    if not os.path.exists(npz_path):
        os.mkdir(npz_path)

    with os.scandir(bags_path) as entries:
        for entry in entries:
            if entry.name.endswith('.bag'):
                print("\n" + entry.name)
                print(25*'-')
                extract_data(entry.name)
            elif entry.is_dir:
                pass
            else:
                print("\nNot a *.bag file: " + entry.name)
