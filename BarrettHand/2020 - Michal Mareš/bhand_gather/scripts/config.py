# This file defines the default values.
# Default values will still be overriden by parameters passed through the command line.

# grasp_object.py
output_path = "/media/mares/DATA/meas/"
object_name = "unnamed"
number = 0
config = 1
speed = 0.3
thresh = 5

# process_data.py
bags_path = output_path
npz_path = output_path + "npz/"
