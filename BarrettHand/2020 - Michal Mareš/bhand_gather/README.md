# Manual
Basic documentation of the Barrett Hand can be found here: http://wiki.ros.org/bhand_controller.

# What does this ROS package do?
The goal of this package is to make gathering data from tactile and torque sensors of barrett hand as easy as possible.

## `grasp_object.py`
Requires the **bhand_controller.launch** from **iiwa_bhand_bringup** package running. Starts the "squeezing process" and saves data with rosbag. To specify the squeezing parameters and filename, following parameters can be modified:
* **speed:** sets desired joint speed
    * range <0.01; 2>, higher values cause unpredictable behavior
    * units rad/s, defaults to 0.3
* **thresh:** maximum pressure on any of the taxels
    * units N/cm2, defaults to 10.0
* **action:** sets gripper's finger configuration
    * implemented options are 1 (opposing fingers) or R (sided fingers), defaults to 1
* **object:** object name to specify rosbag
    * defaults to "unnamed"
* **path:** path to directory where bags should be saved

Experiment is then automatically ended when one of the following events arises:
* joints stop moving
* joints are in their maximum position
* pressure threshold on any tactile cell is exceeded

## `process_data.py`
Reads bag files from the output directory, converts them to numpy format which is then saved as a .npz file. The end of experiment is determined precisely by specific message in the `/rosout` topic.

## `config.py`
Sets the default values for output path, object name, number, finger configuration, speed and threshold.

## Run
`rosrun bhand_gather grasp_object.py`
You may need to `chmod +x grasp_object.py` first.

# Install as standalone ROS package
If you wish to install this folder of the repository as a standalone ROS package, navigate to your catkin workspace src folder and run:
```
mkdir ipalm-grasping
cd ipalm-grasping
git init
git config core.sparseCheckout true
git remote add origin <url>
echo "/gather_data/" > .git/info/sparse-checkout
git fetch
git pull origin master
```

# TODOs
* Dependencies of the package
* License if published