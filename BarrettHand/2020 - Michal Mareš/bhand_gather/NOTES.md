# Basic ROS
Listen to a topic:
```
rostopic echo [topic]
```

Show topic type and message:
```
rostopic type [topic]
rosmsg show [previous return value]
```

# Barrett hand control
Start the BH driver with tactile sensors:
```
roslaunch iiwa_bhand_bringup bhand_controller.launch
```

Control by actions (action 1 is INIT):
```
rosservice call /bhand_node/actions "action: 1"
```

Control by GUI:
```
roslaunch iiwa_bhand_bringup bhand_gui.launch 
```

## Change control mode
```
rosservice call /bhand_node/set_control_mode "mode: '[mode]'" 
```
Mode should be VELOCITY or PID.

## Joint control
Example message:
```
rostopic pub -1 /bhand_node/command sensor_msgs/JointState "{header: auto, name: ['bh_j11_joint', 'bh_j32_joint', 'bh_j12_joint', 'bh_j22_joint'], position: [0.4, 0.4, 0.4, 1], velocity: [0, 0, 0, 0], effort: [0.0, 0.0, 0.0, 0.0]}"
```

## Velocity control
Example message:
```
rostopic pub -r 50 /bhand_node/command sensor_msgs/JointState "{header: auto, name: ['bh_j11_joint', 'bh_j32_joint', 'bh_j12_joint', 'bh_j22_joint'], position: [0, 0, 0, 0], velocity: [0, 0.1, 0.1, 0.1], effort: [0.0, 0.0, 0.0, 0.0]}"
```
