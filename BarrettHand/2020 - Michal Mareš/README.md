# `gather_data`
ROS package for data collection automatization using Barrett Hand.

# `neural`
Created LSTM neural network model is located there, along with training and dataset creation utilities.

# `preprocessing`
Contains tools to convert bag files to npz files, visualization, data truncation and active taxel detection.

# `clustering` - work in progress
Set of tools trying to achieve unsupervised clustering of measured data.