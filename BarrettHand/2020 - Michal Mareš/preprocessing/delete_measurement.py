from config import bags_path, npz_path, npz_trunc_path
import os

while(True):
    print("Enter file name without the extension to be deleted from all folders:")
    file = input()
    os.remove(bags_path + file + '.bag')
    os.remove(npz_path + file + '.npz')
    os.remove(npz_trunc_path + file + '.npz')

    print("Removed" + file)
