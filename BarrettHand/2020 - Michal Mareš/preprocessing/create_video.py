import os
import subprocess
import math
import numpy as np
import matplotlib.pyplot as plt
from plot_data import average_all_taxels


def plot_tact_with_effort(tact_t, avg_tact_tuple, joint_t, joint_effort_tuple, end):
    colors = ["#ffb675", "", "", "", "#5fd35f", "#55a7e2"]
    fig, ax1 = plt.subplots()
    ax1.set_xlabel("time [s]")
    ax1.set_ylabel("torque [N/m]")
    ax1.set_xlim(0, tact_t[-1])
    ax1.set_ylim(0, 0.5)

    for i in [5, 0, 4]:
        ax1.step(joint_t[:end], joint_effort_tuple[i][0][:end],
                 label=joint_effort_tuple[i][1] + " torque", c=colors[i])
    handles1, labels1 = ax1.get_legend_handles_labels()

    colors = ["#ff3333", "#175887", "#db6600", "#217821"]
    ax2 = ax1.twinx()
    ax2.set_ylim(0.2, 1)
    ax2.set_ylabel("pressure [N/cm2]")

    for i, data in enumerate(avg_tact_tuple):
        ax2.step(tact_t[:end], data[0][:end],
                 label=data[1] + " pressure", c=colors[i])

    handles2, labels2 = ax2.get_legend_handles_labels()
    plt.legend(handles1 + handles2, labels1 + labels2, loc='upper left')
    plt.tight_layout()
    fig = plt.gcf()
    plt.close()
    return fig


source_npz = "/Volumes/DATA/meas/npz/bdie_c1_s0.3_t20.0_n0.npz"

# Plot parameters
plt.rcParams['axes.grid'] = True
plt.rcParams['axes.axisbelow'] = True
plt.rcParams['grid.linewidth'] = 0.5
plt.rcParams['grid.alpha'] = 0.5
plt.rcParams['lines.markersize'] = 1
plt.rcParams['legend.fontsize'] = 'small'

measurement = np.load(source_npz)
tact_palm = measurement['palm'].astype('float32')
tact_f1 = measurement['f1'].astype('float32')
tact_f2 = measurement['f2'].astype('float32')
tact_f3 = measurement['f3'].astype('float32')
tact_t = measurement['tact_t']
joint_coords = measurement['coords'].astype('float32')
joint_effort = measurement['effort'].astype('float32')
joint_t = measurement['joint_t']

tact_tuple = [(tact_palm, 'PALM'), (tact_f1, 'F1'),
              (tact_f2, 'F2'), (tact_f3, 'F3')]
avg_all_tact_tuple = average_all_taxels(tact_tuple, tact_t)

joint_coords_t = joint_coords.transpose()
joint_effort_t = joint_effort.transpose()

joint_names = ['F2_TIP', 'F1', 'F2', 'F3',
               'F3_TIP', 'F1_TIP', 'SPREAD_1', 'SPREAD_2']
joint_coords_tuple = [(joint_coords_t[i], joint_names[i])
                      for i in range(len(joint_names))]
joint_effort_tuple = [(joint_effort_t[i], joint_names[i])
                      for i in range(len(joint_names))]

png_folder = "/Volumes/DATA/png"
time = min(len(joint_t), len(tact_t))
max_time = math.floor(tact_t[time-1]*100)/100
fps = 30
num_frames = max_time * fps
time_step = math.floor(time/num_frames)
print("Creating {} frames".format(num_frames))
j = 0
for i in range(1, time, time_step):
    im = plot_tact_with_effort(
        tact_t, avg_all_tact_tuple, joint_t, joint_effort_tuple, i)
    im.savefig(png_folder + "/file%02d.png" % j)
    print("\rCreated {} frames".format(j), end='')
    j += 1

os.chdir(png_folder)
subprocess.call(['ffmpeg', '-framerate', str(fps), '-i',
                 'file%02d.png', '-pix_fmt', 'yuv420p', 'video_name.mp4'])
