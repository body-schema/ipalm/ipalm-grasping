out_format = "pdf"

# Path have to end with '/'
# folder_path = "/Volumes/DATA/Barrett_Hand/objects_180320/"
# folder_path = "/Volumes/DATA/Barrett_Hand/foams_180320/"
# folder_path = "/Volumes/DATA/Barrett_Hand/cubes_020320/"
# folder_path = "/media/mares/DATA/Barrett_Hand/objects_180320/"
# folder_path = "/media/mares/DATA/Barrett_Hand/foams_180320/"
# folder_path = "/media/mares/DATA/Barrett_Hand/cubes_020320/"

# folder_path = "/Volumes/DATA/Barrett_Hand/plots/"
# folder_path = "/media/mares/DATA/Barrett_Hand/objects1-2_160420/"
# folder_path = "/Volumes/DATA/Barrett_Hand/objects1-2_160420/"
# folder_path = "/Volumes/DATA/meas/"
folder_path = '/Users/mares/Documents/MATLAB/'

bags_path = folder_path + "bags/"
npz_path = folder_path + "npz/"
npz_trunc_path = folder_path + "npz_trunc/"
graphs_path = folder_path + "graphs/"
