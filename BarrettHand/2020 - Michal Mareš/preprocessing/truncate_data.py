import os

import numpy as np

import matplotlib.pyplot as plt
from config import npz_path, npz_trunc_path
from utilities import average_all_taxels

indice = None


def onpick(event):
    """
    Picker to select first contact point.
    """
    global indice
    indice = event.ind
    plt.close()


def select_from_tact_with_effort(npz_name, tact_t, avg_tact_tuple, joint_t, joint_effort_tuple):
    """
    Plots the data and handles user interaction.
    """

    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    ax1.set_xlabel("time [s]")

    colors = ["#ff3333", "#175887", "#db6600", "#217821"]
    ax1.set_ylabel("pressure [N/cm2]")
    for i, data in enumerate(avg_tact_tuple):
        ax1.step(tact_t, data[0], label=data[1] + " pressure", c=colors[i])
    handles1, labels1 = ax1.get_legend_handles_labels()

    colors = ["#ffb675", "", "", "", "#5fd35f", "#55a7e2"]
    ax2.set_ylabel("torque [N/m]")
    for i in [5, 0, 4]:
        ax2.plot(joint_t, joint_effort_tuple[i][0],
                 label=joint_effort_tuple[i][1] + " torque", c=colors[i], picker=1)
    handles2, labels2 = ax2.get_legend_handles_labels()

    plt.legend(handles1 + handles2, labels1 + labels2)
    plt.title(npz_name + " select start on one of the torque values")

    cid = fig.canvas.mpl_connect('pick_event', onpick)
    figManager = plt.get_current_fig_manager()
    figManager.full_screen_toggle()
    plt.show()

    fig.canvas.mpl_disconnect(cid)
    plt.clf()
    return 0


def nejaka_funkce(tact_palm, tact_f1, tact_f2, tact_f3, indice):
    num_tactsurf = 4
    num_sensors = 24
    taxel_averages = np.zeros((num_tactsurf, num_sensors))

    start_indice = indice - 100
    if start_indice < 0:
        start_indice = 0

    for r, each in enumerate([tact_palm, tact_f1, tact_f2, tact_f3]):
        each_new = np.transpose(each[start_indice:indice])
        for c in range(num_sensors):
            taxel_averages[r][c] = np.average(each_new[c])

    return taxel_averages


def truncate_data(npz_file, npz_source):
    """
    Parses data from npz file, plots them and lets user select moment of first contact between the gripper and the object. Truncated data are then saved as a new npz file.

    Parameters
    ----------
    npz_file : string
        Name of the input npz file including the file extension.
    npz_source : string
        Path to the input npz file.
    """
    global indice

    measurement = np.load(npz_source + npz_file)
    tact_palm = measurement['palm'].astype('float32')
    tact_f1 = measurement['f1'].astype('float32')
    tact_f2 = measurement['f2'].astype('float32')
    tact_f3 = measurement['f3'].astype('float32')
    tact_t = measurement['tact_t']
    joint_coords = measurement['coords'].astype('float32')
    joint_effort = measurement['effort'].astype('float32')
    joint_t = measurement['joint_t']

    # Configure path where rosbags are stored
    npz_name = npz_file[: -4]

    tact_tuple = [(tact_palm, 'PALM'), (tact_f1, 'F1'),
                  (tact_f2, 'F2'), (tact_f3, 'F3')]
    avg_all_tact_tuple = average_all_taxels(tact_tuple, tact_t)

    joint_effort_trans = joint_effort.transpose()
    joint_names = ['F2_TIP', 'F1', 'F2', 'F3',
                   'F3_TIP', 'F1_TIP', 'SPREAD_1', 'SPREAD_2']
    joint_effort_tuple = [(joint_effort_trans[i], joint_names[i])
                          for i in range(len(joint_names))]

    # Effort and tactile data
    select_from_tact_with_effort(
        npz_name, tact_t, avg_all_tact_tuple, joint_t, joint_effort_tuple)
    if indice is None:
        exit("Window was closed without valid click")
    indice = min(indice)
    print("Object first touch selected at index", indice)

    if len(tact_t) > indice:
        # Convert to numpy arrays
        arrays = [tact_palm, tact_f1, tact_f2, tact_f3,
                  tact_t, joint_coords, joint_effort, joint_t]

        # I need to calculate average before the contact for each of the taxels and save it in some kind of format
        taxel_averages = nejaka_funkce(
            tact_palm, tact_f1, tact_f2, tact_f3, indice)

        for i, _ in enumerate(arrays):
            arrays[i] = arrays[i][indice:]
            arrays[i] = np.asarray(arrays[i])

        # Set t=0 to first contact
        arrays[4] = arrays[4] - arrays[4][0]
        arrays[7] = arrays[7] - arrays[7][0]

        # Save as npz files
        while True:
            np.savez(npz_trunc_path + npz_name, palm=arrays[0], f1=arrays[1], f2=arrays[2], f3=arrays[3],
                     tact_t=arrays[4], coords=arrays[5], effort=arrays[6], joint_t=arrays[7], taxel_averages=taxel_averages)
            try:
                np.load(npz_trunc_path + npz_name + ".npz")
                break
            except Exception:
                print("NPZ file corrupted, attempting to save it again.")

        print("Truncated data created successfully.")
    indice = None
    plt.close('all')
    return 0


if __name__ == "__main__":
    # Plot parameters
    plt.rcParams['axes.grid'] = True
    plt.rcParams['axes.axisbelow'] = True
    # plt.rcParams['grid.linestyle'] = 'dashed'
    plt.rcParams['grid.linewidth'] = 0.5
    plt.rcParams['grid.alpha'] = 0.5
    plt.rcParams['lines.markersize'] = 1
    plt.rcParams['legend.fontsize'] = 'small'

    source = npz_path

    print("NPZ file source set to " + source)

    if not os.path.exists(npz_trunc_path):
        os.mkdir(npz_trunc_path)

    entry_num = 0
    with os.scandir(source) as entries:
        sorted_entries = sorted(entries, key=lambda f: f.name)
        for entry in sorted_entries:
            entry_num += 1
            if entry.name.endswith('.npz'):
                print("\n" + str(entry_num) + " " + entry.name)
                print(25*'-')
                truncate_data(entry.name, source)
            elif entry.is_dir:
                pass
            else:
                print("\nNot a *.npz file: " + entry.name)
