# This script was used to figure out the effective frequency of the tactile topic.

import numpy as np
import os

folder = "/Volumes/DATA/Barrett_Hand/objects_180320/npz/"

surface_avg = []
with os.scandir(folder) as entries:
    for i, entry in enumerate(entries):
        meas_avg = []
        if entry.name.endswith('.npz'):
            measurement = np.load(folder + "bbdie_a1_s0.3_t10_n2.npz")
            measurement = np.load(folder + "germandie_a1_s0.3_t10_n7.npz")
            tact_palm = measurement['palm'].astype('float32')
            tact_f1 = measurement['f1'].astype('float32')
            tact_f2 = measurement['f2'].astype('float32')
            tact_f3 = measurement['f3'].astype('float32')

            last = None
            count = 0
            average = []
            for tact in [tact_palm, tact_f1, tact_f2, tact_f3]:
                for i in tact:
                    if last is None:
                        last = i[0]
                    elif last == i[0]:
                        count += 1
                    elif last != i[0]:
                        average.append(count)
                        last = i[0]
                        count = 0
                meas_avg.append(np.average(average))
            surface_avg.append(meas_avg)

        elif entry.is_dir:
            pass
        else:
            print("\nNot a *.npz file: " + entry.name)

print(np.average(surface_avg, axis=0))
