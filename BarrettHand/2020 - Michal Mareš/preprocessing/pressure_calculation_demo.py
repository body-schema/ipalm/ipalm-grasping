import os
import sys

import numpy as np

import matplotlib.pyplot as plt
from config import graphs_path, npz_path, npz_trunc_path, out_format
from utilities import average_all_taxels, average_active_taxels, get_active_taxels
from fwd_kinematics import calc_dist

indice = None


def onpick(event):
    """
    Picker to select first contact point.
    """
    global indice
    indice = event.ind
    plt.close()


def plot_tact_effort_compression(npz_name, compression, avg_tact_tuple, joint_effort_tuple):
    global indice
    colors = ["#ffb675", "", "", "", "#5fd35f", "#55a7e2"]
    max_len = min(len(compression), len(avg_tact_tuple[0][0]))

    fig, ax1 = plt.subplots()
    ax1.set_xlabel("compression [cm]")
    ax1.set_ylabel("torque [N/m]")
    for i in [5, 0, 4]:
        ax1.step(compression, joint_effort_tuple[i][0],
                 label=joint_effort_tuple[i][1] + " torque", c=colors[i])
    handles1, labels1 = ax1.get_legend_handles_labels()

    ax2 = ax1.twinx()
    ax2.set_ylabel("pressure [N/cm2]")
    colors = ["#ff3333", "#175887", "#db6600", "#217821"]
    for i, data in enumerate(avg_tact_tuple):
        ax2.step(compression[:max_len], data[0][:max_len],
                 label=data[1] + " pressure", c=colors[i], picker=1)
    # reverse the order
    handles2, labels2 = ax2.get_legend_handles_labels()
    plt.legend(handles1 + handles2, labels1 + labels2)
    plt.tight_layout()
    cid = fig.canvas.mpl_connect('pick_event', onpick)
    # plt.title(npz_name + " joint effort & tactile vs compression")
    plt.show()

    fig.canvas.mpl_disconnect(cid)
    print(indice)
    return 0


# Plot parameters
plt.rcParams['axes.grid'] = True
plt.rcParams['axes.axisbelow'] = True
# plt.rcParams['grid.linestyle'] = 'dashed'
plt.rcParams['grid.linewidth'] = 0.5
plt.rcParams['grid.alpha'] = 0.5
plt.rcParams['lines.markersize'] = 1
plt.rcParams['legend.fontsize'] = 'small'

measurement = np.load(
    "/Volumes/DATA/Barrett_Hand/plots/npz_trunc/gv5030_a3_s0.3_t10_n1.npz")
tact_palm = measurement['palm'].astype('float32')
tact_f1 = measurement['f1'].astype('float32')
tact_f2 = measurement['f2'].astype('float32')
tact_f3 = measurement['f3'].astype('float32')
tact_t = measurement['tact_t']
joint_coords = measurement['coords'].astype('float32')
joint_effort = measurement['effort'].astype('float32')
joint_t = measurement['joint_t']

tact_tuple = [(tact_palm, 'PALM'), (tact_f1, 'F1'),
              (tact_f2, 'F2'), (tact_f3, 'F3')]

total_plots = 8
truncated = False
avg_active_tact_tuple = None
try:
    taxel_averages = measurement["taxel_averages"]
    truncated = True
    total_plots = 11
    avg_active_tact_tuple = average_active_taxels(
        tact_tuple, tact_t, taxel_averages)
except Exception:
    print("Data not truncated, can not detect active taxels.")
    exit()  # TODO automatically cycle through all of the foams

avg_all_tact_tuple = average_all_taxels(tact_tuple, tact_t)

tact_to_effort_tuple = avg_all_tact_tuple
if truncated:
    tact_to_effort_tuple = avg_active_tact_tuple

joint_coords_t = joint_coords.transpose()
joint_effort_t = joint_effort.transpose()

joint_names = ['F2_TIP', 'F1', 'F2', 'F3',
               'F3_TIP', 'F1_TIP', 'SPREAD_1', 'SPREAD_2']
joint_coords_tuple = [(joint_coords_t[i], joint_names[i])
                      for i in range(len(joint_names))]
joint_effort_tuple = [(joint_effort_t[i], joint_names[i])
                      for i in range(len(joint_names))]

distance = calc_dist(joint_coords)
distance = 100*distance  # convert to cm
# convert zero point to initial contact
compression = abs(distance - distance[0])

plot_tact_effort_compression(
    "nic", compression, tact_to_effort_tuple, joint_effort_tuple)

index = indice[0]
force_palm = np.sum(tact_palm[index])
finger_surface_areas = np.array([0.3]*15 + [0.15]*9)
force_f1 = tact_f1[index] @ finger_surface_areas
force_f2 = tact_f2[index] @ finger_surface_areas
force_f3 = tact_f3[index] @ finger_surface_areas

total_force = force_palm + force_f1 + force_f2 + force_f3
print("F = ", total_force, " N")

total_area = (3*np.sum(finger_surface_areas) + 24)/10000
print("P = ", total_force/total_area, " Pa")
