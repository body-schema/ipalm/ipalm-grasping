import numpy as np


def average_all_taxels(tact_tuple, tact_t):
    """
    Calculates the averages of all taxels.

    Parameters
    ----------
    tact_tuple : array-like

    tact_t : array-like

    Returns
    -------
    avg_all_tact_tuple : arra-like
    """
    avg_all_tact_tuple = [None] * 4
    num_tact = len(tact_tuple)

    # Average values for each tactile surface
    for i in range(num_tact):
        average_tact = np.average(tact_tuple[i][0], axis=1)
        tact_label = tact_tuple[i][1]
        avg_all_tact_tuple[i] = (average_tact, tact_label)

    return avg_all_tact_tuple


def get_active_taxels(tact_tuple, taxel_averages):
    """
    Returns array with ones and zeros, one represents active taxel.

    Parameters
    ----------
    tact_tuple : array-like

    taxel_averages : array-like

    Returns
    -------
    active_taxels : array-like
    """
    active_taxels = np.zeros((4, 24))
    num_tact = len(tact_tuple)
    for t in range(num_tact):
        data_series = tact_tuple[t][0].transpose()  # (24, t)

        range_i = range(0, len(data_series))
        for i in range_i[::-1]:
            if max(data_series[i]) > 1.3 * taxel_averages[t][i]:
                active_taxels[t][i] = 1
    return active_taxels


def average_active_taxels(tact_tuple, tact_t, taxel_averages):
    """
    Calculates the averages of tactile sensors. Cells are considered active if they cross the tactile_threshold value at any moment during the measurement.

    Parameters
    ----------
    tact_tuple : array-like

    tact_t : array-like

    taxel_averages : array-like

    Returns
    -------
    avg_all_tact_tuple : array-like
    """
    avg_active_tact_tuple = [None] * 4
    num_tact = len(tact_tuple)

    # Average values for each tactile surface
    for t in range(num_tact):
        tact_label = tact_tuple[t][1]
        data_series = tact_tuple[t][0].transpose()  # (24, t)

        range_i = range(0, len(data_series))
        for i in range_i[::-1]:
            if max(data_series[i]) < 1.3 * taxel_averages[t][i]:
                data_series = np.delete(data_series, i, axis=0)
            else:
                data_series[i] = data_series[i] - taxel_averages[t][i]

        data_series = data_series.transpose()  # (t, 24 - x)

        if data_series.shape == (len(tact_t), 0):
            data_series = np.zeros((len(tact_t), 1))

        average_tact = np.average(data_series, axis=1)
        avg_active_tact_tuple[t] = (average_tact, tact_label)

    return avg_active_tact_tuple
