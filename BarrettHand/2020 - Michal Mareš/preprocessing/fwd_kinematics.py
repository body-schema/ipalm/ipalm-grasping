# https://support.barrett.com/wiki/Hand/280/KinematicsJointRangesConversionFactors

import math
from math import cos, sin

import matplotlib.pyplot as plt
import numpy as np

A_w = 0.025
A_1 = 0.05
A_2 = 0.07
A_3 = 0.05
D_w = 0.084  # it is not clear where D_w is measured on the barrett hand
D_3 = 0.0095
# I think there's no way to measure initialization offset so Phi_2 is an assumption
Phi_2 = math.radians(0)
Phi_3 = math.radians(42)

k_values = [1, 2, 3]
i_values = [1, 2, 3, 4]


def T_matrix(i, k, Thetas):
    """
    Builds and returns transformation matrix for i, k and angles Thetas on the input.
    """

    def a_min_1(i, k):
        a_values = [r(k) * A_w, A_1, A_2, A_3]
        return a_values[i-1]

    def alpha_min_1(i):
        alpha_values = [0, math.pi/2, 0, -math.pi/2]
        return alpha_values[i-1]

    def d(i):
        d_values = [D_w, 0, 0, D_3]
        return d_values[i-1]

    def theta(i, k, Thetas):
        theta_values = [r(k)*Thetas[k-1][0] - j(k)*math.pi/2,
                        Thetas[k-1][1] + Phi_2, Thetas[k-1][2], 0]
        return theta_values[i-1]

    def r(k):
        r_values = [-1, 1, 0]
        return r_values[k - 1]

    def j(k):
        j_values = [1, 1, -1]
        return j_values[k - 1]

    if i not in i_values:
        raise Exception(
            "Input i should be an integer 1, 2, 3 or 4. The value of i was: {}".format(i))
    elif k not in k_values:
        raise Exception(
            "Input k should be an integer 1, 2 or 3. The value of k was: {}".format(k))

    a_i_1 = a_min_1(i, k)
    alpha_i_1 = alpha_min_1(i)
    d_i = d(i)
    theta_i = theta(i, k, Thetas)

    matrix = [None] * 4
    matrix[0] = [cos(theta_i), -sin(theta_i), 0, a_i_1]
    matrix[1] = [sin(theta_i)*cos(alpha_i_1), cos(theta_i) *
                 cos(alpha_i_1), -sin(alpha_i_1), -d_i*sin(alpha_i_1)]
    matrix[2] = [sin(theta_i)*sin(alpha_i_1), cos(theta_i) *
                 sin(alpha_i_1), cos(alpha_i_1), d_i*cos(alpha_i_1)]
    matrix[3] = [0, 0, 0, 1]
    return np.array(matrix)


def calc_fwd_kin(joint_angles):
    """
    Input should contain all the joint states at desired time step. Returns matrix 3x3 with position vector for each finger tip.

    joint_names = ['F2_TIP', 'F1', 'F2', 'F3', 'F3_TIP', 'F1_TIP', 'SPREAD_1', 'SPREAD_2']
    """

    Thetas = [None] * 3
    # last one is always zero
    Thetas[0] = [joint_angles[6], joint_angles[7], 0]
    Thetas[1] = [joint_angles[1], joint_angles[2], joint_angles[3]]
    Thetas[2] = [joint_angles[5], joint_angles[0], joint_angles[4]]
    Thetas = np.array(Thetas).transpose()
    # print(Thetas)
    plot = False
    if plot:
        fig = plt.figure()
        ax = fig.gca(projection='3d')

    # take one finger to start with
    joint_pos = [None] * 3
    for k in range(1, 4):  # for each finger
        T_1 = T_matrix(1, k, Thetas)
        T_2 = T_matrix(2, k, Thetas)
        T_3 = T_matrix(3, k, Thetas)
        T_4 = T_matrix(4, k, Thetas)
        vec = np.transpose(np.array([[0, 0, 0, 1]]))
        T_final = np.array([T_1 @ vec, T_1 @ T_2 @ vec, T_1 @
                            T_2 @ T_3 @ vec, T_1 @ T_2 @ T_3 @ T_4 @ vec]).squeeze()

        # this contains cartesian coordinates for all of the joints of one finger
        out_coords = np.transpose(T_final[:, :3])
        joint_pos[k-1] = T_final[:, :3]

        if plot:
            ax.scatter(out_coords[0], out_coords[1], out_coords[2])
            ax.plot(out_coords[0], out_coords[1], out_coords[2])
            plt.show()
    # TODO these are probably just the fingertip positions!
    joint_pos = np.array(joint_pos)
    return joint_pos


def calc_dist(joint_coords):
    """
    This is desinged only for action 3 where fingers press against the palm of the hand

    joint_coords should be of dimensions (t, 8)
    """
    distance = np.zeros(len(joint_coords))

    for i in range(len(joint_coords)):
        joint_pos = calc_fwd_kin(joint_coords[i])

        joint_0_z = joint_pos[:, 0, 2]
        joint_3_z = joint_pos[:, 3, 2]

        z0 = np.average(joint_0_z)
        z3 = np.average(joint_3_z)
        distance[i] = abs(z3-z0)

    return distance


if __name__ == "__main__":
    print("Main of this file is for debugging only!")
    print(100*calc_fwd_kin([0.98, 2.44, 2.44, 2.44, 0.98, 0.98, 3.13, 3.13]))
    print(100*calc_fwd_kin([0.92, 2.29, 2.29, 2.29, 0.92, 0.92, 3.13, 3.13]))
    print(100*calc_fwd_kin([0.9, 2.24, 2.24, 2.24, 0.9, 0.9, 3.13, 3.13]))
    print(100*calc_fwd_kin([0.89, 2.22, 2.22, 2.22, 0.89, 0.89, 3.13, 3.13]))
    print(100*calc_fwd_kin([0.74, 1.85, 1.85, 1.85, 0.74, 0.74, 3.13, 3.13]))
