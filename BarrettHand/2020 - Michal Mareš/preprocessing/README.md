## `plot_data.py`
Goes through all of the .npz files in the specified directory and creates graphs for each of them.

## `config.py`
Configuration file where user can specify working directory for all of the other scripts.

## `truncate_data.py`
The user will be presented with a plot of tactile and effort data and selects the point of initial contact between the gripper and the object. This cutsoff the data before the contact and computes the resting value for each taxel and saves it into a new npz file.

## `delete_measurement.py`
Simple script to delete the measurement from all of the folders of the working directory.

## `fwd_kinematics.py`
Barrett Hand's forward kinematics. Computes cartesian coordinates for each joint with origin in the hand's wrist from joint coordinates.
