import os
import sys

import numpy as np

import matplotlib.pyplot as plt
from config import graphs_path, npz_path, npz_trunc_path, out_format
from utilities import average_all_taxels, average_active_taxels, get_active_taxels
from fwd_kinematics import calc_dist


def print_progress():
    sys.stdout.write('*')
    sys.stdout.flush()
    return 0


def plot_active_taxels_map(npz_name, active_taxels):
    """
    Active taxels array must contain 4*24 values either 0 for inactive or 1 for active. Order: palm, f1, f2, f3
    Plots active taxels visualization
    """
    def tranlate_to_image(array):
        grey = [211, 211, 211]
        blue = [8, 48, 107]
        image = [[0, 0, 0]] * 24
        for i, pixel in enumerate(array):
            if pixel == 0:
                image[i] = grey
            else:
                image[i] = blue
        return image

    labels = ["F1", "F2", "F3"]
    for pos, finger in enumerate(active_taxels[1:]):
        image = tranlate_to_image(finger)
        active_matrix = np.array([[[0, 0, 0]] * 3] * 8)
        for i in range(0, 8):
            for j in range(0, 3):
                active_matrix[i][j] = image[(7-i)*3+j]

        ax = plt.subplot(2, 3, pos+1)
        ax.imshow(active_matrix)
        ax.set_xticks([])
        ax.set_yticks([])
        ax.axis('off')
        ax.set_title(labels[pos])

    white = [255, 255, 255]
    p = tranlate_to_image(active_taxels[0])
    active_matrix = np.array([[white, p[0], p[1], p[2], p[3], p[4], white],
                              [p[5], p[6], p[7], p[8], p[9], p[10], p[11]],
                              [p[12], p[13], p[14], p[15], p[16], p[17], p[18]],
                              [white, p[19], p[20], p[21], p[22], p[23], white]])

    ax = plt.subplot(212)
    ax.imshow(active_matrix)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.axis('off')
    ax.set_title("Palm", y=-0.2)

    plt.subplots_adjust(wspace=0.3, hspace=0.1)
    plt.savefig(graphs_path + npz_name + "_taxelmap." +
                out_format, format=out_format)
    plt.close()
    return 0


def plot_tact_with_effort(npz_name, tact_t, avg_tact_tuple, joint_t, joint_effort_tuple):
    colors = ["#ffb675", "", "", "", "#5fd35f", "#55a7e2"]
    fig, ax1 = plt.subplots()
    ax1.set_xlabel("time [s]")
    ax1.set_ylabel("torque [N/m]")
    for i in [5, 0, 4]:
        ax1.step(joint_t, joint_effort_tuple[i][0],
                 label=joint_effort_tuple[i][1] + " torque", c=colors[i])
    handles1, labels1 = ax1.get_legend_handles_labels()

    colors = ["#ff3333", "#175887", "#db6600", "#217821"]
    ax2 = ax1.twinx()
    ax2.set_ylabel("pressure [N/cm2]")
    for i, data in enumerate(avg_tact_tuple):
        ax2.step(tact_t, data[0], label=data[1] + " pressure", c=colors[i])
    # reverse the order
    handles2, labels2 = ax2.get_legend_handles_labels()
    plt.legend(handles1 + handles2, labels1 + labels2)
    plt.tight_layout()
    # plt.title(npz_name + " joint effort & tactile")
    plt.savefig(graphs_path + npz_name + "_effort&tact." +
                out_format, format=out_format)
    plt.close()
    return 0


def plot_tact_effort_compression(npz_name, compression, avg_tact_tuple, joint_effort_tuple):
    colors = ["#ffb675", "", "", "", "#5fd35f", "#55a7e2"]
    max_len = min(len(compression), len(avg_tact_tuple[0][0]))

    fig, ax1 = plt.subplots()
    ax1.set_xlabel("compression [cm]")
    ax1.set_ylabel("torque [N/m]")
    for i in [5, 0, 4]:
        ax1.step(compression, joint_effort_tuple[i][0],
                 label=joint_effort_tuple[i][1] + " torque", c=colors[i])
    handles1, labels1 = ax1.get_legend_handles_labels()

    ax2 = ax1.twinx()
    ax2.set_ylabel("pressure [N/cm2]")
    colors = ["#ff3333", "#175887", "#db6600", "#217821"]
    for i, data in enumerate(avg_tact_tuple):
        ax2.step(compression[:max_len], data[0][:max_len],
                 label=data[1] + " pressure", c=colors[i])
    # reverse the order
    handles2, labels2 = ax2.get_legend_handles_labels()
    plt.legend(handles1 + handles2, labels1 + labels2)
    plt.tight_layout()
    # plt.title(npz_name + " joint effort & tactile vs compression")
    plt.savefig(graphs_path + npz_name + "_compr-effort&tact." +
                out_format, format=out_format)
    plt.close()
    return 0


def plot_all_taxels(npz_name, tact_t, tact_tuple):
    for data_arr in tact_tuple:
        data_series = data_arr[0].transpose()
        no_series = data_series.shape[0]

        for i in range(0, no_series):
            plt.step(tact_t, data_series[i])
        plt.xlabel("time [s]")
        plt.ylabel("pressure [N/cm2]")
        # plt.title(npz_name + " all tactile sensors on " + data_arr[1])
        plt.savefig(graphs_path + npz_name + "_all-tactile_" +
                    data_arr[1] + "." + out_format, format=out_format)
        plt.close()
    return 0


def plot_active_taxels(npz_name, tact_t, tact_tuple, taxel_averages):
    for t, data_arr in enumerate(tact_tuple):
        data_series = data_arr[0].transpose()
        no_series = data_series.shape[0]

        for i in range(0, no_series):
            if max(data_series[i]) > 1.3 * taxel_averages[t][i]:
                plt.step(tact_t, data_series[i]-taxel_averages[t][i])
            else:
                plt.step(tact_t, np.zeros(len(tact_t)))
        plt.xlabel("time [s]")
        plt.ylabel("pressure [N/cm2]")
        # plt.title(npz_name + " all active tactile sensors on " + data_arr[1])
        plt.savefig(graphs_path + npz_name + "_all-active-tactile_" +
                    data_arr[1] + "." + out_format, format=out_format)
        plt.close()
    return 0


def plot_effort(npz_name, joint_t, joint_effort_tuple):
    # Effort graph, torque sensor is only at the base of the finger tips
    for i in [5, 0, 4]:
        plt.step(joint_t, joint_effort_tuple[i]
                 [0], label=joint_effort_tuple[i][1])
    plt.xlabel("time [s]")
    plt.ylabel("torque [N/m]")
    plt.legend()
    # plt.title(npz_name + " joint effort")
    plt.savefig(graphs_path + npz_name + "_effort." +
                out_format, format=out_format)
    plt.close()
    return 0


def plot_coords(npz_name, joint_t, joint_coords_tuple):
    # Coordinates graph
    for i in [1, 2, 3, 5, 0, 4, 6, 7]:
        plt.step(joint_t, joint_coords_tuple[i]
                 [0], label=joint_coords_tuple[i][1])
    plt.xlabel("time [s]")
    plt.ylabel("joint coordinates [rad]")
    plt.legend()
    # plt.title(npz_name + " joint coordinates")
    plt.savefig(graphs_path + npz_name + "_coord." +
                out_format, format=out_format)
    plt.close()
    return 0


def plot_all_avg_tact(npz_name, tact_t, avg_all_tact_tuple):
    # Plot averaged tactile data
    colors = ["#ff3333", "#175887", "#db6600", "#217821"]
    for i, data in enumerate(avg_all_tact_tuple):
        plt.step(tact_t, data[0], label=data[1], c=colors[i])

    plt.xlabel("time [s]")
    plt.ylabel("pressure [N/cm2]")
    plt.legend()
    # fig = plt.gcf()
    # plt.title(npz_name + " averaged all tactile sensors")
    plt.savefig(graphs_path + npz_name + "_avg-all-tact." +
                out_format, format=out_format)
    plt.close()
    return 0


def plot_active_avg_tact(npz_name, tact_t, avg_active_tact_tuple):
    # Plot averaged active tactile data
    colors = ["#ff3333", "#175887", "#db6600", "#217821"]
    for i, data in enumerate(avg_active_tact_tuple):
        plt.step(tact_t, data[0], label=data[1], c=colors[i])

    plt.xlabel("time [s]")
    plt.ylabel("pressure [N/cm2]")
    plt.legend()
    # plt.title(npz_name + " averaged active tactile sensors")
    plt.savefig(graphs_path + npz_name + "_avg-active-tact." +
                out_format, format=out_format)
    plt.close()
    return 0


def plot_compression(npz_name, joint_t, dist):
    plt.step(joint_t, dist)
    plt.xlabel("time [s]")
    plt.ylabel("compression [cm]")
    # plt.title(npz_name + " compression")
    plt.savefig(graphs_path + npz_name + "_compression." +
                out_format, format=out_format)
    plt.close()
    return 0


def plot_graphs(npz_file, npz_source, all_cells=True):
    """
    Parses data from npz file and calls corresponding plot functions.

    Parameters
    ----------
    npz_file : string
        Path to the input npz file.
    """

    # Configure path where rosbags are stored
    npz_name = npz_file[:-4]

    measurement = np.load(npz_source + npz_file)
    tact_palm = measurement['palm'].astype('float32')
    tact_f1 = measurement['f1'].astype('float32')
    tact_f2 = measurement['f2'].astype('float32')
    tact_f3 = measurement['f3'].astype('float32')
    tact_t = measurement['tact_t']
    joint_coords = measurement['coords'].astype('float32')
    joint_effort = measurement['effort'].astype('float32')
    joint_t = measurement['joint_t']

    tact_tuple = [(tact_palm, 'PALM'), (tact_f1, 'F1'),
                  (tact_f2, 'F2'), (tact_f3, 'F3')]

    total_plots = 8
    truncated = False
    avg_active_tact_tuple = None
    try:
        taxel_averages = measurement["taxel_averages"]
        truncated = True
        total_plots = 11
        avg_active_tact_tuple = average_active_taxels(
            tact_tuple, tact_t, taxel_averages)
    except Exception:
        print("Data not truncated, can not detect active taxels.")

    sys.stdout.write("[%s]" % (" " * total_plots))
    sys.stdout.flush()
    sys.stdout.write("\b" * (total_plots+1))

    avg_all_tact_tuple = average_all_taxels(tact_tuple, tact_t)

    tact_to_effort_tuple = avg_all_tact_tuple
    if truncated:
        tact_to_effort_tuple = avg_active_tact_tuple

    joint_coords_t = joint_coords.transpose()
    joint_effort_t = joint_effort.transpose()

    joint_names = ['F2_TIP', 'F1', 'F2', 'F3',
                   'F3_TIP', 'F1_TIP', 'SPREAD_1', 'SPREAD_2']
    joint_coords_tuple = [(joint_coords_t[i], joint_names[i])
                          for i in range(len(joint_names))]
    joint_effort_tuple = [(joint_effort_t[i], joint_names[i])
                          for i in range(len(joint_names))]

    distance = calc_dist(joint_coords)
    distance = 100*distance  # convert to cm
    # convert zero point to initial contact
    compression = abs(distance - distance[0])

    print_progress()

    if all_cells:
        plot_all_taxels(npz_name, tact_t, tact_tuple)
        sys.stdout.write('*')
        sys.stdout.flush()

    plot_all_avg_tact(npz_name, tact_t, avg_all_tact_tuple)
    print_progress()

    plot_coords(npz_name, joint_t, joint_coords_tuple)
    print_progress()

    plot_effort(npz_name, joint_t, joint_effort_tuple)
    print_progress()

    plot_tact_with_effort(
        npz_name, tact_t, tact_to_effort_tuple, joint_t, joint_effort_tuple)
    print_progress()

    plot_compression(npz_name, joint_t, compression)
    print_progress()

    plot_tact_effort_compression(
        npz_name, compression, tact_to_effort_tuple, joint_effort_tuple)
    print_progress()

    if truncated:
        if all_cells:
            plot_active_taxels(npz_name, tact_t, tact_tuple, taxel_averages)
            print_progress()

        plot_active_avg_tact(npz_name, tact_t, avg_active_tact_tuple)
        print_progress()

        active_taxels = get_active_taxels(tact_tuple, taxel_averages)
        plot_active_taxels_map(npz_name, active_taxels)
        print_progress()

    plt.close('all')
    sys.stdout.write("]\n")  # this ends the progress bar
    return 0


if __name__ == "__main__":
    # Plot parameters
    plt.rcParams['axes.grid'] = True
    plt.rcParams['axes.axisbelow'] = True
    # plt.rcParams['grid.linestyle'] = 'dashed'
    plt.rcParams['grid.linewidth'] = 0.5
    plt.rcParams['grid.alpha'] = 0.5
    plt.rcParams['lines.markersize'] = 1
    plt.rcParams['legend.fontsize'] = 'small'

    source = npz_trunc_path

    print("NPZ file source set to " + source)
    if not os.path.exists(graphs_path):
        os.mkdir(graphs_path)

    with os.scandir(source) as entries:
        for i, entry in enumerate(entries):
            if entry.name.endswith('.npz'):
                print("\n" + entry.name)
                print(25*'-')
                plot_graphs(entry.name, source)
            elif entry.is_dir:
                pass
            else:
                print("\nNot a *.npz file: " + entry.name)
