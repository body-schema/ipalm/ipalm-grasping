clear; 

NF2140_50percentSpeed_old = importRobotiq_SpaceDelimited('data/Robotiq_2F-85/2020-04-03/Mutliple_grips_and_release/Polyurethane_Foams_set/2F85-2020-04-03-17-35-NF2140-50-05.txt');
NF2140_100percentSpeed_new = importRobotiq_SpaceDelimited('data/Robotiq_2F-85/2020-09-23/Hynek-set/NF2140/1.0/2F85-2020-09-23-15-12-NF2140-50-1.000000.txt');
NF2140_0_68percentSpeed_new = importRobotiq_SpaceDelimited('data/Robotiq_2F-85/2020-09-23/Hynek-set/NF2140/0.0068/2F85-2020-09-23-19-05-NF2140-50-0.006800.txt');

f1 = figure(1);
clf;
set(f1,'Name','NF2140_50percentSpeed_old');
Time = NF2140_50percentSpeed_old.Time;
Position = NF2140_50percentSpeed_old.Position;
Current = NF2140_50percentSpeed_old.Current;

    subplot(3,1,1)
    plot(Time,Position,'.b'); 
    ylabel('Gripper width (% of closing)');
    
    subplot(3,1,2)
    plot(Time,Current,'.r');
    ylabel('Current (A)');

    subplot(3,1,3)
    [ax h1 h2]= plotyy(Time,Position,Time,Current);
    set(h1,'LineStyle','none','Marker','.','Color','b');  
    set(h2,'LineStyle','none','Marker','.','Color','r');
    legend('Gripper width','Current');
    ylabel(ax(1), 'Gripper width (% of closing)');
    ylabel(ax(2), 'Current (A)');
    xlabel('Time (s)');
    
f2 = figure(2);
clf;
set(f2,'Name','NF2140_100percentSpeed_new');
Time = NF2140_100percentSpeed_new.Time;
Position = NF2140_100percentSpeed_new.Position;
Current = NF2140_100percentSpeed_new.Current;

    subplot(3,1,1)
    plot(Time,Position,'.b'); 
    ylabel('Gripper width (mm)');
    
    subplot(3,1,2)
    plot(Time,Current,'.r');
    ylabel('Current (A)');

    subplot(3,1,3)
    [ax h1 h2]= plotyy(Time,Position,Time,Current);
    set(h1,'LineStyle','none','Marker','.','Color','b');  
    set(h2,'LineStyle','none','Marker','.','Color','r');
    legend('Gripper width','Current');
    ylabel(ax(1), 'Gripper width (mm)');
    ylabel(ax(2), 'Current (A)');
    xlabel('Time (s)');
    
f3 = figure(3);
clf;
set(f3,'Name','NF2140_0.68percentSpeed_new');
Time = NF2140_0_68percentSpeed_new.Time;
Position = NF2140_0_68percentSpeed_new.Position;
Current = NF2140_0_68percentSpeed_new.Current;

    subplot(3,1,1)
    plot(Time,Position,'.b'); 
    ylabel('Gripper width (mm)');
    
    subplot(3,1,2)
    plot(Time,Current,'.r');
    ylabel('Current (A)');

    subplot(3,1,3)
    [ax h1 h2]= plotyy(Time,Position,Time,Current);
    set(h1,'LineStyle','none','Marker','.','Color','b');  
    set(h2,'LineStyle','none','Marker','.','Color','r');
    legend('Gripper width','Current');
    ylabel(ax(1), 'Gripper width (mm)');
    ylabel(ax(2), 'Current (A)');
    xlabel('Time (s)');

    
