function output = importRobotiq_SpaceDelimited(filename, dataLines)
%IMPORTFILE Import data from a text file
%  output = IMPORTFILE(FILENAME) reads data from text file
%  FILENAME for the default selection.  Returns the data as a table.
%
%  output = IMPORTFILE(FILE, DATALINES) reads data for the
%  specified row interval(s) of text file FILENAME. Specify DATALINES as
%  a positive scalar integer or a N-by-2 array of positive scalar
%  integers for dis-contiguous row intervals.
%
%  Example:
%  output = importfile("/home/hoffmmat/VERSION_CONTROLLED/body-schema-FELgitlab/ipalm/ipalm-grasping/squeezing_matlab/Pressing/matej-playground/data/Robotiq_2F-85/2020-09-23/Hynek-set/NF2140/1.0/2F85-2020-09-23-15-12-NF2140-50-1.000000.txt", [2, Inf]);
%
%  See also READTABLE.
%
% Auto-generated by MATLAB on 24-Sep-2020 12:09:01

%% Input handling

% If dataLines is not specified, define defaults
if nargin < 2
    dataLines = [2, Inf];
end

%% Set up the Import Options and import the data
opts = delimitedTextImportOptions("NumVariables", 3);

% Specify range and delimiter
opts.DataLines = dataLines;
opts.Delimiter = " ";
opts

% Specify column names and types
opts.VariableNames = ["Time", "Position", "Current"];
opts.VariableTypes = ["double", "double", "double"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
opts.ConsecutiveDelimitersRule = "join";
opts.LeadingDelimitersRule = "ignore";

% Import the data
output = readtable(filename, opts);

end