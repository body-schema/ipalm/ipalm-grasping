function [a1,a2,b1,b2] = find_lin_coef_from_slm(slm)

x_p = slm.knots%break point
y_p = slm.coef%value at breakpoints

%find first equation
a1 = (y_p(2)-y_p(1))/(x_p(2)-x_p(1))
b1 = y_p(1)-a1*x_p(1)
%find second equation
a2 = (y_p(3)-y_p(2))/(x_p(3)-x_p(2))
b2 = y_p(2)-a2*x_p(2)