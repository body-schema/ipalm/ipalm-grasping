clear;
close all;

% load('stressball_kinova.mat');
% load('empty_kinova.mat');
load('Kinova2020_01_15_Empty.mat')
load('Kinova2020_01_15_KinovaCube.mat')
load('Kinova2020_01_15_BrownCube.mat')

% x{1} =  (1-StressballKin.position)*85;
% y{1} = StressballKin.current;
% x{2} = (1-EmptyKin.position)*85;
% y{2} = EmptyKin.current;

% x{1} =  (1-cell2mat(Kinova2020_01_15_Empty.position))*85;
% y{1} = cell2mat(Kinova2020_01_15_Empty.current);
% x{1} =  (1-cell2mat(Kinova2020_01_15_KinovaCube.position))*85;
% y{1} = cell2mat(Kinova2020_01_15_KinovaCube.current);
x{1} =  (1-cell2mat(Kinova2020_01_15_BrownCube.position))*85;
y{1} = cell2mat(Kinova2020_01_15_BrownCube.current);

figure();
slm=cell(1,2);
for i = 1:length(x)
    slm{i} = slmengine(x{i},y{i},'degree',1,'knots',3, 'interiorknots','free','plot','on');
    slmeval(1.3,slm{i});
end
close all;
figure()
hold on;
for i = 1:length(x)
    scatter(x{i},y{i},8);
end
%empty hand
%    xrange = slm{i}.knots
%    xev = linspace(xrange(1),xrange(2),size(x{i},1))
%    yline1 = a1*xev+b1;
%    plot(xev,yline1);hold on;
%    xev = linspace(xrange(2),xrange(3),size(x{i},1))
%    yline2 = a2*xev+b2;
%s    plot(xev,yline2);hold on;

for i = 1:length(x)
    [a1,a2,b1,b2] = find_lin_coef_from_slm(slm{i});
    %[a1,a2,a3,b1,b2,b3] = find_lin_coef_from_slm3(slm{i})
    %find first equation
    
    %find second equation
    xrange = slm{i}.knots;
    xev = linspace(xrange(1),xrange(2),size(x{i},1));
    yline1 = a1*xev+b1;
    plot(xev,yline1);
    xev = linspace(xrange(2),xrange(3),size(x{i},1));
    yline2 = a2*xev+b2;
    plot(xev,yline2);
    %xev = linspace(xrange(3),xrange(4),size(x{i},1))
    %yline2 = a3*xev+b3;
    %plot(xev,yline3);hold on;
end
hold off;
% legend('empty','Kinova Cube','Brown Cube');
legend('Brown Cube');
xlabel('Gripper position [mm]');
% axis([0 1 0 900]);
ylabel('Relative current [%]');
%set(gca,'Xdir','reverse')
