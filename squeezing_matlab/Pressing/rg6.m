load('sponge_wide_rg6.mat')
plot(sponge_wide.F, sponge_wide.width)
set(gca,'Xdir','reverse')
xlabel('measured gripper width')
ylabel('applied force')

x = sponge_wide.F
y = sponge_wide.width
%x = (sort(rand(1,100)) - 0.5)*pi;
%y = sin(x).^5 + randn(size(x))/10;

%slm = slmengine(x,y,'plot','on','knots',10,'increasing','on', ...
%'leftslope',0,'rightslope',0)

slm = slmengine(x,y,'degree',1,'knots',3, 'interiorknots','free','plot','on')

slmeval(1.3,slm)
slmpar(slm,'maxslope') 

plotslm(slm)