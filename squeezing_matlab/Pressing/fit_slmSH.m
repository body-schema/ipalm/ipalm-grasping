clear;
close all;
load('sponge_softhand.mat');
load('stressball_softhand.mat');
load('empty_softhand.mat');
load('block_softhand.mat');

x{1} = (spongeSH.position-200)/17900;
y{1} = spongeSH.current;
x{2} = (stressballSH.position-200)/17900;
y{2} = stressballSH.current;
x{3} = (emptySH.position-200)/17900;
y{3} = emptySH.current;
x{4} = (blockSH.position-200)/17900;
y{4} = blockSH.current;

slm=cell(1,4);

% figure();
for i = 1:length(x)
    slm{i} = slmengine(x{i},y{i},'degree',1,'knots',3, 'interiorknots','free','plot','on');
    slmeval(1.3,slm{i});
end
close all;
figure();
for i = 1:length(x)
    scatter(x{i},y{i},15);hold on;
end
%empty hand
%     xrange = slm{i}.knots
%     xev = linspace(xrange(1),xrange(2),size(x{i},1))
%     yline1 = a1*xev+b1;
%     plot(xev,yline1);hold on;
%     xev = linspace(xrange(2),xrange(3),size(x{i},1))
%     yline2 = a2*xev+b2;
%     plot(xev,yline2);hold on;
hold on;
for i = 1:length(x)
    [a1,a2,b1,b2] = find_lin_coef_from_slm(slm{i});
    %[a1,a2,a3,b1,b2,b3] = find_lin_coef_from_slm3(slm{i})
    %find first equation
    
    %find second equation
    xrange = slm{i}.knots;
    xev = linspace(xrange(1),xrange(2),size(x{i},1));
    yline1 = a1*xev+b1;
    plot(xev,yline1);
    
    xev = linspace(xrange(2),xrange(3),size(x{i},1));
    yline2 = a2*xev+b2;
    plot(xev,yline2);%hold on;
    %xev = linspace(xrange(3),xrange(4),size(x{i},1))
    %yline2 = a3*xev+b3;
    %plot(xev,yline3);hold on;
end
hold off;
legend('Sponge - Vertical','Stressball','Empty hand','Green Cube','FontSize',20)
xlabel('Position - [%]','FontSize',20)
axis([0 1 0 900]);

ylabel('Current - [mA]','FontSize',20)
set(gca,'FontSize',20)
