s2 = 1;
P0 = [.1 1 1 -.5 1];
lb = [-1 -10 -10 -.9 .1];
ub = [1 10 10 .8 1.8];
s2 = 1;
plusfun = @(x) max(x,0);
model = @(P,x) P(1) + s2*x + (s2-P(2))*plusfun(P(4)-x) + (P(3)-s2)*plusfun(x-(P(4)+P(5)));
Pfit = lsqcurvefit(model,P0,x,y,lb,ub)


modelpred = model(Pfit,sort(x));
plot(x,y,'o',sort(x),modelpred,'r-')