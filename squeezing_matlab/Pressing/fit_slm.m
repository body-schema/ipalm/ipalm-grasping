clear;
close all;
load('sponge_wide_rg6.mat');
load('stressball_rg6.mat');
load('sponge_thin_rg6.mat');
load('cube_rg6.mat');
load('block_rg6.mat');


x{1} = sponge_wide.width;
y{1} = sponge_wide.F;

x{2} = stressball.width;
y{2} = stressball.F;

x{3} = sponge_thin.width;
y{3} = sponge_thin.F;

x{4} = cube.width;
y{4} = cube.F;

x{5} = block.width;
y{5} = block.F;

%x = (sort(rand(1,100)) - 0.5)*pi;
%y = sin(x).^5 + randn(size(x))/10;

%slm = slmengine(x,y,'plot','on','knots',10,'increasing','on', ...
%'leftslope',0,'rightslope',0)


figure();
slm=cell(1,5);
x{1} = sponge_wide.width;
x{2} = stressball.width;
x{3} = sponge_thin.width;
for i = 1:length(x)
    slm{i} = slmengine(x{i},y{i},'degree',1,'knots',3, 'interiorknots','free','plot','on');
    slmeval(1.3,slm{i});
end
close all;


figure()
hold on;
for i = 1:length(x)
    scatter(x{i},y{i},8);
end

for i = 1:length(x)
    [a1,a2,b1,b2] = find_lin_coef_from_slm(slm{i});
    %find first equation
    
    %find second equation
    xrange = slm{i}.knots;
    xev = linspace(xrange(1),xrange(2),size(x{i},1));
    yline1 = a1*xev+b1;
    plot(xev,yline1);
    xev = linspace(xrange(2),xrange(3),size(x{i},1));
    yline2 = a2*xev+b2;
    plot(xev,yline2);
end
hold off;
legend('spongeWide','stressball','spongeThin','cube','block','FontSize',12);
xlabel('Measured gripper width - [mm]','FontSize',12);
ylabel('Applied force - [N]','FontSize',12);
set(gca,'FontSize',12)
